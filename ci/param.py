#!/usr/bin/env python
#coding=utf-8
"""
A parser for Parameter.cpp
"""

import re

def olname():
   """Parses Parameter.cpp to get the correspondence between
   input CLI parameters and their ONELAB name
   """
   ## Regexp
   param = re.compile(r'void Parameter::defParam\(void\)\{')
   clsng = re.compile(r'\}')
   keywd = re.compile(r'(number|flag|string|\".*?\")')

   ## Prepare dic and open file
   dic = dict()
   with open('../src/interface/Parameter.cpp') as src:
      # Read file and look for span of void Parameter::defParam(void){...}'
      txt  = src.read()
      strt = param.search(txt).span()[1]
      end  = clsng.search(txt).span()[0]

      # Find all keywords and loop through them
      match = keywd.findall(txt[strt:end])

      # Loop through them and construct dic according to the keywords
      i = 0
      while i < len(match):
         if match[i] == "number" or match[i] == "flag":
            key = match[i+1]
            val = match[i+2]
            i = i+3
         elif match[i] == "string":
            key = match[i+1]
            val = match[i+3]
            i = i+4
         else:
            raise RuntimeError('Error when parsing Parameter.cpp')

         if val != "":
            dic[key[1:-1]] = val[1:-1] # Remove enclosing '"'

   ## Done
   return dic
