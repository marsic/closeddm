#!/usr/bin/env python
#coding=utf-8
"""
spectrum.py, Spectrum of optimized Schwarz operator
"""

import os
import re
import sys
import tempfile
from   utils import argument, option, info, temp, mesh, closeddm


def valgrind(cdd, geo, size, k, full):
    """Checks memory leaks using Valgrind.
    Input:
     * cdd, the options to pass to closeddm (without the '-')
     * geo, the .geo file with the model to extract the modes from
     * size, the mesh size (in element per wavelength, see below)
     * k, the wavenumber for the mesh target wavelength (see above)
     * full, uses a direct solver if true, uses the Schwarz one otherwise
    """
    ## Options
    cdd = cdd | {'k': k}
    option(cdd)

    ## Info
    info('valgrind', {'geo': geo, 'size': size, 'k': k, 'full': full}, cdd)

    ## CWD
    cwd = os.getcwd()

    ## Run
    with tempfile.TemporaryDirectory() as tmp:
        geofile, mshfile, dbfile = temp(geo, tmp)

        msh, err = mesh(geofile, size, cdd, tmp)
        ddm, err = closeddm(mshfile, dbfile, full, tmp,
                            'valgrind', ['--leak-check=full',
                                         # '--show-leak-kinds=all',
                                         '--suppressions=' + \
                                           cwd + '/valgrind.supp'])

        s = re.search(r'.*? ERROR SUMMARY: (?P<error>\d+) errors .*', err)

    ## Done
    return int(s.group('error')), err


if __name__ == "__main__":
   ## Arguments
   vg, cdd = argument(sys.argv, 'vg',
                         ['vg_geo', 'vg_size', 'vg_k', 'vg_full'])
   geo  = vg['vg_geo']
   size = int(vg['vg_size'])
   k    = float(vg['vg_k'])
   full = vg['vg_full'] == '1'

   ## Run
   err, msg = valgrind(cdd, geo, size, k, full)

   ## Check
   if err == 0:
       print('# OK :-)')
   else:
       print('# Error(s) found :-(')
       print(msg)
