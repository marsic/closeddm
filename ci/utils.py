#!/usr/bin/env python
#coding=utf-8
"""
Some useful functions for continuous integration
"""

import os
import param
import re
import shutil
import subprocess


## Constants
path_closeddm  = os.path.abspath('../build/closeddm')
path_gmsh      = 'gmsh'
option_default = {'save':  '0',
                  'merge': '0'}
olname         = param.olname()


# Functions
def closeddm(msh, db, full=False, cwd=None, instrument='', instr_opt=[]):
   """Runs closeddm on the given mesh and with the given database file.
   If full is True, then use a direct solver.
   If cwd is not None, use the given path as working directory.
   If an insrument is given (e.g. valgrind), closeddm is run within it.
   In addition, this instrument can be passed the instr_opt options.
   """
   arg = ['full'] if full else []
   arg = arg + ['-msh', msh, '-db', db]
   if instrument == '':
      return run(path_closeddm, arg, cwd)
   else:
      return run(instrument, instr_opt+[path_closeddm]+arg, cwd)

def mesh(geo, size, param, cwd=None):
   """Meshes the given .geo file, with the given size, the given parameters
   (converted to their ONELAB names if availlable) and saves the mesh and
   the ONELAB database (with extension .msh and .db respectively).
   If cwd is not None, use the given path as working directory.
   """
   arg = [geo, '-run', '-setnumber', '02Mesh/02Points per wavelength', size]
   for i in param:
      arg.append('-setnumber' if isfloat(param[i]) else '-setstring')
      arg.append(i if i not in olname else olname[i])
      arg.append(param[i])
   return run(path_gmsh, arg, cwd)

def run(exe, arg, cwd=None):
   """Runs the given executable with the given arguments
   and returns the stdout and stderr.
   If cwd is not None, use the given path as working directory.
   """
   ## Prepare command and run
   cmd = [str(exe)]
   for i in arg:
      cmd.append(str(i))
   p = subprocess.run(cmd, cwd=cwd, capture_output=True, text=True)

   ## Clean ANSI escape codes (see [1] and end of file)
   ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
   err = ansi_escape.sub('', p.stderr)
   out = ansi_escape.sub('', p.stdout)

   ## Look for errors and behave accordingly
   if re.search(r'Error', err) is not None or p.returncode != 0:
      raise RuntimeError('Error when calling ' + exe + os.linesep + \
                         'Return code: ' + str(p.returncode) + os.linesep + \
                         'stderr' + os.linesep + \
                         '======' + os.linesep + err)

   ## Done
   return out, err

def temp(geo, dir):
   """Given a .geo file and a directory, populates the given directory
   with the data required by the .geo model and returns the name of
   the .geo, .msh and .db files within that directoty.
   """
   geoname = os.path.basename(geo)
   mshname = geoname.split('.')[0] + '.msh'
   dbname  = geoname.split('.')[0] + '.db'
   dirname = os.path.dirname(geo)

   shutil.copytree(dirname, dir, dirs_exist_ok=True)
   geofile = os.path.join(dir, geoname)
   mshfile = os.path.join(dir, mshname)
   dbfile  = os.path.join(dir,  dbname)

   return geofile, mshfile, dbfile

def option(opt):
   """Set the opt dictionary to its default values, if a key is not present"""
   for i in option_default:
      if i not in opt:
         opt[i] = option_default[i]

def info(name, param, cdd):
   """Print a little message stating the name of the test,
   its parameters and the parameters of closeddm.
   A parameter is a dictionary associating the parameter name with its value.
   """
   print('# Start ' + name + ' test')
   for i in param:
      print(' -->',  name + ' - ' + i + ':', param[i])
   for i in cdd:
      print(' -->', 'closeddm - ' + i + ':', cdd[i])

def argument(arg, prefix, required=[]):
   """Parses the list of arguments arg and returns two dictionaries:
     * one with the arguments starting with '-prefix_' and
     * one with the other arguments.
   Each dictionary uses the argument as a key (without the '-' sign).
   This function will raise an exception
   if all the entries in required are not met in arg.
   """
   if len(arg)-1 < len(required):
      raise RuntimeError('At least ' + str(len(required)) + \
                         ' options are required: ' + str(required))

   if arg[1][0] != '-':
      raise RuntimeError('An option must start with a "-" sign')

   it = iter(arg)
   next(it)

   cur = arg[1][1:]
   opt = dict()
   ext = dict()
   for a in it:
      if a[0] == '-':
         cur = a[1:]
      else:
         if(cur[0:len(prefix)] == prefix):
            add(opt, cur, a)
         else:
            add(ext, cur, a)

   for i in required:
      if (i not in opt) and (i not in ext):
         raise RuntimeError('Option ' + i + ' is required')

   return opt, ext

def add(dic, key, value):
   """Add the given value in the given dictionary (dic) at the given key.
   If the key already exists, append the value into a list at that key.
   """
   if key in dic:
      if type(dic[key]) is list:
         dic[key].append(value)
      else:
         dic[key] = [dic[key], value]
   else:
      dic[key] = value

def isfloat(x):
   """Returns True if x can be converted to a float and Flase otherwise"""
   try:
      tmp = float(x)
      return True
   except:
      return False


## Notes
### [1] https://stackoverflow.com/questions/14693701/how-can-i-remove-the-ansi-escape-sequences-from-a-string-in-python
