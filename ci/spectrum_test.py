import csv
import functools
import pytest
from   spectrum import spectrum

geo   = '../test/rectangle.geo'
size  = 8
k     = 42
order = 4
tol   = 1

tc   = 'tcName'
vr   = 'tcVar'
nAux = 'nAux'
cav  = 'cavity'
ref  = 'ref'
base = './spectrum/'

scenario = [
    {tc: 'oo0',  vr: 'open',   nAux: 0,  cav: 0, ref: base+'oo0_open.csv'},
    {tc: 'oo2',  vr: 'open',   nAux: 0,  cav: 0, ref: base+'oo2_open.csv'},
    {tc: 'pade', vr: 'open',   nAux: 32, cav: 0, ref: base+'pade32_open.csv'},
    {tc: 'oo0',  vr: 'closed', nAux: 0,  cav: 1, ref: base+'oo0_closed.csv'},
    {tc: 'ml',   vr: 'closed', nAux: 32, cav: 1, ref: base+'ml32_closed.csv'},
    {tc: 'pade', vr: 'closed', nAux: 32, cav: 1, ref: base+'pade32_closed.csv'}]

fovr = {'order': 1,  ## Override for fast version ##
        'size':  2}


@pytest.mark.parametrize("opt", scenario)
def test_eigenvalue(opt, fast):
    ## Fast or full?
    mysize  = size  if not fast else fovr['size']
    myorder = order if not fast else fovr['order']

    ## Spectrum
    spc = spectrum(opt | {'order': myorder}, geo, mysize, k)

    ## Reference
    ref = list()
    with open(opt['ref'], mode ='r') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for l in reader:
            ref.append(complex(float(l[0]), float(l[1])))

    ## Check
    if not fast:
        done = list()
        for _ in range(len(ref)):
            done.append(False)

        for r in ref:
            nrt = nearest(spc, r, tol, done)
            assert len(nrt) != 0, 'Spectra do not match'
            done[nrt[0][0]] = True

    ## Done
    print('# OK :-)')


def nearest(lst, ref, tol, done):
    ## Checks
    if len(lst) == 0:
        return list()
    if len(lst) != len(done):
        return list()

    ## Sort according to distance w.r.t. ref
    cpy = [(i, lst[i]) for i in range(len(lst))]
    cpy.sort(key=functools.cmp_to_key(lambda x, y:  abs(x[1]-ref)
                                                  < abs(y[1]-ref)))

    ## Take candidate better than tol, but exclude those already met
    cdd = list()
    for j in cpy:
        if abs(j[1]-ref)/abs(ref) < tol/100 and not done[j[0]]:
            cdd.append(j)

    ## Done
    return cdd
