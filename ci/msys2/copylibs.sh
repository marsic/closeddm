#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "Usage: copylibs.sh EXE DIR"
    echo "Determines the libraries related to MinGW or MSYS of the given"
    echo "executable and copies them in the given directory"

    exit -1
fi


for i in $(ldd $1 | grep -e $HOME -e 'mingw' -e 'msys' | awk '{print $3}')
do
    cp $i $2
done
