#!/usr/bin/python3.exe

# From https://gitlab.com/petsc/petsc/-/issues/820

if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-debugging=1',
    '--with-mpi=0',
    '--with-mpiuni-fortran-binding=0',
    '--download-mumps=yes',
    '--with-mumps-serial=yes',
    '--with-shared-libraries=1',
    '--with-single-library=1',
    '--with-x=0',
    '--with-ssl=0',
    '--with-scalar-type=complex',
    '--with-petsc-arch=m',
    '--with-fortran-bindings=0',
    'CFLAGS=-march=native -g -O3',
    'CXXFLAGS=-march=native -g -O3',
    'FFLAGS=-march=native -g -O3',
  ]
  configure.petsc_configure(configure_options)
