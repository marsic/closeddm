#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Usage: winlib.sh DLL"
    echo "Creates the .def and .lib files associated with the given .dll"

    exit -1
fi


BASE=$(basename $1 .dll)

gendef - ${BASE}.dll > ${BASE}.def
dlltool --input-def ${BASE}.def --output-lib ${BASE}.lib
