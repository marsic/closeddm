#!/bin/bash

## Stop if error
set -e

## Evironement variables
export PETSC_DIR=${HOME}/p
export PETSC_ARCH=m
export SLEPC_DIR=${HOME}/s
export GMSH_DIR=${HOME}/project/gmsh/install
export FEM_DIR=${HOME}/project/fem/install
export DDM_DIR=${HOME}/project/ddm/install
export MPS_DIR=${HOME}/project/MPSolve/install-dir

export PETSC_LIB=${PETSC_DIR}/${PETSC_ARCH}/lib
export SLEPC_LIB=${SLEPC_DIR}/${PETSC_ARCH}/lib
export GMSH_LIB=${GMSH_DIR}/lib
export FEM_LIB=${FEM_DIR}/lib
export DDM_LIB=${DDM_DIR}/lib
export MPS_LIB=${MPS_DIR}/bin

export LD_LIBRARY_PATH=${PETSC_LIB}:${SLEPC_LIB}:${GMSH_LIB}:${FEM_LIB}:${DDM_LIB}:${MPS_LIB}:${LD_LIBRARY_PATH}
export CMAKE_PREFIX_PATH=${GMSH_DIR}:${FEM_DIR}:${DDM_DIR}:${MPS_DIR}:${CMAKE_PREFIX_PATH}

export CFLAGS="-g -O3 -march=native"
export CXXFLAGS=$CFLAGS
export FFLAGS=$CFLAGS

## Root
ROOT=${PWD}

## Update
pacman -Suuy --noconfirm

## Pacman
pacman -S --noconfirm \
  mingw-w64-x86_64-toolchain mingw-w64-x86_64-cmake mingw-w64-x86_64-ninja \
  mingw-w64-x86_64-python python \
  mingw-w64-x86_64-python-numpy mingw-w64-x86_64-python-pytest \
  mingw-w64-x86_64-eigen3 mingw-w64-x86_64-openblas \
  mingw-w64-x86_64-opencascade mingw-w64-x86_64-fltk \
  base-devel autoconf make automake-wrapper bison \
  zsh git emacs patch which wget

## bin directory
cd ${HOME}
mkdir bin
cp ${ROOT}/copylibs.sh bin/
cp ${ROOT}/winlib.sh   bin/

## PETSc (see [1])
cd ${HOME}
git clone -b release https://gitlab.com/petsc/petsc.git p
cp ${ROOT}/run_petsc_seq.py p/
cd p
/usr/bin/python3 run_petsc_seq.py
make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} all

## SLEPc
cd ${HOME}
git clone -b release https://gitlab.com/slepc/slepc.git s
cd s
/usr/bin/python3 configure
make SLEPC_DIR=${SLEPC_DIR} PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} all

## Unix to MS-Windows paths
cd ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf
cp petscvariables petscvariables.unix
sed s_${PETSC_DIR}_$(cygpath -m ${PETSC_DIR})_g petscvariables.unix \
    > petscvariables.win
cp petscvariables.win petscvariables

## Create missing .def and .lib files for PETSc and SLEPc
cd ${PETSC_DIR}/${PETSC_ARCH}/lib
sh ${HOME}/bin/winlib.sh libpetsc.dll
cd ${SLEPC_DIR}/${PETSC_ARCH}/lib
sh ${HOME}/bin/winlib.sh libslepc.dll

## Projects
cd ${HOME}
mkdir project

## Gmsh (lib)
cd ${HOME}/project
git clone https://gitlab.onelab.info/gmsh/gmsh.git
cd gmsh
mkdir buildlib
cd buildlib
CMAKE_GENERATOR="Ninja" \
  cmake -DDEFAULT=0 -DENABLE_PARSER=1 -DENABLE_POST=1 -DENABLE_ANN=1 \
        -DENABLE_EIGEN=1 -DENABLE_OPENMP=1 -DENABLE_BUILD_SHARED=1   \
        -DENABLE_BUILD_DYNAMIC=1 -DENABLE_PRIVATE_API=1              \
        -DENABLE_OPENMP=1 -DENABLE_SYSTEM_CONTRIB=1                  \
        -DCMAKE_INSTALL_PREFIX=../install ..
ninja install -j$(nproc)

## Gmsh (exe)
cd ${HOME}/project/gmsh
mkdir build
cd build
CMAKE_GENERATOR="Ninja" \
  cmake ..
ninja -j$(nproc)
cp gmsh.exe ${HOME}/bin

## GmshFEM
cd ${HOME}/project
git clone https://gitlab.onelab.info/gmsh/fem.git
cd fem
mkdir build
cd build
CMAKE_GENERATOR="Ninja" \
  cmake -DENABLE_SYSTEM_CONTRIB=1 \
        -DCMAKE_INSTALL_PREFIX=../install ..
ninja install -j$(nproc)

## GmshDDM
cd ${HOME}/project
git clone https://gitlab.onelab.info/gmsh/ddm.git
cd ddm
mkdir build
cd build
CMAKE_GENERATOR="Ninja" \
  cmake -DENABLE_MPI=0 \
        -DCMAKE_INSTALL_PREFIX=../install ..
ninja install -j$(nproc)

## MPSolve
cd ${HOME}/project
wget https://numpi.dm.unipi.it/_media/software/mpsolve/mpsolve-3.2.1.tar.bz2
tar xf mpsolve-3.2.1.tar.bz2
mv mpsolve-3.2.1 MPSolve
rm mpsolve-3.2.1.tar.bz2
cd MPSolve
./configure --prefix=$PWD/install-dir
make -j$(nproc)
make install

## closeddm
cd ${HOME}/project
git clone https://git.rwth-aachen.de/marsic/closeddm.git
cd closeddm
mkdir build
cd build
CMAKE_GENERATOR="Ninja" \
  cmake -DMPI=0 ..
ninja -j$(nproc)

## Done
cd ${HOME}

## TODO
# mingw-w64-x86_64-msmpi

## Notes
## [1] https://gitlab.com/petsc/petsc/-/issues/820
