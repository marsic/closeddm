import numpy
from   osm import osm
import pytest

geo   = '../test/rectangle.geo'
nDom  = 4
k     = 314.17
size  = 8
order = 4
ny    = 1 ## First mode only
allmd = 0 ## ...
maxIt = 1000

fovr = {'nDom':  2,  ## Override for fast version ##
        'k':     k/4,
        'order': 1,
        'size':  2}

tc   = 'tcName'
vr   = 'tcVar'
nAux = 'nAux'
mPro = 'mixPro'
mAux = 'mixAux'

scenario = [{tc: 'oo0',  vr: 'open',   nAux: 0,  mPro: 0,   mAux: 0},
            {tc: 'oo2',  vr: 'open',   nAux: 0,  mPro: 0,   mAux: 0},
            {tc: 'pade', vr: 'open',   nAux: 32, mPro: 0,   mAux: 0},
            {tc: 'oo0',  vr: 'closed', nAux: 0,  mPro: 0,   mAux: 0},
            {tc: 'ml',   vr: 'closed', nAux: 32, mPro: 0,   mAux: 0},
            {tc: 'pade', vr: 'closed', nAux: 32, mPro: 0,   mAux: 0},
            {tc: 'oo0',  vr: 'mixed',  nAux: 0,  mPro: 0.9, mAux: 4},
            {tc: 'ml',   vr: 'mixed',  nAux: 32, mPro: 0.9, mAux: 4},
            {tc: 'pade', vr: 'mixed',  nAux: 32, mPro: 0.9, mAux: 4}]

refWG = [scenario[0] | {'ref': 13},  ## Reference values from 2022.11.15
         scenario[1] | {'ref': 11},  ## ...
         scenario[2] | {'ref':  7},  ## ...
         scenario[3] | {'ref': 49},  ## ...
         scenario[4] | {'ref': 44},  ## ...
         scenario[5] | {'ref': 23},  ## ...
         scenario[6] | {'ref': 53},  ## ...
         scenario[7] | {'ref': 31},  ## ...
         scenario[8] | {'ref': 19}]  ## ...
                                     ## ...
refCV = [scenario[0] | {'ref': 34},  ## ...
         scenario[1] | {'ref': 29},  ## ...
         scenario[2] | {'ref': 11},  ## ...
         scenario[3] | {'ref': 120}, ## ...
         scenario[4] | {'ref': 35},  ## ...
         scenario[5] | {'ref': 17},  ## ...
         scenario[6] | {'ref': 55},  ## ...
         scenario[7] | {'ref': 32},  ## ...
         scenario[8] | {'ref': 17}]  ## ...

scenarioWG = [x | {'cavity': 0, 'ny': ny, 'allmodes': allmd} for x in refWG]
scenarioCV = [x | {'cavity': 1, 'ny': ny, 'allmodes': allmd} for x in refCV]


@pytest.mark.parametrize("opt", scenarioWG + scenarioCV)
def test_osm(opt, fast):
    ## Fast or full?
    myndom  = nDom  if not fast else fovr['nDom']
    myk     = k     if not fast else fovr['k']
    mysize  = size  if not fast else fovr['size']
    myorder = order if not fast else fovr['order']

    ## Convergence history of optimized Schwarz
    hist = osm({'order': myorder, 'maxIt': maxIt} | opt,
               geo, myndom, mysize, myk)

    ## Check
    if not fast:
        assert len(hist) == opt['ref'], \
            'Have '+str(len(hist))+' iterations, but expected '+str(opt['ref'])

    ## Done
    print("# OK :-)")
