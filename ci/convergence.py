#!/usr/bin/env python
#coding=utf-8
"""
convergence.py, convergence test w.r.t. mesh refinement and L2 relative error
"""

import csv
import numpy
import re
import sys
import tempfile
from   utils import argument, option, info, temp, mesh, closeddm


def convergence(cdd, geo, size, order, ref, full):
   """Performs a convergence test and returns the relative L2 errors
   as well as their slopes.
   Input:
     * cdd, the options to pass to closeddm (without the '-')
     * geo, the .geo file with the model to test the convergence on
     * size, a list with the mesh sizes (in elements per wavelength)
     * order, a list with the finite element orders
     * ref, the reference solution to compute the error against
     * full, uses a direct solver if true, uses the Schwarz one otherwise
   """
   ## Options
   cdd = cdd | {'error': ref}
   option(cdd)

   ## Info
   info('convergence', {'geo': geo, 'size': size, 'order': order,
                        'ref': ref, 'full': full}, cdd)

   ## Format
   fmt = ' {0: <4} | {1: <5} | {2}'
   print(fmt.format('Mesh', 'Order', 'Error'))

   ## Run
   rel = {}
   with tempfile.TemporaryDirectory() as tmp:
      geofile, mshfile, dbfile = temp(geo, tmp)
      for f in order:
         rel[f] = {}
         for m in size:
            mycdd = cdd | {'order': f, 'gauss': 2*f}
            msh, err = mesh(geofile, m, mycdd, tmp)
            ddm, err = closeddm(mshfile, dbfile, full, tmp)

            rel[f][m] = float(re.search(r'(?<=Relative error).+',
                                        ddm).group(0).split(':')[-1])
            print(fmt.format(str(m), str(f), '{:.4e}'.format(rel[f][m])))

   ## Post
   e = numpy.zeros((len(order), len(size)))
   for i in range(len(order)):
      for j in range(len(size)):
         e[i, j] = rel[order[i]][size[j]]

   slope = numpy.zeros((len(order), len(size)-1))
   for i in range(len(order)):
      slope[i, :] = numpy.diff(numpy.log(e[i, :])) / numpy.diff(numpy.log(size))

   ## Print and return
   print('# Slopes:')
   print(slope)
   return e, slope

def validate(slope, order, tol):
   """Validate the given slope for the given orders and tolerance
   (in percent of slope)
   """
   if(not slope.any()):
      print("Cannot validate slope: only one error computed?")
      return

   for i, j in zip(range(len(order)), order):
      assert -slope[i, -1] > j - j*tol/100 + 1, 'Bad slope for order ' + str(j)
   print('# OK :-)')


if __name__ == "__main__":
   ## Arguments
   conv, cdd = argument(sys.argv, 'conv', ['conv_geo', 'conv_size',
                                           'conv_order', 'conv_ref',
                                           'conv_tol', 'conv_full', 'nDom'])
   cs = conv['conv_size']
   co = conv['conv_order']

   geo   = conv['conv_geo']
   size  = [int(x) for x in cs] if type(cs) is list else [int(cs)]
   order = [int(x) for x in co] if type(co) is list else [int(co)]
   ref   = conv['conv_ref']
   tol   = float(conv['conv_tol'])
   full  = conv['conv_full'] == '1'

   ## Run and validate
   try:
      error, slope = convergence(cdd, geo, size, order, ref, full)
      validate(slope, order, tol)
   except Exception as e:
      print(e)

   ## Write error in .csv file
   with open('convergence.csv', 'w') as csvfile:
      writer = csv.writer(csvfile)
      writer.writerow(['h'] + ['o'+str(x) for x in order]) # Header with orders
      for j in range(len(size)):
         writer.writerow([size[j]] + error[:, j].tolist())
