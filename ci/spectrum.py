#!/usr/bin/env python
#coding=utf-8
"""
spectrum.py, Spectrum of optimized Schwarz operator
"""

import csv
import os
import sys
import tempfile
from   utils import argument, option, info, temp, mesh, closeddm


def spectrum(cdd, geo, size, k):
    """Computes the spectrum of an optimized Schwarz transmission operator.
    Input:
     * cdd, the options to pass to closeddm (without the '-')
     * geo, the .geo file with the model to extract the modes from
     * size, the mesh size (in element per wavelength, see below)
     * k, the wavenumber for the mesh target wavelength (see above)
    """
    ## Options
    cdd = cdd | {'k': k, 'spectrum': 1}
    option(cdd)

    ## Info
    info('spectrum', {'geo': geo, 'size': size, 'k': k}, cdd)

    ## Run
    spect = list()
    with tempfile.TemporaryDirectory() as tmp:
        geofile, mshfile, dbfile = temp(geo, tmp)

        msh, err = mesh(geofile, size, cdd, tmp)
        ddm, err = closeddm(mshfile, dbfile, False, tmp)

        with open(os.path.join(tmp, 'eig.csv'), mode ='r') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for l in reader:
                spect.append(complex(float(l[0]), float(l[1])))

    ## Done
    return spect


if __name__ == "__main__":
   ## Arguments
   stest, cdd = argument(sys.argv, 'stest',
                         ['stest_geo', 'stest_size', 'stest_k'])
   geo  = stest['stest_geo']
   size = int(stest['stest_size'])
   k    = float(stest['stest_k'])

   ## Run and write in .csv file
   spect = spectrum(cdd, geo, size, k)
   with open('spectrum.csv', 'w') as csvfile:
      writer = csv.writer(csvfile, delimiter=';')
      for i in spect:
         writer.writerow([i.real, i.imag])
