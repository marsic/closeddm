#!/usr/bin/env python
#coding=utf-8
"""
osm.py, convergence test w.r.t. optimized Schwarz transmission condition
"""

import csv
import numpy
import re
import sys
import tempfile
from   utils import argument, option, info, temp, mesh, closeddm


def osm(cdd, geo, ndom, size, k):
   """Returns the convergence history of the optimized Schwarz method (OSM).
   Input:
     * cdd, the options to pass to closeddm (without the '-')
     * geo, the .geo file with the model to test the OSM on
     * ndom, the number of subdomains
     * size, the mesh sizes (in elements per wavelength)
     * k, the wavenumber of the wave problem
   """
   ## Options
   cdd = cdd | {'k': k, 'nDom': ndom}
   option(cdd)

   ## Info
   info('OSM', {'geo': geo, 'k': k, 'size': size, 'ndom': ndom}, cdd)

   ## Run
   hist = list()
   with tempfile.TemporaryDirectory() as tmp:
      geofile, mshfile, dbfile = temp(geo, tmp)

      msh, err = mesh(geofile, size, cdd, tmp)
      ddm, err = closeddm(mshfile, dbfile, False, tmp)

      for i in re.findall(r'(?<=absolute residual = ).+', ddm):
         hist.append(float(i.split(',')[0]))

   ## Print
   print('# Done!')
   print(' -->: ' + str(len(hist)) + ' iterations')

   ## Done
   return hist


if __name__ == "__main__":
   ## Arguments
   otest, cdd = argument(sys.argv, 'otest',
                         ['otest_geo', 'otest_nDom', 'otest_size', 'otest_k'])
   geo  = otest['otest_geo']
   ndom = otest['otest_nDom']
   size = int(otest['otest_size'])
   k    = float(otest['otest_k'])

   ## Run
   hist = osm(cdd, geo, ndom, size, k)

   ## Save residual in .csv file
   with open('residual.csv', 'w') as csvfile:
      writer = csv.writer(csvfile)
      for i in hist:
         writer.writerow([i])
