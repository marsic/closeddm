import pytest

def pytest_addoption(parser):
    parser.addoption('--fast', action='store_true', default=False,
                     help='Faster tests that do not check for exactness')


@pytest.fixture
def fast(request):
    return request.config.getoption('--fast')
