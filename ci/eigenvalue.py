#!/usr/bin/env python
#coding=utf-8
"""
eigenvalue.py, solves eigenvalue problems
"""

import csv
import os
import sys
import tempfile
from   utils import argument, option, info, temp, mesh, closeddm


def eigenvalue(cdd, geo, size, order, k, full):
    """Computes some eigenvalues of the given problem.
    Input:
     * cdd, the options to pass to closeddm (without the '-')
     * geo, the .geo file with the model to extract the modes from
     * size, the mesh size (in element per wavelength, see below)
     * order, the finite element *and* mesh order
     * k, the wavenumber for the mesh target wavelength (see above)
     * full, uses a direct solver if true, uses the Schwarz one otherwise
    """
    ## Options
    cdd = cdd | {'k': k, 'order': order, 'eigenmode': 1, 'cavity': 1}
    option(cdd)

    ## Info
    info('eigenvalue', {'geo': geo, 'size': size, 'order': order, 'k': k,
                        'full': full}, cdd)

    ## Run
    eig = list()
    res = list()
    with tempfile.TemporaryDirectory() as tmp:
        geofile, mshfile, dbfile = temp(geo, tmp)

        msh, err = mesh(geofile, size, cdd, tmp)
        ddm, err = closeddm(mshfile, dbfile, full, tmp)

        with open(os.path.join(tmp, 'eig.csv'), mode ='r') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for l in reader:
                eig.append(complex(float(l[0]), float(l[1])))
                res.append(float(l[2]))

    ## Print
    print('# Eigenvalues:')
    for i, j in zip(eig, res):
        print(' --> {0: .5e} | {1: .2e}'.format(i, j))

    ## Return
    return eig, res


if __name__ == "__main__":
   ## Arguments
   etest, cdd = argument(sys.argv, 'etest', ['etest_geo',
                                             'etest_size', 'etest_order',
                                             'etest_k', 'etest_full'])
   geo   = etest['etest_geo']
   size  = int(etest['etest_size'])
   order = int(etest['etest_order'])
   k     = float(etest['etest_k'])
   full  = etest['etest_full'] == '1'

   ## Run
   eigenvalue(cdd, geo, size, order, k, full)
