from   convergence import convergence, validate
import pytest

geo   = '../test/rectangle.geo'
size  = [4, 8, 16]
order = [2, 3]
ref   = 'rectangle'
tol   = 1
nDom  = 2
cdd   = [{'tcName': 'oo0',  'tcVar': 'open',   'nAux': 0 },
         {'tcName': 'oo2',  'tcVar': 'open',   'nAux': 0 },
         {'tcName': 'pade', 'tcVar': 'open',   'nAux': 32},
         {'tcName': 'oo0',  'tcVar': 'closed', 'nAux': 0 },
         {'tcName': 'ml',   'tcVar': 'closed', 'nAux': 32},
         {'tcName': 'pade', 'tcVar': 'closed', 'nAux': 32}]

fovr = {'order': [1],  ## Override for fast version ##
        'size':  [2]}

scenario = list()
for c in [0, 1]:
    tmp = [x.copy() for x in cdd]
    for t in tmp:
        t['cavity'] = c
    scenario = scenario + tmp


@pytest.mark.parametrize("opt", scenario)
def test_convergence_osm(opt, fast):
    ## Fast or full?
    mysize  = size  if not fast else fovr['size']
    myorder = order if not fast else fovr['order']

    ## FE convergence
    error, slope = convergence(opt | {'nDom': nDom},
                               geo, mysize, myorder, ref, False)

    ## Check
    if not fast:
        validate(slope, myorder, tol)

    ## Done
    print('# OK :-)')

@pytest.mark.parametrize("cavity", [0, 1])
def test_convergence_full(cavity, fast):
    ## Fast or full?
    mysize  = size  if not fast else fovr['size']
    myorder = order if not fast else fovr['order']

    ## FE convergence
    error, slope = convergence({'nDom': nDom, 'cavity': cavity},
                               geo, mysize, myorder, ref, True)

    ## Check
    if not fast:
        validate(slope, myorder, tol)

    ## Done
    print('# OK :-)')
