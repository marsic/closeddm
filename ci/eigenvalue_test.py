from   eigenvalue import eigenvalue
import numpy
import pytest

case = {'geo':  '../test/rectangle.geo',
        'lx':   0.5,
        'ly':   0.25,
        'lz':   -1,
        'k':    25,
        'size': [4, 8, 16, 32],
        'path': 'circle(31, 0, 1, 10)',
        'mode': [3, 2, 0]}

common = {'maxIt':  1000,
          'nAux':      8,
          'nDom':      8,
          'order':     2,
          'eig_maxIt': 10,
          'tol':       5}

solver = [{'full': True},
          {'full': False, 'tcName': 'oo0',  'tcVar': 'open'},
          {'full': False, 'tcName': 'oo2',  'tcVar': 'open'},
          {'full': False, 'tcName': 'pade', 'tcVar': 'open'},
          {'full': False, 'tcName': 'oo0',  'tcVar': 'closed'},
          {'full': False, 'tcName': 'ml',   'tcVar': 'closed'},
          {'full': False, 'tcName': 'pade', 'tcVar': 'closed'}]

fovr = {'order':     1,  ## Override for fast version ##
        'size':      [4],
        'nDom':      2,
        'maxIt':     3,
        'eig_maxIt': 1}

scenario = list()
for f in ['hierarchical', 'lagrange']:
    tmp = [common | x.copy() for x in solver]
    for t in tmp:
        t['family'] = f
    scenario = scenario + tmp


@pytest.mark.parametrize("opt", scenario)
def test_eigenvalue(opt, fast):
    ## Override with fast test cases?
    if(fast):
        for i in opt:
            if i in fovr:
                opt[i] = fovr[i]
        for i in case:
            if i in fovr:
                case[i] = fovr[i]

    ## Compute reference from the given mode
    mode = case['mode']
    ref  = rectangle([case['lx'], case['ly'], case['lz']], mode)

    ## Compute eigenvalues and errors w.r.t. to reference
    eig = list()
    res = list()
    err = list()
    for i in case['size']:
        e, r = eigenvalue(opt | {'cim_path': case['path']},
                          case['geo'], i, opt['order'], case['k'], opt['full'])
        myE = e[0]
        myR = r[0];
        eig.append(myE);
        res.append(myR);
        err.append(abs(myE-ref)/abs(ref))
        print('# Error with respect to {0} mode: {1: .4e}'.format(str(mode),
                                                                  err[-1]))

    ## Slope
    slope = numpy.diff(numpy.log(err)) / numpy.diff(numpy.log(case['size']))
    print('# Slopes:')
    print(slope)

    ## Check
    if not fast:
        target = 2*opt['order'] - 2*opt['order']*opt['tol']/100
        assert -numpy.mean(slope) > target, \
            'Bad (mean) slope for order '+str(opt['order'])

    ## Done
    print('# OK :-)')


def rectangle(l, n):
    return numpy.sqrt((numpy.pi*n[0]/l[0])**2 + \
                      (numpy.pi*n[1]/l[1])**2 + \
                      (numpy.pi*n[2]/l[2])**2)
