from   valgrind import valgrind
import pytest

case = {'geo':  '../test/rectangle.geo',
        'size': 2}

common = {'k':    31,
          'nAux':  2,
          'nDom':  3,
          'order': 2,
          'maxIt': 3}

solver = [{'full': True},
          {'full': False, 'tcName': 'oo0',  'tcVar': 'open'},
          {'full': False, 'tcName': 'oo2',  'tcVar': 'open'},
          {'full': False, 'tcName': 'pade', 'tcVar': 'open'},
          {'full': False, 'tcName': 'oo0',  'tcVar': 'closed'},
          {'full': False, 'tcName': 'ml',   'tcVar': 'closed'},
          {'full': False, 'tcName': 'pade', 'tcVar': 'closed'}]

scenario = [common | x.copy() for x in solver]


@pytest.mark.parametrize("opt", scenario)
def test_valgrind(opt, fast):
    if not fast:
        err, msg = valgrind(opt,
                            case['geo'], case['size'], opt['k'], opt['full'])
        assert err == 0, msg
        print('# OK :-)')
