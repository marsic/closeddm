// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ANALYTIC_HELMHOLTZ_RECTANGLE_H_
#define _CLOSEDDM_ANALYTIC_HELMHOLTZ_RECTANGLE_H_

#include "AnalyticModal.h"
#include "Parameter.h"

namespace closeddm{
  namespace analytic{
    /// Base class for Helmholtz AnalyticModal solutions on rectangles
    template<class T>
    class AnalyticHelmholtzRectangle:
      public AnalyticModal<T, gmshfem::Degree::Degree0>{
    protected:
      T k;      // Wavenumber
      double L; // Length
      double h; // Height

    public:
      virtual ~AnalyticHelmholtzRectangle(void) = 0;

    protected:
      void init(const closeddm::input::Parameter &param);
    };
  };
};

#endif
