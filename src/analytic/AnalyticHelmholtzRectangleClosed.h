// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ANALYTIC_HELMHOLTZ_RECTANGLE_CLOSED_H_
#define _CLOSEDDM_ANALYTIC_HELMHOLTZ_RECTANGLE_CLOSED_H_

#include "AnalyticHelmholtzRectangle.h"

namespace closeddm{
  namespace analytic{
    /// Name a AnalyticHelmholtzRectangle for cavity problems
    template<class T>
    class AnalyticHelmholtzRectangleClosed final:
      public AnalyticHelmholtzRectangle<T>{
    public:
      /// Constructs a new AnalyticHelmholtzRectangleClosed with the given
      /// Parameter `param`, whose relevant values are:
      ///  - "k", the real part of the wavenumber
      ///  - "kd", the imaginary part of the wavenumber
      ///  - "lx", the 'x' dimension of the domain
      ///  - "ly", the 'y' dimension of the domain
      ///  - "ny", the mode number in the 'y' direction
      ///  - "allmodes", if this flag is true all modes
      ///    from 1 to "ny" are summed up
      AnalyticHelmholtzRectangleClosed(const closeddm::input::Parameter &param);
      ~AnalyticHelmholtzRectangleClosed(void);

    protected:
      virtual T u(double x, double y, double z, int n) const override final;
    };
  };
};

#endif
