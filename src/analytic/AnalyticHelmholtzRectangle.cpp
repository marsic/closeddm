// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmsh.h"

#include "AnalyticHelmholtzRectangle.h"
#include "CloseDDM.h"
#include "Utils.h"

using namespace closeddm;
using namespace closeddm::analytic;
using namespace closeddm::input;
using namespace closeddm::utils;

template<class T>
AnalyticHelmholtzRectangle<T>::~AnalyticHelmholtzRectangle(void){
}

template<class T>
void AnalyticHelmholtzRectangle<T>::init(const Parameter &param){
  // Check dimension //
  int dim = gmsh::model::getDimension();
  if(dim != 2)
    throw gmshfem::common::Exception("AnalyticHelmholtzRectangle: " +
                                     std::to_string(dim) +
                                     "D case not implemented, yet...");

  // Bounding box //
  double xmin, ymin, zmin, xmax, ymax, zmax;
  gmsh::model::getBoundingBox(-1, -1, xmin, ymin, zmin, xmax, ymax, zmax);
  this->x0[0] = xmin;
  this->x0[1] = ymin;
  this->x0[2] = zmin;

  // Problem //
  this->k = complexify(param.getNumber("k"), param.getNumber("kd"));
  this->L = param.getNumber("lx");
  this->h = param.getNumber("ly");

  // Source //
  this->N        = param.getNumber("ny");
  this->allmodes = param.getFlag("allmodes");
}

// Explicit instantiation //
////////////////////////////
template class AnalyticHelmholtzRectangle<cmplx>;
