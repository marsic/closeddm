// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/AnalyticalFunction.h"

#include "AnalyticFactory.h"
#include "AnalyticHelmholtzRectangleClosed.h"
#include "AnalyticHelmholtzRectangleOpen.h"

using namespace closeddm;
using namespace closeddm::input;
using namespace gmshfem::analytics;

SFunction analytic::factory(const Parameter &param){
  // Which case //
  std::string kind = param.getString("error");
  bool cavity = param.getFlag("cavity");

  // Factory //
  SFunction fAna;
  if(kind == "rectangle" && cavity)
    fAna = AnalyticalFunction<AnalyticHelmholtzRectangleClosed<cmplx> >(param);
  else if(kind == "rectangle" && !cavity)
    fAna = AnalyticalFunction<AnalyticHelmholtzRectangleOpen<cmplx> >(param);
  else
    throw gmshfem::common::Exception("Unknown closed form solution: " +
                                     kind +
                                     " (" + (cavity? "closed" : "open") + ")");

  return fAna;
}
