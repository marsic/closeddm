// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "AnalyticHelmholtzRectangleClosed.h"
#include "CloseDDM.h"
#include "Constant.h"

using namespace closeddm;
using namespace closeddm::analytic;
using namespace closeddm::constant;
using namespace closeddm::input;

template<class T>
AnalyticHelmholtzRectangleClosed<T>::
AnalyticHelmholtzRectangleClosed(const Parameter &param){
  this->init(param);
}

template<class T>
AnalyticHelmholtzRectangleClosed<T>::
~AnalyticHelmholtzRectangleClosed(void){
}

template<class T>
T AnalyticHelmholtzRectangleClosed<T>::
u(double x, double y, double z, int n) const{
  const T      &k = this->k;
  const double &L = this->L;
  const double &h = this->h;

  T ky = n * Pi / h;
  T kx = sqrt(k*k - ky*ky);
  return sin(ky * y) * sin(kx*(L - x)) / sin(kx*L);
}

// Explicit instantiation //
////////////////////////////
template class AnalyticHelmholtzRectangleClosed<cmplx>;
