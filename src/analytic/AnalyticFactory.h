// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ANALYTIC_FACTORY_H_
#define _CLOSEDDM_ANALYTIC_FACTORY_H_

#include "CloseDDM.h"
#include "Parameter.h"

namespace closeddm{
  /// Analytic (closed form) solution
  namespace analytic{
    /// A factory for closed form solutions,
    /// as defined by the Parameter `param`, whose relevant values are:
    ///  - "cavity", if `true`, a cavity solution is to be computed
    ///  - "error", the kind of analytic solution, that is
    ///    + "rectangle", for a rectangular waveguide/cavity (see "cavity")
    SFunction factory(const closeddm::input::Parameter &param);
  };
};

#endif
