// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/KahanSum.h"

#include "AnalyticModal.h"
#include "CloseDDM.h"

using namespace closeddm;
using namespace closeddm::analytic;

template<class T, gmshfem::Degree D>
AnalyticModal<T, D>::~AnalyticModal(void){
}

template<class T, gmshfem::Degree D>
void AnalyticModal<T, D>::operator()(gmshfem::function::OutputVector<T, D> &val,
                                     const std::vector<double> &pnt) const{
  for(size_t i = 0; i < val.size(); i++){
    const double x = pnt[3 * i]     - x0[0];
    const double y = pnt[3 * i + 1] - x0[1];
    const double z = pnt[3 * i + 2] - x0[2];

    typename gmshfem::MathObject<T, D>::Object v;
    if(allmodes){
      gmshfem::common::KahanSum<T> sum(0.);
      for(int n = 1; n <= N; n++)
        sum += u(x, y, z, n);
      v = sum.sum();
    }
    else{
      v = u(x, y, z, N);
    }

    val[i] = v;
  }
}

// Explicit instantiation //
////////////////////////////
template class AnalyticModal<cmplx, gmshfem::Degree::Degree0>;
