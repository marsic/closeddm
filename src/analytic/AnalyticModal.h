// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ANALYTIC_MODAL_H_
#define _CLOSEDDM_ANALYTIC_MODAL_H_

#include "gmshfem/AnalyticalNode.h"

namespace closeddm{
  namespace analytic{
    /// Base class for analytic (closed form) solutions based on a modal sum

    /// @tparam T the scalar type of this AnalyticModal
    /// @tparam D the gmshfem::Degree of this AnalyticModal
    template<class T, gmshfem::Degree D>
    class AnalyticModal: public gmshfem::function::AnalyticalOperation<T, D>{
    protected:
      double x0[3];  // XYZ-coordinates of the axes reference
      int N;         // Mode number (excitation on the left bndry of the cavity)
      bool allmodes; // Take all modes from [1, N] or only the Nth one?

    public:
      /// Destructor
      virtual ~AnalyticModal(void) = 0;

      /// Populates the OutputVector<T, D> `val` with the values of this
      /// AnalyticModal evaluated at a given set of points `pnt`
      /// (i.e. a vector with the unrolled 3D coordinates of each point)
      void operator()(gmshfem::function::OutputVector<T, D> &val,
                      const std::vector<double> &pnt) const override;

    protected:
      virtual T u(double x, double y, double z, int n) const = 0;
    };
  };
};

#endif
