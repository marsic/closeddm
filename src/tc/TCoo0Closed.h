// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_OO0_CLOSED_H_
#define _CLOSEDDM_TC_OO0_CLOSED_H_

#include "TCoo0.h"

namespace closeddm{
  namespace tc{
    /// Optimized zeroth-order (OO0) transmission condition for closed problems

    /// @see N. Marsic et al., "Transmission operators for the non-overlapping
    ///      Schwarz method for solving Helmholtz problems
    ///      in rectangular cavities", arXiv preprint, 2022,
    ///      [arXiv:2205.06518](https://arxiv.org/abs/2205.06518)
    class TCoo0Closed final: public TCoo0{
    public:
      /// Constructs a new OO0 TC for closed problems of size `L` with the given
      /// wavenumber `k`, complex shift `gamma` (percentage of `k`),
      /// neighbourhoud map `neighbour` and interfaces `sigma`
      TCoo0Closed(cmplx k, double gamma, double L,
                  const std::vector<std::vector<unsigned int> > &neighbour,
                  const gmshddm::domain::Interface &sigma);
      ~TCoo0Closed(void);
    };
  }
}

#endif
