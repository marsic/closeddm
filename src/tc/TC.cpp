// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TC.h"

using namespace closeddm::tc;

TC::~TC(void){
}

void TC::scale(double S){
  this->S = S;
}

std::string TC::info(void) const{
  std::string s = variant + "_" + name;

  if(gamma != 0.)
    s += "-Reg" + std::to_string(int(gamma));
  if(S != 1.)
    s += " (scaling factor: " + std::to_string(S) + ")";

  return s;
}

std::string TC::nameWithAux(std::string name, int nAux){
  return name + std::to_string(nAux);
}
