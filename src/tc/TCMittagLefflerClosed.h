// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_MITTAGLEFFLER_CLOSED_H_
#define _CLOSEDDM_TC_MITTAGLEFFLER_CLOSED_H_

#include "TCRational.h"

namespace closeddm{
  namespace tc{
    /// Mittag-Leffler-localized (ML) TC for closed problems

    /// @see N. Marsic et al., "Transmission operators for the non-overlapping
    ///      Schwarz method for solving Helmholtz problems
    ///      in rectangular cavities", arXiv preprint, 2022,
    ///      [arXiv:2205.06518](https://arxiv.org/abs/2205.06518)
    class TCMittagLefflerClosed final: public TCRational{
    public:
      /// Constructs a new ML TC for closed problems of size `L`
      /// with the given wavenumber `k`, complex shift `gamma`
      /// (percentage of `k`), number `nAux` of auxiliary fields
      /// of order `order` and family `family`, neighbourhoud map `neighbour`,
      /// interfaces `sigma` and their boundary `dSigma`
      TCMittagLefflerClosed(cmplx k, double gamma, double L,
                            int nAux, int order, std::string family,
                            const std::vector<std::vector<unsigned int> >
                                                                     &neighbour,
                            const gmshddm::domain::Interface &sigma,
                            const gmshddm::domain::Interface &dSigma);
      ~TCMittagLefflerClosed(void);
    };
  }
}

#endif
