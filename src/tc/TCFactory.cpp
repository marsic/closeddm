// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCFactory.h"

#include "Constant.h"
#include "TCoo0Open.h"
#include "TCoo0Closed.h"
#include "TCoo2Open.h"
#include "TCPadeOpen.h"
#include "TCPadeClosed.h"
#include "TCMittagLefflerClosed.h"
#include "Utils.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace closeddm::input;
using namespace gmshddm::domain;
using namespace gmshfem::common;
using namespace std;

list<tc::TC*> tc::factory(const Parameter &param,
                          const vector<vector<unsigned int> > &neighbour,
                          const Interface &sigma,
                          const Interface &dSigma){
  // TC type
  string name = param.getString("tcName");
  string  var = param.getString("tcVar");

  // Wavenumber and complex factor
  double gamma = param.getNumber("gamma");
  cmplx      k = utils::complexify(param.getNumber("k"), param.getNumber("kd"));

  // OO2 Stuffs
  double kMin  = 0;
  double kMax  = Pi / param.getNumber("lc");
  double delta = Pi / param.getNumber("ly");

  // Rational stuffs
  int    order  = param.getNumber("order");
  string family = param.getString("family");
  int     nAux  = param.getNumber("nAux");
  double theta  = param.getNumber("theta");
  double     L  = param.getNumber("lx");

  // Mixed stuff
  int nAuxOpen = utils::calc(nAux, param.getString("mixAux"));
  double  frac = param.getNumber("mixPro");
  if(var == "mixed" && (frac <= 0 || frac >= 1))
    throw Exception("Mixed Pade TC: "
                    "proportion of closed TC must be in the range ]0, 1[");

  // Shorter names
  const vector<vector<unsigned int> > &nb = neighbour;
  const Interface                     &s  = sigma;
  const Interface                     &dS = dSigma;

  // TC per say
  list<TC*> tc;
  if(name == "oo0" && var == "open")
    tc.push_back(new TCoo0Open(k, gamma, nb));
  else if(name == "oo0" && var == "closed")
    tc.push_back(new TCoo0Closed(k, gamma, L, nb, s));
  else if(name == "oo0" && var == "mixed"){
    tc.push_back(new TCoo0Closed(k, 0, L, nb, s));
    tc.back()->scale(frac);

    tc.push_back(new TCPadeOpen(k, 0, theta, nAuxOpen, order, family,
                                nb, s, dS));
    tc.back()->scale(1-frac);
  }

  else if(name == "oo2" && var == "open")
    tc.push_back(new TCoo2Open(k, kMin, kMax, delta));

  else if(name == "ml" && var == "closed")
    tc.push_back(new TCMittagLefflerClosed(k, gamma, L, nAux, order, family,
                                           nb, s,dS));
  else if(name == "ml" && var == "mixed"){
    tc.push_back(new TCMittagLefflerClosed(k, 0, L, nAux, order, family,
                                           nb, s,dS));
    tc.back()->scale(frac);

    tc.push_back(new TCPadeOpen(k, 0, theta, nAuxOpen, order, family,
                                nb, s, dS));
    tc.back()->scale(1-frac);
  }

  else if(name == "pade" && var == "open")
    tc.push_back(new TCPadeOpen(k, gamma, theta, nAux, order, family,
                                nb, s, dS));
  else if(name == "pade" && var == "closed")
    tc.push_back(new TCPadeClosed(k, gamma, L, nAux, order, family, nb, s, dS));
  else if(name == "pade" && var == "mixed"){
    tc.push_back(new TCPadeClosed(k, 0, L, nAux, order, family, nb, s, dS));
    tc.back()->scale(frac);

    tc.push_back(new TCPadeOpen(k, 0, theta, nAuxOpen, order, family,
                                nb, s, dS));
    tc.back()->scale(1-frac);
  }
  else
    throw Exception("Unknown transmission condition " + var + "_" + name);

  // Done
  return tc;
}
