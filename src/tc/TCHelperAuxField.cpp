// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "Utils.h"
#include "TCHelperAuxField.h"

using namespace closeddm;
using namespace gmshddm::domain;
using namespace gmshfem::field;
using namespace std;

void tc::auxField::build(string name,
                         NMap<std::vector<Field0> > &phi,
                         unsigned int nAux,
                         std::string family,
                         unsigned int order,
                         const vector<vector<unsigned int> > &neighbour,
                         const Interface &sigma,
                         const Interface &dSigma){
  FunctionSpaceTypeForm0 Hgrad =
    utils::functionspace::family<FunctionSpaceTypeForm0>(family);

  size_t nDom = neighbour.size();
  phi.resize(nDom);
  for(size_t i = 0; i < nDom; i++){
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      phi[i][j].resize(nAux);
      for(unsigned int n = 0; n < nAux; n++)
        phi[i][j][n] = Field0(name+"_"+to_string(i)
                                  +"_"+to_string(j)
                                  +"_"+to_string(n), sigma(i, j), Hgrad, order);
    }
  }

  for(size_t i = 0; i < nDom; i++){
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      for(unsigned int n = 0; n < nAux; n++)
        phi[i][j][n].addConstraint(dSigma(i, j), 0);
    }
  }
}

void tc::auxField::build(string name,
                         NMap<std::vector<Field0> > &phi,
                         vector<unordered_map<unsigned int,
                                              unsigned int> > &nAux,
                         std::string family,
                         unsigned int order,
                         const vector<vector<unsigned int> > &neighbour,
                         const Interface &sigma,
                         const Interface &dSigma){
  FunctionSpaceTypeForm0 Hgrad =
    utils::functionspace::family<FunctionSpaceTypeForm0>(family);

  size_t nDom = neighbour.size();
  phi.resize(nDom);
  for(size_t i = 0; i < nDom; i++){
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      phi[i][j].resize(nAux[i][j]);
      for(unsigned int n = 0; n < nAux[i][j]; n++)
        phi[i][j][n] = Field0(name+"_"+to_string(i)
                                  +"_"+to_string(j)
                                  +"_"+to_string(n), sigma(i, j), Hgrad, order);
    }
  }

  for(size_t i = 0; i < nDom; i++){
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      for(unsigned int n = 0; n < nAux[i][j]; n++)
        phi[i][j][n].addConstraint(dSigma(i, j), 0);
    }
  }
}
