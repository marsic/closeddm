// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_OO0_H_
#define _CLOSEDDM_TC_OO0_H_

#include "TC.h"

namespace closeddm{
  namespace tc{
    /// Abstract class for zeroth-order transmission conditions
    class TCoo0: public TC{
    protected:
      // Hack: mutable to allow operator[] (unordered_map) in const methods
      //       --> I will keep constness myself, I promise...
      mutable NMap<SFunction> C0;  // Constant term

    public:
      virtual ~TCoo0(void) = 0;
      virtual void interface(gmshfem::problem::Formulation<cmplx> &f,
                             Field0 &ui,
                             const gmshfem::domain::Domain &sij,
                             const unsigned int i,
                             const unsigned int j,
                             const std::string &g) const override final;

      virtual void update(gmshfem::problem::Formulation<cmplx> &f,
                          Field0 &ui,
                          Field0 &gij,
                          const gmshfem::domain::Domain &sij,
                          const unsigned int i,
                          const unsigned int j,
                          const std::string &g) const override final;
    };
  }
}

#endif
