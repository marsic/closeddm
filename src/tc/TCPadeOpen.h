// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_PADE_OPEN_H_
#define _CLOSEDDM_TC_PADE_OPEN_H_

#include "TCRational.h"

namespace closeddm{
  namespace tc{
    /// Padé-localized (PADE) TC for open problems

    /// @see Y. Boubendir et al., "A quasi-optimal non-overlapping domain
    ///      decomposition algorithm for the Helmholtz equation",
    ///      Journal of Computational Physics,
    ///      vol. 231, no. 2, pp. 262-280, 2012,
    ///      [DOI:10.1016/j.jcp.2011.08.007](
    ///        https://doi.org/10.1016/j.jcp.2011.08.007)
    class TCPadeOpen final: public TCRational{
    public:
      /// Constructs a new PADE TC for open problems with the given
      /// wavenumber `k`, complex shift `gamma` (percentage of `k`),
      /// branch cut angle of the (complex) square root `theta`,
      /// number `nAux` of auxiliary fields of order `order`
      /// and family `family`, neighbourhoud map `neighbour`, interfaces `sigma`
      /// and their boundary `dSigma`
      TCPadeOpen(cmplx k, double gamma, double theta,
                 int nAux, int order, std::string family,
                 const std::vector<std::vector<unsigned int> > &neighbour,
                 const gmshddm::domain::Interface &sigma,
                 const gmshddm::domain::Interface &dSigma);
      ~TCPadeOpen(void);
    };
  }
}

#endif
