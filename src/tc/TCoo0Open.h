// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_OO0_OPEN_H_
#define _CLOSEDDM_TC_OO0_OPEN_H_

#include "TCoo0.h"

namespace closeddm{
  namespace tc{
    /// Optimized zeroth-order (OO0) transmission condition for open problems

    /// @see
    ///   - B. Després, "Décomposition de domaine et problème de Helmholtz",
    ///     Comptes Rendus de l'Académie des Sciences,
    ///     vol. 311, pp. 313-316, 1990, [ARK:/12148/bpt6k57815213](
    ///       https://gallica.bnf.fr/ark:/12148/bpt6k57815213)
    ///   - Y. Boubendir, "An analysis of the BEM-FEM non-overlapping domain
    ///     decomposition method for a scattering problem",
    ///     Journal of Computational and Applied Mathematics ,
    ///     vol. 204, no. 2, pp. 281-291, 2007, [DOI:10.1016/j.cam.2006.02.044](
    ///       https://doi.org/10.1016/j.cam.2006.02.044)
    class TCoo0Open final: public TCoo0{
    public:
      /// Constructs a new OO0 TC for open problems with the given
      /// wavenumber `k`, complex shift `gamma` (percentage of `k`) and
      /// neighbourhoud map `neighbour`
      TCoo0Open(cmplx k, double gamma,
                const std::vector<std::vector<unsigned int> > &neighbour);
      ~TCoo0Open(void);
    };
  }
}

#endif
