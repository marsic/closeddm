// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_OO2_OPEN_H_
#define _CLOSEDDM_TC_OO2_OPEN_H_

#include "TCoo2.h"

namespace closeddm{
  namespace tc{
    /// Optimized second-order (OO2) transmission condition for open problems

    /// @see M. J. Gander et al., "Optimized Schwarz methods without overlap
    ///      for the Helmholtz equation", SIAM Journal on Scientific Computing,
    ///      vol. 24, no. 1, pp. 38-60, 2002, [DOI:10.1137/S1064827501387012](
    ///        https://doi.org/10.1137/S1064827501387012)
    class TCoo2Open final: public TCoo2{
    public:
      /// Constructs a new OO2 TC for open problems with the given
      /// wavenumber `k`.
      /// The parameters `kMin`, `kMax` and `delta` are the optimization
      /// parameters of the OO2 TC, see equation (3.3) from the aforementioned
      /// reference, where `kMin`\f$=k_\text{min}\f$, `kMax`\f$=k_\text{max}\f$
      /// and `delta`\f$=(\omega_+-\omega_-)\f$
      TCoo2Open(cmplx k, cmplx kMin, cmplx kMax, double delta);
      ~TCoo2Open(void);
    };
  }
}

#endif
