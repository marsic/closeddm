// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCoo2.h"

using namespace closeddm::tc;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::problem;

TCoo2::~TCoo2(void){
}

void TCoo2::interface(Formulation<cmplx> &f,
                      Field0 &ui,
                      const Domain &sij,
                      const unsigned int i,
                      const unsigned int j,
                      const std::string &g) const{
  f.integral(+S*A *   dof(ui),    tf(ui),  sij, g);
  f.integral(-S*B * d(dof(ui)), d(tf(ui)), sij, g);
}

void TCoo2::update(Formulation<cmplx> &f,
                   Field0 &ui,
                   Field0 &gij,
                   const Domain &sij,
                   const unsigned int i,
                   const unsigned int j,
                   const std::string &g) const{
  f.integral(-2.*S*A *   ui,    tf(gij),  sij, g);
  f.integral(+2.*S*B * d(ui), d(tf(gij)), sij, g);
}
