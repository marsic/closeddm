// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCoo0Closed.h"
#include "TCHelperSubSize.h"
#include "Constant.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::tc;
using namespace gmshddm::domain;
using namespace std;

TCoo0Closed::TCoo0Closed(cmplx k, double gamma, double L,
                         const vector<vector<unsigned int> > &neighbour,
                         const Interface &sigma){
  // Namespace //
  using gmshfem::function::tan;

  // Init //
  this->gamma   = gamma;
  this->S       = 1;
  this->name    = "oo0";
  this->variant = "closed";
  C0.resize(neighbour.size());

  // Size of complement domains (ld) //
  NMap<SFunction> ld;
  subSize::build(ld, neighbour, sigma, L);

  // Regularization with constant damping //
  cmplx R = utils::damping(k, gamma);

  // TC per say //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      this->C0[i][neighbour[i][j]] = k * 1./tan(k*ld[i][neighbour[i][j]]) + R;
  //                        --> cot(x) = 1./tan(x)
}

TCoo0Closed::~TCoo0Closed(void){
}
