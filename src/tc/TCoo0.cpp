// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCoo0.h"

using namespace closeddm::tc;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::problem;

TCoo0::~TCoo0(void){
}

void TCoo0::interface(Formulation<cmplx> &f,
                      Field0 &ui,
                      const Domain &sij,
                      const unsigned int i,
                      const unsigned int j,
                      const std::string &g) const{
  f.integral(S*C0[i][j] * dof(ui), tf(ui), sij, g);
}

void TCoo0::update(Formulation<cmplx> &f,
                   Field0 &ui,
                   Field0 &gij,
                   const Domain &sij,
                   const unsigned int i,
                   const unsigned int j,
                   const std::string &g) const{
  f.integral(-S*C0[i][j] * ui, tf(gij), sij, g); // S[i][j](u(i))
  f.integral(-S*C0[j][i] * ui, tf(gij), sij, g); // S[j][i](u(i))
}
