// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_H_
#define _CLOSEDDM_TC_H_

#include "gmshddm/Formulation.h"
#include "CloseDDM.h"

namespace closeddm{
  /// Transmission condition (TC) of the optimized Schwarz
  namespace tc{
    /// Base class for all optimzed Schwarz transmission conditions (TCs)

    /// Each TC defines
    ///    i) an "interface" term at the artificial boundary
    ///       of a subproblem and
    ///   ii) an "update" term for computing the artificial source
    ///       at the the aforementioned boundary.
    ///
    /// Formaly, let \f$\mathcal{S_{ij}}\f$ be the transmission operator
    /// between the subdomains \f$\Omega_i\f$ and \f$\Omega_j\f$,
    /// \f$u_i\f$ the unknown (sub)field on \f$\Omega_i\f$,
    /// \f$g_{ij}\f$ the artificial source on \f$\Sigma_{ij}\f$
    /// (the artificial boundary between \f$\Omega_i\f$ and \f$\Omega_j\f$) and
    /// \f$\mathcal{H}\f$ is some suitable function space.
    /// This class defines the following two terms
    /// \f[
    ///   \int_{\Sigma_{ij}}\mathcal{S_{ij}}(u_i)u_i^\prime\mathrm{d}\Sigma_{ij}
    ///   \quad\forall u_i^\prime\in\mathcal{H}(\Omega_i)
    /// \f]
    /// and
    /// \f[
    ///   \int_{\Sigma_{ij}}
    ///      (\mathcal{S_{ji}}+\mathcal{S_{ij}})(u_i)
    ///      g_{ij}^\prime\mathrm{d}\Sigma_{ij}
    ///   \quad\forall g_{ij}^\prime\in\mathcal{H}(\Sigma_{ij}),
    /// \f]
    /// where the first integral is the "interface" term [Thierry2016, eq. 16]
    /// of the subproblem and where the second integral is
    /// the "update" term [Thierry2016, eq. 17] (see note below).
    ///
    /// @see [Thierry2016]: B. Thierry et al., "GetDDM: an open framework for
    ///   testing optimized Schwarz methods for time-harmonic wave problems",
    ///   Computer Physics Communications, vol. 203, pp. 309-330, 2016,
    ///   [DOI: 10.1016/j.cpc.2016.02.030](
    ///     https://doi.org/10.1016/j.cpc.2016.02.030)
    ///
    /// @note This reference, assumes that
    ///   \f$\mathcal{S_{ji}} = \mathcal{S_{ij}} = \mathcal{S}\f$, hence the
    ///   \f$2\mathcal{S}\f$ in equation (17)
    class TC{
    protected:
      std::string name;
      std::string variant;
      double gamma;
      double S;

    public:
      /// Destructor
      virtual ~TC(void) = 0;

      /// Scales this TC by a factor `S`
      void scale(double S);

      /// Adds the "interface" term of this TC on the given subproblem `f`,
      /// for the field `ui` on interface `sij` between subdomains `i` and `j`
      /// with the quadrature rule `g`
      virtual void interface(gmshfem::problem::Formulation<cmplx> &f,
                             Field0 &ui,
                             const gmshfem::domain::Domain &sij,
                             const unsigned int i,
                             const unsigned int j,
                             const std::string &g) const = 0;

      /// Adds the "update" term of this TC on the given subproblem `f`,
      /// for the field `ui` and the artificial source `gij`
      /// on interface `sij` between subdomains `i` and `j`
      /// with the quadrature rule `g`
      virtual void update(gmshfem::problem::Formulation<cmplx> &f,
                          Field0 &ui,
                          Field0 &gij,
                          const gmshfem::domain::Domain &sij,
                          const unsigned int i,
                          const unsigned int j,
                          const std::string &g) const = 0;

      /// Returns a string with some informations regarding this TC
      std::string info(void) const;

    protected:
      std::string nameWithAux(std::string name, int nAux);
    };
  }
}

#endif
