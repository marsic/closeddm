// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_RATIONAL_H_
#define _CLOSEDDM_TC_RATIONAL_H_

#include "TC.h"

namespace closeddm{
  namespace tc{
    /// Abstract class for rational transmission conditions
    class TCRational: public TC{
    protected:
      // Hack: mutable to allow operator[] (unordered_map) in const methods
      //       --> I will keep constness myself, I promise...
      mutable NMap<SFunction>               C;  // Constant term
      mutable NMap<std::vector<SFunction> > A;  // Num.   term * Phi[or]Psi
      mutable NMap<std::vector<SFunction> > Ad; // Num.   term * dPhi[or]dPsi
      mutable NMap<std::vector<SFunction> > B;  // Denom. term * Phi[or]Psi
      mutable NMap<std::vector<SFunction> > Bd; // Denom. term * dPhi[or]dPsi
      mutable NMap<SFunction>               Uc; // Constant update term
      mutable NMap<std::vector<SFunction> > Uu; // Update term * U
      mutable NMap<std::vector<SFunction> > Up; // Update term * Phi[or]Psi)

      mutable NMap<std::vector<Field0> > phi;   // Auxiliary field 'Phi'
      mutable NMap<std::vector<Field0> > psi;   // Auxiliary field 'Psi'

      cmplx R;                                  // Regularization term

    public:
      ~TCRational(void) = 0;
      virtual void interface(gmshfem::problem::Formulation<cmplx> &f,
                             Field0 &ui,
                             const gmshfem::domain::Domain &sij,
                             const unsigned int i,
                             const unsigned int j,
                             const std::string &g) const override final;

      virtual void update(gmshfem::problem::Formulation<cmplx> &f,
                          Field0 &ui,
                          Field0 &gij,
                          const gmshfem::domain::Domain &sij,
                          const unsigned int i,
                          const unsigned int j,
                          const std::string &g) const override final;

    protected:
      void init(const std::vector<std::vector<unsigned int> > &neighbour,
                int nAux);

      static void zero(NMap<SFunction> &v,
                       const std::vector<std::vector<unsigned int> > &neighbour);
      static void zero(NMap<std::vector<SFunction> > &v,
                       const std::vector<std::vector<unsigned int> > &neighbour,
                       int nAux);
    };
  }
}

#endif
