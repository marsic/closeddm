// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_HELPER_AUX_FIELD_H_
#define _CLOSEDDM_TC_HELPER_AUX_FIELD_H_

#include "gmshddm/Interface.h"
#include "gmshfem/Field.h"
#include "CloseDDM.h"

namespace closeddm{
  namespace tc{
    /// Helper function for constructing auxiliary fields (PADE and ML TC)
    namespace auxField{
      /// Populates `phi` with `nAux` auxiliary fields (of order `order`
      /// and function space family `family`) defined
      /// on each the interfaces `sigma` with boundary `dSigma`
      /// (neighbourhoud map given by `neighbour`) and with
      /// the base name `name`.
      /// Upon success, `phi[i][j][k]` is the `k`th auxiliary function
      /// defined on the interface between subdomains `i` and `j`,
      /// on the side of domain `i`.
      void build(std::string name,
                 NMap<std::vector<Field0> > &phi,
                 unsigned int nAux,
                 std::string family,
                 unsigned int order,
                 const std::vector<std::vector<unsigned int> > &neighbour,
                 const gmshddm::domain::Interface &sigma,
                 const gmshddm::domain::Interface &dSigma);

      /// Same as other build(), but now the number `nAux` of auxiliary variable
      /// can be different from interface to interface.
      /// In other words if `nAux[i][j] = K`, then `phi[i][j].size() == K`.
      void build(std::string name,
                 NMap<std::vector<Field0> > &phi,
                 std::vector<std::unordered_map<unsigned int,
                                                unsigned int> > &nAux,
                 std::string family,
                 unsigned int order,
                 const std::vector<std::vector<unsigned int> > &neighbour,
                 const gmshddm::domain::Interface &sigma,
                 const gmshddm::domain::Interface &dSigma);
    }
  }
}

#endif
