// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCRational.h"

using namespace closeddm::tc;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::function;
using namespace gmshfem::problem;

TCRational::~TCRational(void){
  C.clear();
  A.clear();
  Ad.clear();
  B.clear();
  Bd.clear();

  Uc.clear();
  Uu.clear();
  Up.clear();

  phi.clear();
  psi.clear();
}

void TCRational::interface(Formulation<cmplx> &f,
                           Field0 &ui,
                           const Domain &sij,
                           const unsigned int i,
                           const unsigned int j,
                           const std::string &g) const{
  f.integral(S*C[i][j] * dof(ui), tf(ui), sij, g);
  for(size_t n = 0; n < phi[i][j].size(); n++){
    f.integral(S*A[i][j][n]  *   dof(phi[i][j][n]),    tf(ui),  sij, g);
    f.integral(S*Ad[i][j][n] * d(dof(phi[i][j][n])), d(tf(ui)), sij, g);

    // Auxiliary problems for phi
    f.integral(               -dof(ui),              tf(phi[i][j][n]),  sij, g);
    f.integral(B[i][j][n]  *   dof(phi[i][j][n]),    tf(phi[i][j][n]),  sij, g);
    f.integral(Bd[i][j][n] * d(dof(phi[i][j][n])), d(tf(phi[i][j][n])), sij, g);
  }

  // Auxiliary problems for psi (pre-computed for later use in update law)
  for(size_t n = 0; n < psi[i][j].size(); n++){
    f.integral(               -dof(ui),              tf(psi[i][j][n]),  sij, g);
    f.integral(B[j][i][n]  *   dof(psi[i][j][n]),    tf(psi[i][j][n]),  sij, g);
    f.integral(Bd[j][i][n] * d(dof(psi[i][j][n])), d(tf(psi[i][j][n])), sij, g);
  }

  // Regularization with constant damping
  f.integral(S*R * dof(ui), tf(ui), sij, g);
}

void TCRational::update(Formulation<cmplx> &f,
                        Field0 &ui,
                        Field0 &gij,
                        const Domain &sij,
                        const unsigned int i,
                        const unsigned int j,
                        const std::string &g) const{
  // Contribution of S[i][j](u(i))
  f.integral(-S*Uc[i][j] * ui, tf(gij), sij, g);
  for(size_t n = 0; n < phi[i][j].size(); n++){
    f.integral(-S*Uu[i][j][n] * ui,           tf(gij), sij, g);
    f.integral(-S*Up[i][j][n] * phi[i][j][n], tf(gij), sij, g);
  }

  // Contribution of S[j][i](u(i))
  f.integral(-S*Uc[j][i] * ui, tf(gij), sij, g);
  for(size_t n = 0; n < psi[i][j].size(); n++){
    f.integral(-S*Uu[j][i][n] * ui,           tf(gij), sij, g);
    f.integral(-S*Up[j][i][n] * psi[i][j][n], tf(gij), sij, g);
  }

  // Constribution of regularization term
  f.integral(-2.*S*R * ui, tf(gij), sij, g);
}


// Other helpers //
void TCRational::init(const std::vector<std::vector<unsigned int> > &neighbour,
                      int nAux){
  zero(C,  neighbour);
  zero(A,  neighbour, nAux);
  zero(Ad, neighbour, nAux);
  zero(B,  neighbour, nAux);
  zero(Bd, neighbour, nAux);
  zero(Uc, neighbour);
  zero(Uu, neighbour, nAux);
  zero(Up, neighbour, nAux);
}

void TCRational::zero(NMap<SFunction> &v,
                      const std::vector<std::vector<unsigned int> > &neighbour){
  size_t nDom = neighbour.size();
  v.resize(nDom);

  for(size_t i = 0; i < nDom; i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      v[i][neighbour[i][j]] = 0.;
}

void TCRational::zero(NMap<std::vector<SFunction> > &v,
                      const std::vector<std::vector<unsigned int> > &neighbour,
                      int nAux){
  size_t nDom = neighbour.size();
  v.resize(nDom);
  for(size_t i = 0; i < nDom; i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      v[i][neighbour[i][j]].resize(nAux);

  for(size_t i = 0; i < nDom; i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        v[i][neighbour[i][j]][n] = 0.;
}
