// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "Constant.h"
#include "TCHelperPadeSquareRoot.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace std;

double tc::padeSquareRoot::aj(int j, int N){
  double tmp = sin((double)j * Pi / (2. * N + 1.));

  return 2. / (2. * N + 1.) * tmp * tmp;
}

double tc::padeSquareRoot::bj(int j, int N){
  double tmp = cos((double)j * Pi / (2. * N + 1.));

  return tmp * tmp;
}

complex<double> tc::padeSquareRoot::C0(int N, double theta){
  complex<double> sum = complex<double>(1, 0);
  complex<double> one = complex<double>(1, 0);
  complex<double> z   = complex<double>(cos(-theta) - 1,  sin(-theta));

  for(int j = 1; j <= N; j++)
    sum += (z * aj(j, N)) / (one + z * bj(j, N));

  z = complex<double>(cos(theta / 2.), sin(theta / 2.));

  return sum * z;
}

complex<double> tc::padeSquareRoot::R0(int N, double theta){
  complex<double> sum = C0(N, theta);

  for(int j = 1; j <= N; j++)
    sum += A(j, N, theta) / B(j, N, theta);

  return sum;
}

complex<double> tc::padeSquareRoot::A(int j, int N, double theta){
  complex<double> one = complex<double>(1, 0);
  complex<double> res;
  complex<double> z;

  z   = complex<double>(cos(-theta / 2.), sin(-theta / 2.));
  res = z * aj(j, N);

  z   = complex<double>(cos(-theta) - 1., sin(-theta));
  res = res / ((one + z * bj(j, N)) * (one + z * bj(j, N)));

  return res;
}

complex<double> tc::padeSquareRoot::B(int j, int N, double theta){
  complex<double> one = complex<double>(1, 0);
  complex<double> res;
  complex<double> z;

  z   = complex<double>(cos(-theta), sin(-theta));
  res = z * bj(j, N);

  z   = complex<double>(cos(-theta) - 1., sin(-theta));
  res = res / (one + z * bj(j, N));

  return res;
}
