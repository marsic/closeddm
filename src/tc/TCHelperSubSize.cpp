// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCHelperSubSize.h"

#include <numeric>
#include "gmsh.h"
#include "gmshfem/MathObject.h"
#include "Constant.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace gmshddm::domain;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace std;

void tc::subSize::build(NMap<SFunction> &ld,
                        const vector<vector<unsigned int> > &neighbour,
                        const Interface &sigma,
                        double L){
  // Namespace
  using gmshfem::function::x;
  using internal::populate;
  using internal::meanLij;

  // Alloc
  ld.resize(neighbour.size());

  // Populate
  //populate<SFunction>(ld, neighbour, sigma, L,
  //                    [](const Domain &sij){ return x<cmplx>(); });
  //populate<cmplx>(ld, neighbour, sigma, L,
  //                [](const Domain &sij){ return cmplx(0.165, 0); });
  populate<cmplx>(ld, neighbour, sigma, L,
                  [](const Domain &sij){ return meanLij(x<cmplx>(), sij); });

//  ld[0][neighbour[0][0]] = x<cmplx>() / nDom;
//  for(size_t i = 1; i < nDom-1; i++){
//    ld[i][neighbour[i][0]] = x<cmplx>() / nDom;
//    ld[i][neighbour[i][1]] = x<cmplx>() / nDom;
//  }
//  ld[nDom-1][neighbour[nDom-1][0]] = x<cmplx>() / nDom;
}

void tc::subSize::nAux(NMap<unsigned int> &nAux,
                       unsigned int n,
                       const vector<vector<unsigned int> > &neighbour){
  // Alloc
  size_t nDom = neighbour.size();
  nAux.resize(nDom);

  // Populate
  for(size_t i = 0; i < nDom; i++){
    for(unsigned int jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      nAux[i][j] = n;
    }
  }
}

void tc::subSize::nAux(NMap<unsigned int> &nAux,
                       double mult,
                       NMap<SFunction> &ld,
                       const vector<vector<unsigned int> > &neighbour,
                       const Interface &sigma,
                       double k){
  // Alloc
  size_t nDom = neighbour.size();
  nAux.resize(nDom);

  // Populate
  for(size_t i = 0; i < nDom; i++){
    for(unsigned int jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      double lij = internal::meanLij(ld[i][j], sigma(i,j)).real();
      nAux[i][j] = ceil((k*lij/Pi - 1) * mult);
    }
  }
}

cmplx tc::subSize::internal::meanLij(const SFunction &lij,
                                     const Domain &sigmaij){
  // Sanity
  if(sigmaij.isEmpty())
    throw Exception("Something went wrong");

  // Compute lij on each node contained in sigmaij and append them in 'l'
  list<cmplx> l;
  for(auto it = sigmaij.cbegin(); it != sigmaij.cend(); it++){
    vector<size_t> nodeTags;
    vector<double> nodeCoords, nodeParams;
    gmsh::model::mesh::getNodes(nodeTags, nodeCoords, nodeParams,
                                it->first, it->second);

    size_t nNode = nodeTags.size();
    if(nodeCoords.size() != 3*nNode)
      throw Exception("Something went wrong");

    for(size_t i = 0; i < nNode; i++){
      gmshfem::MathObject<cmplx, gmshfem::Degree::Degree0>::Object tmp;
      lij.evaluate(tmp, nodeCoords[3*i+0],
                        nodeCoords[3*i+1],
                        nodeCoords[3*i+2], *it);
      l.push_back(tmp);
    }
  }

  // Return value
  return accumulate(l.begin(), l.end(), cmplx(0)) / cmplx(l.size());
}

template<class T>
void tc::subSize::internal::populate(NMap<SFunction> &ld,
                                     const vector<vector<unsigned int> > &nb,
                                     const Interface &sigma,
                                     double L,
                                     T (*lc)(const Domain &sigmaij)){
  size_t nDom = nb.size();

  ld[0][nb[0][0]] = L - lc(sigma(0, nb[0][0]));
  for(size_t i = 1; i < nDom-1; i++){
    ld[i][nb[i][0]] =     lc(sigma(i, nb[i][0]));
    ld[i][nb[i][1]] = L - lc(sigma(i, nb[i][1]));
  }
  ld[nDom-1][nb[nDom-1][0]] = lc(sigma(nDom-1, nb[nDom-1][0]));
}

// Explicit instantiation //
////////////////////////////
namespace closeddm::tc::subSize::internal{
  template
  void populate<SFunction>(NMap<SFunction> &ld,
                           const std::vector<std::vector<unsigned int> > &nb,
                           const gmshddm::domain::Interface &sigma,
                           double L,
                           SFunction (*lc)(const Domain &sigmaij));
  template
  void populate<cmplx>(NMap<SFunction> &ld,
                       const std::vector<std::vector<unsigned int> > &nb,
                       const gmshddm::domain::Interface &sigma,
                       double L,
                       cmplx (*lc)(const Domain &sigmaij));
}
