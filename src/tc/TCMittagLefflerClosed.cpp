// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCMittagLefflerClosed.h"
#include "TCHelperAuxField.h"
#include "TCHelperSubSize.h"
#include "Constant.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::tc;
using namespace gmshddm::domain;
using namespace std;

#define SQU(a) (a*a)

TCMittagLefflerClosed::
TCMittagLefflerClosed(cmplx k, double gamma, double L,
                      int nAux, int order, string family,
                      const vector<vector<unsigned int> > &neighbour,
                      const Interface &sigma,
                      const Interface &dSigma){
  // Init //
  this->gamma   = gamma;
  this->S       = 1;
  this->name    = nameWithAux("ml", nAux);
  this->variant = "closed";

  // Create auxiliary fields //
  auxField::build("phi", phi, nAux, family, order, neighbour, sigma, dSigma);
  auxField::build("psi", psi, nAux, family, order, neighbour, sigma, dSigma);

  // Size of complement domains (ld) //
  NMap<SFunction> ld;
  subSize::build(ld, neighbour, sigma, L);

  // Init stuff (and set them to zero) //
  init(neighbour, nAux);

  // Terms of the Mittag-Leffler TC (cotangent function) //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      C[i][neighbour[i][j]] = 1./ld[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        A[i][neighbour[i][j]][n] = +2./ld[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Ad[i][neighbour[i][j]][n] = -2./(ld[i][neighbour[i][j]] * SQU(k));

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        B[i][neighbour[i][j]][n] = 1.-SQU(((n+1)*Pi)/
                                          (k*ld[i][neighbour[i][j]]));

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Bd[i][neighbour[i][j]][n] = -1./SQU(k);

  // Update terms //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      Uc[i][neighbour[i][j]] = C[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Uu[i][neighbour[i][j]][n] = A[i][neighbour[i][j]][n];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Up[i][neighbour[i][j]][n] = A[i][neighbour[i][j]][n] *
          SQU(((n+1)*Pi)/(k*ld[i][neighbour[i][j]]));

  // Regularization with constant damping //
  R = utils::damping(k, gamma);
}

TCMittagLefflerClosed::~TCMittagLefflerClosed(void){
  // See TCRational::~TCRational()
}
