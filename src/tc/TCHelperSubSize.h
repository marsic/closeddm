// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_HELPER_SUB_SIZE_H_
#define _CLOSEDDM_TC_HELPER_SUB_SIZE_H_

#include "CloseDDM.h"

namespace closeddm{
  namespace tc{
    /// Helpers functions determining the subsizes of a rectangular cavity
    namespace subSize{
      /// Populate `ld` with the subsizes of rectangular cavity of size `L`,
      /// interfaces `sigma` and a neighbourhood map `neighbour`.
      /// This results in `ld[i][j]` being the subsize associated with
      /// the interface between subdomains `i` and `j`, on the side of `i`.
      void build(NMap<SFunction> &ld,
                 const std::vector<std::vector<unsigned int> > &neighbour,
                 const gmshddm::domain::Interface &sigma,
                 double L);

      /// Populates `nAux` with the value `n` according to the
      /// neighbourhood map `neighbour`
      void nAux(NMap<unsigned int> &nAux,
                unsigned int n,
                const std::vector<std::vector<unsigned int> > &neighbour);

      /// TODO
      void nAux(NMap<unsigned int> &nAux,
                double mult,
                NMap<SFunction> &ld,
                const std::vector<std::vector<unsigned int> > &neighbour,
                const gmshddm::domain::Interface &sigma,
                double k);

      namespace internal{
        template<class T>
        void populate(NMap<SFunction> &ld,
                      const std::vector<std::vector<unsigned int> > &nb,
                      const gmshddm::domain::Interface &sigma,
                      double L,
                      T (*lc)(const gmshfem::domain::Domain &sigmaij));
        cmplx meanLij(const SFunction &lij,
                      const gmshfem::domain::Domain &sigmaij);
      }
    }
  }
}

#endif
