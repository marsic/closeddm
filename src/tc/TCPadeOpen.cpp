// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCPadeOpen.h"
#include "TCHelperAuxField.h"
#include "TCHelperPadeSquareRoot.h"
#include "Constant.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::tc;
using namespace gmshddm::domain;
using namespace std;

TCPadeOpen::TCPadeOpen(cmplx k, double gamma, double theta,
                       int nAux, int order, string family,
                       const vector<vector<unsigned int> > &neighbour,
                       const Interface &sigma,
                       const Interface &dSigma){
  // Init //
  this->gamma   = gamma;
  this->S       = 1;
  this->name    = nameWithAux("pade", nAux);
  this->variant = "open";

  // Create auxiliary fields //
  auxField::build("phi", phi, nAux, family, order, neighbour, sigma, dSigma);
  auxField::build("psi", psi,    0, family, order, neighbour, sigma, dSigma);

  // Complexified wavenumber //
  cmplx kc  = k + utils::damping(k, gamma);
  cmplx kc2 = kc*kc;

  // Init stuff (and set them to zero) //
  init(neighbour, nAux);

  // Terms of the Pade TC (square root function) //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      C[i][neighbour[i][j]] = -J*k*padeSquareRoot::C0(nAux, theta);

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Ad[i][neighbour[i][j]][n] = +J*k*padeSquareRoot::A(n+1,nAux,theta)/kc2;

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        B[i][neighbour[i][j]][n] = +1;

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Bd[i][neighbour[i][j]][n] = -padeSquareRoot::B(n+1,nAux,theta)/kc2;

  // Update terms //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      Uc[i][neighbour[i][j]] = C[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Uu[i][neighbour[i][j]][n] = -2.*J*k*padeSquareRoot::A(n+1,nAux,theta) /
                                            padeSquareRoot::B(n+1,nAux,theta);

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Up[i][neighbour[i][j]][n] = -Uu[i][neighbour[i][j]][n];

  // Regularization with constant damping //
  R = 0;
}

TCPadeOpen::~TCPadeOpen(void){
  // See TCRational::~TCRational()
}
