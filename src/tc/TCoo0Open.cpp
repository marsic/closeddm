// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCoo0Open.h"
#include "Constant.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::tc;
using namespace std;

TCoo0Open::TCoo0Open(cmplx k, double gamma,
                     const vector<vector<unsigned int> > &neighbour){
  // Init //
  this->gamma   = gamma;
  this->S       = 1;
  this->name    = "oo0";
  this->variant = "open";
  C0.resize(neighbour.size());

  // Complexified wavenumber //
  cmplx kc = k + utils::damping(k, gamma);

  // TC per say //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      this->C0[i][neighbour[i][j]] = -J * kc;
}

TCoo0Open::~TCoo0Open(void){
}
