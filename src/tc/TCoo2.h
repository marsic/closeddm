// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_OO2_H_
#define _CLOSEDDM_TC_OO2_H_

#include "TC.h"

namespace closeddm{
  namespace tc{
    /// Abstract class for second-order transmission conditions
    class TCoo2: public TC{
    protected:
      cmplx A;
      cmplx B;

    public:
      ~TCoo2(void) = 0;
      virtual void interface(gmshfem::problem::Formulation<cmplx> &f,
                             Field0 &ui,
                             const gmshfem::domain::Domain &sij,
                             const unsigned int i,
                             const unsigned int j,
                             const std::string &g) const override final;

      virtual void update(gmshfem::problem::Formulation<cmplx> &f,
                          Field0 &ui,
                          Field0 &gij,
                          const gmshfem::domain::Domain &sij,
                          const unsigned int i,
                          const unsigned int j,
                          const std::string &g) const override final;
    };
  }
}

#endif
