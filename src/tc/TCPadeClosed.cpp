// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCPadeClosed.h"
#include "TCHelperAuxField.h"
#include "TCHelperPadeTrig.h"
#include "TCHelperSubSize.h"
#include "Constant.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::tc;
using namespace gmshddm::domain;
using namespace std;

#define SQU(a) (a*a)

TCPadeClosed::TCPadeClosed(cmplx k, double gamma, double L,
                           int nAux, int order, string family,
                           const vector<vector<unsigned int> > &neighbour,
                           const Interface &sigma,
                           const Interface &dSigma){
  // Init //
  this->gamma   = gamma;
  this->S       = 1;
  this->name    = nameWithAux("pade", nAux);
  this->variant = "closed";

  // Create auxiliary fields //
  auxField::build("phi", phi, nAux, family, order, neighbour, sigma, dSigma);
  auxField::build("psi", psi, nAux, family, order, neighbour, sigma, dSigma);

  // Size of complement domains (ld) //
  NMap<SFunction> ld;
  subSize::build(ld, neighbour, sigma, L);
  //  NMap<cmplx> ld(nDom);
  //  double ldd0 = ol.getNumber("05Test/00Ld0");
  //  double ldd1 = ol.getNumber("05Test/01Ld1");
  //  ld[0][neighbour[0][0]] = ldd0;
  //  ld[1][neighbour[1][0]] = ldd1;

  // Init stuff (and set them to zero) //
  init(neighbour, nAux);

  // Pade terms of the cotangent function //
  double pC;
  vector<double> pA, pB;
  padeTrig::approx(nAux, pC, pA, pB);
  //  closeddm::msg::info << "C0: " << pC << closeddm::msg::endl;
  //  closeddm::msg::info << "Ai" << closeddm::msg::endl;
  //  for(auto &i: pA)
  //    closeddm::msg::info << "| " << i << closeddm::msg::endl;
  //  closeddm::msg::info << "Bi" << closeddm::msg::endl;
  //  for(auto &i: pB)
  //    closeddm::msg::info << "| " << i << closeddm::msg::endl;
  //  std::sort(pB.begin(), pB.end());
  //  closeddm::msg::info << "BPi" << closeddm::msg::endl;
  //  for(auto &i: B)
  //    closeddm::msg::info << "| " << sqrt(i)/(Pi) << closeddm::msg::endl;

  // Terms of the Pade TC (cotangent function) //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      C[i][neighbour[i][j]] = pC/ld[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        A[i][neighbour[i][j]][n] = pA[n]/ld[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        B[i][neighbour[i][j]][n] = SQU(k*ld[i][neighbour[i][j]]) - pB[n];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Bd[i][neighbour[i][j]][n] = -SQU(ld[i][neighbour[i][j]]);

  // Update terms //
  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      Uc[i][neighbour[i][j]] = C[i][neighbour[i][j]];

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t j = 0; j < neighbour[i].size(); j++)
      for(int    n = 0; n < nAux; n++)
        Up[i][neighbour[i][j]][n] = A[i][neighbour[i][j]][n];

  // Regularization with constant damping //
  R = utils::damping(k, gamma);
}

TCPadeClosed::~TCPadeClosed(void){
  // See TCRational::~TCRational()
}
