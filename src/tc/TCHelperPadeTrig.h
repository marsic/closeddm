// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_HELPER_PADE_TRIG_H_
#define _CLOSEDDM_TC_HELPER_PADE_TRIG_H_

#include <gmpxx.h>
#include <vector>
#include "CloseDDM.h"

namespace closeddm{
  namespace tc{
    /// Pade approximant of some trigonometric functions
    namespace padeTrig{
      /// Computes the \f$(N+1)\f$ terms \f$(N > 0)\f$ Pade approximation
      /// of \f$x\cot(x)\f$ as:
      /// \f[x\cot(x) \approx C_0 + \sum_{i=1}^N A_i / (x^2 - B_i).\f]
      /// The underlying Pade approximant of the \f$x\cot(x)\f$ function
      /// is of order \f$[2N, 2N]\f$.
      ///
      /// @param[in]  N Determines the total number of Pade terms, i.e \f$N+1\f$
      /// @param[out] C0 Populated with the \f$C_0\f$ coeff. of the Pade series
      /// @param[out] Ai Populated with the \f$A_i\f$s of the Pade series
      /// @param[out] Bi Populated with the \f$B_i\f$s of the Pade series
      /// @param[in]  prec Precision of floating-point operations in bits
      ///
      /// @see G. A. Baker and P. Graves-Morris, "Pade Approximants",
      ///   Cambridge University Press, 1996,
      ///   [DOI: 10.1017/cbo9780511530074](
      ///     https://doi.org/10.1017/cbo9780511530074)
      /// @see <https://functions.wolfram.com/ElementaryFunctions/Cot/10/>
      ///
      /// @warning The floating-point computations are carried out
      ///   in arbitrary precision as prescribed by `prec`
      /// @see [GMP](https://gmplib.org/manual/Initializing-Floats)
      int approx(int N,
                 double &C0, std::vector<double> &Ai, std::vector<double> &Bi,
                 size_t prec=1000);

      // 'Private' helper functions
      namespace internal{
        int pade_xcotx_generic(std::vector<mpq_class> &C, unsigned int m,
                               unsigned int(*order)(unsigned int),
                               const std::vector<int> &one,
                               const std::vector<int> &two);
        unsigned int pade_xcotx_num_order(unsigned int m);
        int          pade_xcotx_num(std::vector<mpq_class> &C, unsigned int m);
        unsigned int pade_xcotx_den_order(unsigned int m);
        int          pade_xcotx_den(std::vector<mpq_class> &C, unsigned int m);

        int   polydiv_eqO(const std::vector<mpq_class> &a,
                          const std::vector<mpq_class> &b,
                          std::vector<mpq_class> &q,
                          std::vector<mpq_class> &r);
        mpf_class polyval(const std::vector<mpq_class> &p,
                          const mpf_class &x, size_t prec);
        int       polyder(const std::vector<mpq_class> &p,
                          std::vector<mpq_class> &dp);

        int   roots(const std::vector<mpq_class> &p,
                    std::vector<mpf_class> &rootR, std::vector<mpf_class> &rootI);
        int residue(const std::vector<mpq_class> &N,
                    const std::vector<mpq_class> &D,
                    const std::vector<mpf_class> &pole,
                    std::vector<mpf_class> &res,
                    size_t prec);
      }
    }
  }
}

#endif
