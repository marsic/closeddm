// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_FACTORY_H_
#define _CLOSEDDM_TC_FACTORY_H_

#include "Parameter.h"
#include "TC.h"

namespace closeddm{
  namespace tc{
    /// Returns a list of TC, defined on the interfaces `sigma`
    /// with boundaries `dSigma` and neighbourhoud map `neighbour`,
    /// according to the given parameters.
    /// The relevant parameters are:
    ///   - "tcName", the name of the TC ("oo0", "oo2", "pade" or "ml")
    ///   - "tcVar", the variant of the TC ("open", "closed" or "mixed")
    ///   - "k", the real part of the wavenumber
    ///   - "kd", the imaginary part of the wavenumber
    ///   - "gamma", the complex shift expressed as a percentage of k
    ///   - "lc", the characteristic length of the mesh
    ///   - "ly", the 'transverse' size of the domain
    ///   - "lx", the characteristic size of the domain
    ///   - "order", the finite element order
    ///   - "theta", the branch cut angle of the (complex) square root
    ///   - "nAux", the number of auxiliary terms in a TC
    ///   - "mixAux", the number of aux. terms of the "open" TC in a mixed TC
    ///   - "mixPro", the proportion of the "open" TC in a mixed TC
    ///
    /// @note In practice, only a subset of these parameters are relevant
    ///   for a given transmission condition
    std::list<TC*> factory(const closeddm::input::Parameter &param,
                           const std::vector<
                                   std::vector<unsigned int> > &neighbour,
                           const gmshddm::domain::Interface &sigma,
                           const gmshddm::domain::Interface &dSigma);
  };
};

#endif
