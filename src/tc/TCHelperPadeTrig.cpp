// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "CloseDDMDefines.h"
#ifdef HAVE_WIN32
#define __WINDOWS // for mps/debug.h when using MinGW (included by mps/mps.h)
#endif

#include <math.h>
#include "mps/mps.h"
#include "TCHelperPadeTrig.h"

using namespace closeddm;
using namespace closeddm::tc::padeTrig::internal;
using namespace std;

int tc::padeTrig::approx(int N,
                         double &C0,
                         vector<double> &Ai, vector<double> &Bi,
                         size_t prec){
  // Sanity //
  if(N < 1)
    return -1;

  // Pade approximation (Am/Bm) of order [2*N,2*N] //
  vector<mpq_class> A;
  vector<mpq_class> B;
  pade_xcotx_num(A, 2*N);
  pade_xcotx_den(B, 2*N);

  // Polynomial long division of A by B (i.e. A/B = Q + R/B) //
  vector<mpq_class> Q;
  vector<mpq_class> R;
  polydiv_eqO(A, B, Q, R);

  // Roots of Bm (via MPSolve) //
  vector<mpf_class> poleRe;
  vector<mpf_class> poleIm;
  roots(B, poleRe, poleIm);

  // For debugging: check if the roots of Bm converge towards (i*Pi)^2 //
  // vector<mpf_class> BPi(poleRe.size());
  // for(size_t i = 0; i < BPi.size(); i++)
  //   BPi[i] = sqrt(poleRe[i])/(4*atan(1));
  // sort(BPi.begin(), BPi.end());
  // for(size_t i = 0; i < BPi.size(); i++)
  //   std::cout << i << ": " << BPi[i] << std::endl;
  // throw std::exception();

  // Residues of R/B (assuming /real/ and /simple/ poles) //
  vector<mpf_class> res;
  residue(R, B, poleRe, res, prec);

  // Converting to 'double' and populating C0, Ai and Bi //
  // C0
  C0 = Q[0].get_d();

  // Ai
  Ai.resize(N);
  for(int i = 0; i < N; i++)
    Ai[i] = res[i].get_d();

  // Bi
  Bi.resize(N);
  for(int i = 0; i < N; i++)
    Bi[i] = poleRe[i].get_d();

  // Done //
  return 0;
}

// Private helper functions //
//////////////////////////////
namespace closeddm::tc::padeTrig::internal{

/// Generic 3-terms recurrence formula for the Pade approximant of
/// the \f$x\cot(x)\f$ function, ie
///     \f$C_{m} = (2*m+1) C_{m-1} - x^2 C_{m-2}\f$.
///
/// @param C     vector of coefficients of
///              \f$C_{m}(x) = \sum_i\f$ C[i] \f$x^i\f$
/// @param m     order of the Pade approximant
/// @param order pointer to a function returning
///              the polynomial order of \f$C_{m}\f$
/// @param C1    polynomial \f$C_1\f$ for starting the 3-terms recurrence
/// @param C2    polynomial \f$C_2\f$ for starting the 3-terms recurrence
///
/// @see G. A. Baker and P. Graves-Morris, "Pade Approximants",
///   Cambridge University Press, 1996,
///   [DOI: 10.1017/cbo9780511530074](https://doi.org/10.1017/cbo9780511530074)
/// @see <https://functions.wolfram.com/ElementaryFunctions/Cot/10/>
int pade_xcotx_generic(vector<mpq_class> &C, unsigned int m,
                       unsigned int(*order)(unsigned int),
                       const vector<int> &C1,
                       const vector<int> &C2){
  // Sanity
  if(m < 0){
    return -1;
  }

  // Polynomial order
  unsigned int N = order(m);

  // Allocate temp arrays for the three-terms recurrence
  unsigned int o[3] = {0, 0, 0};
  vector<mpq_class> X[3];
  X[0].resize(N+1);
  X[1].resize(N+1);
  X[2].resize(N+1);

  // Initialization
  o[0] = C1.size() - 1;
  for(unsigned int i = 0; i <= o[0]; i++)
    X[0][i] = C1[i];

  o[1] = C2.size() - 1;
  for(unsigned int i = 0; i <= o[1]; i++)
    X[1][i] = C2[i];

  // Three-terms recurrence
  unsigned int it;
  if(m < 2){
    it = m;
  }
  else{
    for(unsigned int mm = 2; mm <= m; mm++){
      // X[m]  = X[m-1] * (2m+1)
      for(unsigned int i = 0; i <= o[(mm-1) % 3]; i++)
        X[mm % 3][i] = X[(mm-1) % 3][i] * (2*mm + 1);

      // X[m] += X[m-2] * (-x)
      for(unsigned int i = 0; i <= o[(mm-2) % 3]; i++)
        X[mm % 3][i+1] += X[(mm-2) % 3][i] * (-1);

      // Order of X[m] and current temp vector
      o[mm % 3] = order(mm);
      it = mm;
    }
  }

  // Coefficient vector
  C.resize(N+1);
  for(unsigned int i = 0; i < N+1; i++)
    C[i] = X[it % 3][i];

  // Done
  return 0;
}

/// Returns the polynomial order of the numerator
/// of the m-th order Pade approximant of \f$x\cot(x)\f$
unsigned int pade_xcotx_num_order(unsigned int m){
  return floor((m+1)/2);
}

/// Populates C with the numerator
/// of the m-th order Pade approximant of \f$x\cot(x)\f$
int pade_xcotx_num(vector<mpq_class> &C, unsigned int m){
  vector<int> one(1); one[0] = 1;
  vector<int> two(2); two[0] = 3; two[1] = -1;
  return pade_xcotx_generic(C, m, pade_xcotx_num_order, one, two);
}

/// Returns the polynomial order of the denominator
/// of the m-th order Pade approximant of \f$x\cot(x)\f$
unsigned int pade_xcotx_den_order(unsigned int m){
  return floor(m/2);
}

/// Populates C with the denominator
/// of the m-th order Pade approximant of \f$x\cot(x)\f$
int pade_xcotx_den(vector<mpq_class> &C, unsigned int m){
  vector<int> one(1); one[0] = 1;
  vector<int> two(1); two[0] = 3;
  return pade_xcotx_generic(C, m, pade_xcotx_den_order, one, two);
}

/// Polynomial long division of a(x)/b(x),
/// that is finding q(x) and r(x) such that a(x) = b(x)*q(x) + r(x)
///
/// @warning We assume here that deg(a) = deg(b)
int polydiv_eqO(const vector<mpq_class> &a,
                const vector<mpq_class> &b,
                vector<mpq_class> &q,
                vector<mpq_class> &r){
  // Polynomial degrees
  size_t degA = a.size() - 1;
  size_t degB = b.size() - 1;

  // Sanity
  if(degA != degB)
    return -1; // TBD

  // Allocate q [q = 0 and deg(q) = 0 (because of our assumptions)]
  q.resize(1);

  // Allocate r [r = a without the leading term (deg(r) = deg(a) - 1)]
  r.resize(degA);
  size_t degR = degA - 1;
  for(size_t i = 0; i <= degR; i++)
    r[i] = a[i];

  // Long division (in this special case, only one 'iteration' is needed)
  mpq_class t = a[degA] / b[degB];  // t = lead(a) / lead(b)
  q[0] += t;                        // q = q + t (here deg(q) = 0)
  for(size_t i = 0; i <= degR; i++) // r = r - t * b
    r[i] = r[i] - t * b[i];

  // Done
  return 0;
}

/// Returns the evaluation of polynomial p at x with precision prec (in bits)
/// @see <https://en.wikipedia.org/wiki/Horner%27s_method>
mpf_class polyval(const vector<mpq_class> &p,
                  const mpf_class &x, size_t prec){
  size_t  deg = p.size() - 1;
  mpf_class y(p[deg], prec);

  for(size_t i = 1; i <= deg; i++){
    y *= x;
    y += p[deg-i];
  }

  return y;
}

/// Computes dp, the derivative of polynomial p
int polyder(const vector<mpq_class> &p, vector<mpq_class> &dp){
  // Init
  unsigned int deg = p.size() - 1;
  dp.resize(deg);
  for(unsigned int i = 0; i < deg; i++)
    dp[i] = 0;

  // Derive
  for(unsigned int i = 1; i <= deg; i++)
    dp[i-1] = p[i] * i;

  // Done
  return 0;
}

/// Finds the roots of polynomial p and stores:
///   * the real part in rootR
///   * the imaginary part in rootI
///
/// @note Relies on [MPSolve](https://github.com/robol/MPSolve)
int roots(const vector<mpq_class> &p,
          vector<mpf_class> &rootR, vector<mpf_class> &rootI){
  // Polynomial degree and init
  size_t deg = p.size() - 1;

  // Const cast to use mpq_class::get_mpq_t... I won't change it, I promisse :o)
  vector<mpq_class> &pc = const_cast<vector<mpq_class>&>(p);

  // Zero mpq_t struct
  mpq_t zero;
  mpq_init(zero);

  // MPSolve context and polynomial
  mps_context      *ctx = mps_context_new();
  mps_monomial_poly *pp = mps_monomial_poly_new(ctx, deg);
  for(size_t i = 0; i <= deg; i++)
    mps_monomial_poly_set_coefficient_q(ctx, pp, i, pc[i].get_mpq_t(), zero);
  mps_context_set_input_poly(ctx, MPS_POLYNOMIAL(pp));

  // Solve
  mps_mpsolve(ctx);
  if(mps_context_has_errors(ctx)){
    //std::cout << string(mps_context_error_msg(ctx)) << std::endl;
    return -1;
  }

  // Roots
  mpc_t  *root   = NULL;
  rdpe_t *radius = NULL;
  mps_context_get_roots_m(ctx, &root, &radius);

  // Populate rootR/I
  rootR.resize(deg);
  for(size_t i = 0; i < deg; i++)
    rootR[i] = mpf_class(mpc_Re(root[i]));

  rootI.resize(deg);
  for(size_t i = 0; i < deg; i++)
    rootI[i] = mpf_class(mpc_Im(root[i]));

  // TODO: Should we return radius as well?

  // Clear
  mpq_clear(zero);
  mpc_vclear(root, deg);
  free(root);
  free(radius);
  mps_monomial_poly_free(ctx, (mps_polynomial*)pp);
  mps_context_free(ctx);

  // Done
  return 0;
}

/// Computes the residues of N(x)/D(x), where N and D are polynomials.
///
/// @param N    the polynomial N(x)
/// @param D    the polynomial D(x)
/// @param pole the zeros of polynomial D(x)
/// @param res  the vector where the residues will be stored
/// @param prec the required precision in bits
///
/// @warning We assume that:
///   * deg(N) < deg(D)
///   * the poles of D are simple
/// @see <https://en.wikipedia.org/wiki/Residue_(complex_analysis)#Simple_poles>
int residue(const vector<mpq_class> &N, const vector<mpq_class> &D,
            const vector<mpf_class> &pole,
            vector<mpf_class> &res, size_t prec){
  // Sanity //
  size_t degN  = N.size() - 1;
  size_t degD  = D.size() - 1;
  size_t nPole = pole.size();
  if(degN >= degD || degD != nPole)
    return -1;

  // Residues //
  // Derivative of D
  vector<mpq_class> dD; polyder(D, dD);

  // Init
  res.resize(nPole);

  // Loop over each *simple* pole
  for(size_t i = 0; i < nPole; i++)
    res[i] = polyval(N, pole[i], prec) / polyval(dD, pole[i], prec);

  // Done
  return 0;
}
}
