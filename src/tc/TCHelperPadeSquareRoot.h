// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_TC_HELPER_PADE_SQUARE_ROOT_H_
#define _CLOSEDDM_TC_HELPER_PADE_SQUARE_ROOT_H_

#include "gmshddm/Interface.h"
#include "gmshfem/Field.h"
#include "CloseDDM.h"

namespace closeddm{
  namespace tc{
    /// Pade approximant of the square root function

    /// @see [1] Milinazzo F. A. et al., "Rational square-root approximations
    ///      for parabolic equation algorithms", The Journal of the Acoustical
    ///      Society of America, vol. 101, no. 2, pp. 760-766, 1997,
    ///      [DOI:10.1121/1.418038](https://doi.org/10.1121/1.418038)
    /// @note In what follows, the parameter `theta` corresponds the parameter
    ///       \f$\alpha\f$ in [1] and will be designated as \f$\theta\f$
    namespace padeSquareRoot{
      /// Returns \f$C_0(N, \theta)\f$ as defined in [1, eq. 11]
      cmplx C0(int N, double theta);
      /// Returns \f$\sum_{j=1}^N A_j(N, \theta) / B_j(N, \theta)\f$ (see below)
      cmplx R0(int N, double theta);
      /// Returns \f$A_j(N, \theta)\f$ as defined in [1, eq. 13 where \f$j=n\f$]
      cmplx  A(int j, int N, double theta);
      /// Returns \f$B_j(N, \theta)\f$ as defined in [1, eq. 11 where \f$j=n\f$]
      cmplx  B(int j, int N, double theta);

      /// Returns \f$a_j(N)\f$ as defined in [1, eq. 6 where \f$j=n\f$]
      double aj(int j, int N);
      /// Returns \f$b_j(N)\f$ as defined in [1, eq. 6 where \f$j=n\f$]
      double bj(int j, int N);
    }
  }
}

#endif
