// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "TCoo2Open.h"
#include "Constant.h"

using namespace closeddm::constant;
using namespace closeddm::tc;

TCoo2Open::TCoo2Open(cmplx k, cmplx kMin, cmplx kMax, double delta){
  // Init //
  this->gamma   = 0;
  this->S       = 1;
  this->name    = "oo2";
  this->variant = "open";

  // OO2
  cmplx alpha = J*pow((k*k - kMin*kMin) * (k*k - (k-delta)*(k-delta)), 0.25);
  cmplx beta  =   pow((kMax*kMax - k*k) * ((k+delta)*(k+delta) - k*k), 0.25);

  this->A = -(alpha * beta - k*k) / (alpha + beta);
  this->B =                   -1. / (alpha + beta);
}

TCoo2Open::~TCoo2Open(void){
}
