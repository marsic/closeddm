// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ONELAB_H_
#define _CLOSEDDM_ONELAB_H_

#include <unordered_map>
#include "CloseDDMDefines.h"
#ifdef HAVE_WIN32
#define WIN32_LEAN_AND_MEAN
#endif

#include "gmsh/onelab.h"

namespace closeddm{
  namespace input{
    /// ONELAB interface

    /// @see
    /// - https://www.onelab.info
    /// - https://gitlab.onelab.info/gmsh/gmsh/-/blob/master/src/common/onelab.h
    class Onelab{
    private:
      onelab::remoteNetworkClient *olClient;
      std::string olName;

    public:
      /// Reads the given ONELAB database (from `filename` file)
      /// and returns the associated <onelab_name,onelab_value> map
      static std::unordered_map<std::string,
                                std::string> readDB(std::string filename);

    public:
      /// Instanciates a new ONELAB object with the given input strings
      /// @param[in] argv Pointer to C-strings
      /// @param[in] argc Number of strings in argv
      /// @note Looks for the input parameter "-onelab <name> <socket>"
      Onelab(int argc, char **argv);
      /// Destructor
      ~Onelab(void);

      /// Returns `true` if this Onelab instance is connected to a server
      bool                    isConnected(void) const;
      /// Returns the onelab::remoteNetworkClient of this Onelab instance
      onelab::remoteNetworkClient &client(void) const;
      /// Returns the name of the underlying Onelab model
      std::string                   model(void) const;
      /// Returns the action performed by the Onelab server
      std::string                  action(void) const;

      /// Fetches the Onelab number `name` from the server
      double      getNumber(std::string name) const;
      /// Fetches the Onelab string `name` from the server
      std::string getString(std::string name) const;

      /// Set the Onelab number `name` to the given value `value`
      void setNumber(std::string name, double value) const;

      /// Merges the file `filename` to the Onelab server
      void merge(std::string filename) const;
    };
  }
}

#endif
