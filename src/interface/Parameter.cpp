// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/Exception.h"
#include "Constant.h"
#include "Parameter.h"

using namespace gmshddm::common;
using namespace gmshfem::common;
using namespace closeddm::constant;
using namespace closeddm::input;

void Parameter::defParam(void){
  // Namespace
  using std::make_pair;

  // Parameters, their default values and their ONELAB name
  string["msh"] = make_pair("", "");
  string["db"]  = make_pair("", "");

  flag["eigenmode"] = make_pair(0, "06Solver/01Eigenmode?");

  number["lx"] = make_pair(0.50, "00Geometry/00X dimension");
  number["ly"] = make_pair(0.25, "00Geometry/01Y dimension");
  number["lz"] = make_pair(0.25, "00Geometry/02Z dimension");

  number["k"]      = make_pair(314.17, "01Problem/00Wavenumber");
  number["kd"]     = make_pair(0,      "01Problem/01Damping");
  number["ny"]     = make_pair(1,      "01Problem/03Mode(s) along Y");
  number["nz"]     = make_pair(1,      "01Problem/04Mode(s) along Z");
  flag["allmodes"] = make_pair(0,      "01Problem/05All modes?");
  flag["cavity"]   = make_pair(0,      "01Problem/06Cavity?");

  string["source"] = make_pair("rectangle", "01Problem/07Source/00Kind");
  number["lsy"]    = make_pair(0.25,        "01Problem/07Source/01Y dimension");
  number["lsz"]    = make_pair(0.25,        "01Problem/07Source/02Z dimension");

  number["lc"] = make_pair(0.00249991, "02Mesh/03Mesh density");

  number["order"]  = make_pair(4,              "03FEM/00Order");
  number["gauss"]  = make_pair(10,             "03FEM/01Gauss");
  string["family"] = make_pair("hierarchical", "03FEM/02Family");

  string["tcName"] = make_pair("oo0",  "04DDM/00Transmission condition");
  string["tcVar"]  = make_pair("open", "04DDM/01Variant");
  number["nDom"]   = make_pair(2,      "04DDM/02Number of subdomains");
  number["maxIt"]  = make_pair(1000,   "04DDM/03Max iterations");
  number["tolE"]   = make_pair(-6,     "04DDM/04Tolerance");
  flag["spectrum"] = make_pair(0,      "04DDM/05Spectrum?");
  number["gamma"]  = make_pair(0,      "04DDM/06Complex shift");
  number["nAux"]   = make_pair(4,      "04DDM/07Number of auxiliary fields");
  number["theta"]  = make_pair(Pi/4.,  "04DDM/08Branch cut angle");
  number["mixPro"] = make_pair(0.9,    "04DDM/09Mixed/00Closed proportion");
  string["mixAux"] = make_pair("4",    "04DDM/09Mixed/01Open aux fields");

  flag["save"]    = make_pair(1,      "05Post/00Save views?");
  flag["merge"]   = make_pair(1,      "05Post/01Merge views?");
  string["error"] = make_pair("none", "05Post/02Relative error");
  flag["dump"]    = make_pair(1,      "05Post/03Dump parameters?");

  number["eig_maxIt"] = make_pair(10,   "07Eigenmode/00Max iterations");
  number["eig_tol"]   = make_pair(1e-4, "07Eigenmode/01Tolerance");
  string["cim_path"]  = make_pair("circle(16, 0, 5, 88)",
                                        "07Eigenmode/02CIM/00Path");
  number["cim_start"] = make_pair(3,    "07Eigenmode/02CIM/01Start snapshots");
  number["cim_step"]  = make_pair(3,    "07Eigenmode/02CIM/02Step snapshots");
  number["cim_thr"]   = make_pair(1e-3, "07Eigenmode/02CIM/03Rank threshold");
  flag["cim_jac"]     = make_pair(1,    "07Eigenmode/02CIM/04Use Jacobian?");
  string["cim_vhat"]  = make_pair("alt","07Eigenmode/02CIM/05Kind of vHat");
  flag["cim_gs"]      = make_pair(1,    "07Eigenmode/02CIM/06Gram-Schmidt?");
}

void Parameter::dbsParam(const std::string &db){
  // Was a database provided?
  if(db.empty())
    return;

  // Read database
  std::unordered_map<std::string, std::string> map = Onelab::readDB(db);

  // Update flags
  for(auto &it: flag){
    auto found = map.find(it.second.second);
    if(found != map.end())
      it.second.first = std::stoi(found->second);
  }

  // Update number
  for(auto &it: number){
    auto found = map.find(it.second.second);
    if(found != map.end())
      it.second.first = std::stod(found->second);
  }

  // Update string
  for(auto &it: string){
    auto found = map.find(it.second.second);
    if(found != map.end())
      it.second.first = found->second;
  }
}

void Parameter::argParam(const std::unordered_map<
                           std::string, std::list<std::string> > &arg){
  auto fetch = [&arg](std::string entry)->std::string{
    entry.insert(0, 1, '-');
    if(arg.count(entry) != 0 && arg.at(entry).size() > 0)
      return arg.at(entry).back();
    else
      throw -1;
  };

  for(auto &it: flag)
    try{ it.second.first = std::stoi(fetch(it.first)); } catch(...){ }
  for(auto &it: number)
    try{ it.second.first = std::stod(fetch(it.first)); } catch(...){ }
  for(auto &it: string)
    try{ it.second.first =           fetch(it.first);  } catch(...){ }
}

void Parameter::oneParam(const Onelab &ol){
  if(!ol.isConnected())
    return;

  for(auto &it: flag)
    try{ it.second.first = ol.getNumber(it.second.second); } catch(...){ }
  for(auto &it: number)
    try{ it.second.first = ol.getNumber(it.second.second); } catch(...){ }
  for(auto &it: string)
    try{ it.second.first = ol.getString(it.second.second); } catch(...){ }
}

Parameter::Parameter(const std::string &db,
                     const std::unordered_map<std::string,
                                              std::list<std::string> > &arg,
                     const Onelab &ol){
  defParam();    // Populate maps with default parameters
  dbsParam(db);  // Populate maps with database parameters
  argParam(arg); // Populate maps with runtime parameters
  oneParam(ol);  // Populate maps with ONELAB parameters, if available
}

Parameter::~Parameter(void){
  flag.clear();
  number.clear();
  string.clear();
}

bool Parameter::getFlag(std::string name) const{
  auto found = flag.find(name);
  if(found == flag.end())
    throw Exception("Unknown flag parameter " + name);

  return (found->second.first == 0)? false : true;
}

double Parameter::getNumber(std::string name) const{
  auto found = number.find(name);
  if(found == number.end())
    throw Exception("Unknown number parameter " + name);

  return found->second.first;
}

std::string Parameter::getString(std::string name) const{
  auto found = string.find(name);
  if(found == string.end())
    throw Exception("Unknown string parameter " + name);

  return found->second.first;
}

void Parameter::setFlag(std::string name, bool value){
  flag[name].first = value;
}

void Parameter::setNumber(std::string name, double value){
  number[name].first = value;
}

void Parameter::setString(std::string name, std::string value){
  string[name].first = value;
}

std::string Parameter::dump(void) const{
  std::stringstream str;

  for(auto &it: flag)
    str << it.first         << ","
        << it.second.first  << ","
        << it.second.second << std::endl;

  for(auto &it: number)
    str << it.first         << ","
        << it.second.first  << ","
        << it.second.second << std::endl;

  for(auto &it: string)
    str << it.first         << ","
        << it.second.first  << ","
        << it.second.second << std::endl;

  return str.str();
}
