// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include <cerrno>
#include "Onelab.h"
#include "gmshfem/Exception.h"

using namespace gmshfem::common;
using namespace closeddm::input;

std::unordered_map<std::string,
                   std::string> Onelab::readDB(std::string filename){
  // Open file
  std::FILE *fp = std::fopen(filename.c_str(), "rb");
  if(!fp)
    throw Exception("Onelab::readDB(): cannot open file "+filename+
                    " ("+std::string(std::strerror(errno))+")");

  // Read and close
  std::vector<std::string> msg;
  onelab::parameter::fromFile(msg, fp);
  std::fclose(fp);

  // Loop through msg and populate map
  std::unordered_map<std::string, std::string> map;
  for(size_t i = 0; i < msg.size(); i++){
    std::string version, type, name;
    onelab::parameter::getInfoFromChar(msg[i], version, type, name);

    if(type == "number"){
      onelab::number v(name);
      v.fromChar(msg[i]);
      map[name] = v.getValueAsString();
    }
    else if(type == "string"){
      onelab::string v(name);
      v.fromChar(msg[i]);
      map[name] = v.getValue();
    }
    else{
      throw Exception("Onelab::readDB(): unknown parameter type " + type);
    }
  }

  // Done
  return map;
}

Onelab::Onelab(int argc, char **argv){
  // Look for -onelab
  std::string olAddress;
  for(int i = 0; i < argc-2; i++){
    std::string arg(argv[i]);
    if(arg.compare("-onelab") == 0){
      std::string    name(argv[i+1]);
      std::string address(argv[i+2]);

      if(name[0] != '-' && address[0] != '-'){
        olName    = name;
        olAddress = address;
      }
    }
  }

  // Connect to server
  if(!olName.empty() && !olAddress.empty())
    olClient = new onelab::remoteNetworkClient(olName, olAddress);
  else
    olClient = NULL;
}

Onelab::~Onelab(void){
  if(olClient)
    delete olClient;
}

bool Onelab::isConnected(void) const{
  return olClient && olClient->getGmshClient();
}

onelab::remoteNetworkClient& Onelab::client(void) const{
  if(!isConnected())
    throw Exception("Onelab::client(): no onelab server");
  return *olClient;
}

std::string Onelab::model(void) const{
  if(!isConnected())
    return "unknown";

  std::string full;
  try{
    full = getString(olName+"/Model name");
  }
  catch(...){
    full = getString("Gmsh/Model name");
  }

  return full.substr(0, full.find_last_of("."));
}

std::string Onelab::action(void) const{
  return (!isConnected())? "unknown" : getString(olName+"/Action");
}

double Onelab::getNumber(std::string name) const{
  std::vector<onelab::number> n;
  client().get(n, name);
  if(n.empty())
      throw Exception("Onelab::getNumber(): entry "+name+" not found");

  return n[0].getValue();
}

std::string Onelab::getString(std::string name) const{
  std::vector<onelab::string> n;
  client().get(n, name);
  if(n.empty())
    throw Exception("Onelab::getString(): entry "+name+" not found");

  return n[0].getValue();
}

void Onelab::setNumber(std::string name, double value) const{
  if(isConnected()){
    onelab::number n(name, value);
    client().set(n);
  }
}

void Onelab::merge(std::string filename) const{
  if(isConnected())
    client().sendMergeFileRequest(filename);
}
