// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PARAMETER_H_
#define _CLOSEDDM_PARAMETER_H_

#include <list>
#include <unordered_map>

#include "gmshfem/GmshFem.h"
#include "gmshddm/GmshDdm.h"
#include "Onelab.h"

namespace closeddm{
  /// Interface with the outside world
  namespace input{
    /// Database with input parameters (see source for available parameters)

    /// @see https://git.rwth-aachen.de/marsic/closeddm/-/blob/master/src/interface/Parameter.cpp
    class Parameter{
    private:
      std::unordered_map<std::string, std::pair<int,
                                                std::string> > flag;
      std::unordered_map<std::string, std::pair<double,
                                                std::string> > number;
      std::unordered_map<std::string, std::pair<std::string,
                                                std::string> > string;

    private:
      void defParam(void);
      void dbsParam(const std::string &db);
      void argParam(const std::unordered_map<std::string,
                                             std::list<std::string> > &arg);
      void oneParam(const Onelab &ol);

    public:
      /// Instanciates a new Parameter database from the given
      /// databse file `db`, argument map `arg` and ONELAB server `ol`.
      /// In case of conflicts, the ONELAB server has the highest priority and
      /// the database file has the lowest one.
      /// When multiple values are present for a given option in `arg`,
      /// only the last one is considered.
      Parameter(const std::string &db,
                const std::unordered_map<std::string,
                                         std::list<std::string> > &arg,
                const Onelab &ol);
      /// Destructor
      ~Parameter(void);

      /// Fetches the flag `name` from this Parameter database
      bool          getFlag(std::string name) const;
      /// Fetches the number `name` from this Parameter database
      double      getNumber(std::string name) const;
      /// Fetches the string `name` from this Parameter database
      std::string getString(std::string name) const;

      /// Sets the flag `name` to the value `value` *without* updating ONELAB
      void   setFlag(std::string name, bool value);
      /// Sets the number `name` to the value `value` *without* updating ONELAB
      void setNumber(std::string name, double value);
      /// Sets the string `name` to the value `value` *without* updating ONELAB
      void setString(std::string name, std::string value);

      /// Returns a string describing this Parameter database
      std::string dump(void) const;
    };
  }
}

#endif
