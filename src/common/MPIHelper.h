// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_MPI_HELPER_H_
#define _CLOSEDDM_MPI_HELPER_H_

#include "CloseDDMDefines.h"

namespace closeddm{
  /// Helper functions for the Message Passing Interface (MPI)

  /// @see https://www.open-mpi.org/doc/ for instance
  namespace mpi{
    /// Wrapper for `MPI_Init(&argc, &argv)`
    int  init(int argc, char **argv);
    /// Wrapper for `MPI_Finalize()`
    int  finalize(void);
    /// Returns `true` if this process is the master one
    bool master(void);

    /// Returns the result of a `MPI_Allreduce` call
    /// applied to `send` with the operation `op
    ///
    /// @note Possible values for `op` are:
    ///  - '+', the reduction operation is a sum
    template<class T> T allreduce(T send, char op);
  }
}

#ifdef HAVE_MPI
#include <mpi.h>

namespace closeddm{
  namespace mpi{
    /// Returns the `rank` according to `MPI_Comm_rank(com, &rank)`
    int rank(MPI_Comm com);

    /// Trait for `MPI_Datatype`
    template<class T> class trait{ public: static MPI_Datatype type; };
    /// Returns the `MPI_Op` associated with the given char `op`
    ///
    /// @note Possible values for `op` are:
    ///  - '+', associated with `MPI_SUM`
    MPI_Op operation(char op);
  }
}
#endif

#endif
