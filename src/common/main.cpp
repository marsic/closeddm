// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/CSVio.h"

#include "CIM.h"
#include "CloseDDM.h"
#include "Message.h"
#include "ProblemFullHelmholtz.h"
#include "ProblemOSMHelmholtz.h"
#include "ProblemHelper.h"
#include "Utils.h"


using namespace closeddm;
using namespace closeddm::input;
using namespace closeddm::utils;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshfem::common;
using namespace gmshfem::domain;


void options(Parameter &param){
  if(param.getFlag("eigenmode")){
    // Eigenvalue problems: reduce verbosity and force RCM DoF reordering,
    // as the Hilbert one does not seem stable when assembling many systems
    // (i.e. the same DoF might end up with a different number)
    gmshfem::common::Options::instance()->verbose = 1;
    gmshfem::common::Options::instance()->dofsSortAlgorithm =
      gmshfem::problem::DofsSort::Algorithm::RCM;
  }
}


void ddm_source(const Onelab &ol, Parameter &param){
  // Options
  options(param);

  // Mesh, subdomains, interfaces and neighbourhood
  SMap<Subdomain> domain;
  SMap<Interface> interface;
  std::vector<std::vector<unsigned int> > neighbour;
  problem::helper::mesh(ol, param);
  problem::helper::domain(domain, interface, neighbour, param);

  // Solve Helmholtz problem
  problem::ProblemOSMHelmholtz prob(param, domain, interface, neighbour);
  msg::info << prob.info("solver") << msg::endl;
  msg::info << prob.info("domain") << msg::endl;
  msg::info << prob.info("source") << msg::endl;
  for(int i = 0; i < prob.nInfo("tc"); i++)
    msg::info << prob.info("tc", i) << msg::endl;

  prob.pre();
  if(param.getFlag("spectrum")){
    gmshfem::algebra::MatrixCRS<cmplx> D = prob.matrix();
    D.save("mat");

    gmshfem::algebra::Vector<cmplx> lambda;
    std::vector<gmshfem::algebra::Vector<cmplx> > v;
    gmshfem::algebra::eigenvalues<cmplx>(lambda, v, D, false, D.size(0));

    CSVio csv("eig");
    for(size_t i = 0; i < lambda.size(); i++)
      csv << gmshfem::csv::scientific << lambda[i] << gmshfem::csv::endl;

    for(size_t i = 0; param.getFlag("save") && i < v.size(); i++){
      prob.osFormulation().setSolutionIntoInterfaceFields(v[i].getStdVector());
      for(size_t j = 0; j < neighbour.size(); j++){
        prob.formulation(j).pre();
        prob.formulation(j).assemble();
        prob.formulation(j).solve();
      }

      utils::save(utils::subfields(prob, domain.at("omega"), "u"),
                  domain.at("omega"), "u_eig"+std::to_string(i), param, ol);
    }
  }
  else{
    int maxIt = param.getNumber("maxIt");
    int tolE  = param.getNumber("tolE");
    prob.solve("gmres+mgs", std::pow(10, tolE), maxIt); // tfqmr
    utils::save(utils::subfields(prob, domain.at("omega"), "u"),
                domain.at("omega"), "u", param, ol);
  }

  // Error w.r.t. closed form solution
  utils::error(utils::subfields(prob, domain.at("omega"), "u"),
               domain.at("omega"), "uAna", param, ol);

  // Done
  utils::dump(param);
}

void ddm_eigenmode(const Onelab &ol, Parameter &param){
  // Options
  options(param);

  // Mesh, subdomains, interfaces and neighbourhood
  SMap<Subdomain> domain;
  SMap<Interface> interface;
  std::vector<std::vector<unsigned int> > neighbour;
  problem::helper::mesh(ol, param);
  problem::helper::domain(domain, interface, neighbour, param);

  // Solve Helmholtz eigenproblem
  problem::ProblemOSMHelmholtz prob(param, domain, interface, neighbour);
  std::cout << prob.info("solver") << std::endl;
  std::cout << prob.info("domain") << std::endl;
  std::cout << prob.info("source") << std::endl;
  for(int i = 0; i < prob.nInfo("tc"); i++)
    std::cout << prob.info("tc", i)   << std::endl;

  std::vector<cmplx> lambda;
  std::vector<Field0> v;
  std::vector<double> residual;
  std::vector<std::list<std::string> > status;
  eigensolver::cim(param, prob, "u", "us",
                   utils::path::parse(param.getString("cim_path")),
                   lambda, v, residual, status);

  // Save
  CSVio csv("eig");
  for(size_t i = 0; i < lambda.size(); i++)
    csv << gmshfem::csv::scientific
        << lambda[i].real() << lambda[i].imag() << residual[i]
        << gmshfem::csv::endl;
  for(size_t i = 0; i < v.size(); i++)
    utils::save(v[i], domain.at("omega"), "u_"+std::to_string(i), param, ol);

  // Done
  utils::dump(param);
}

void ddm(int argc, char **argv, Parameter &param, const Onelab &ol){
  // Init GmshDDM
  GmshDdm gmshDdm(argc, argv);

  if(param.getFlag("eigenmode"))
    ddm_eigenmode(ol, param);
  else
    ddm_source(ol, param);
}


void full_source(const Onelab &ol, Parameter &param){
  // Options
  options(param);

  // Mesh and domains
  SMap<Domain> domain;
  problem::helper::mesh(ol, param);
  problem::helper::domain(domain, param);

  // Solve Helmholtz problem
  problem::ProblemFullHelmholtz prob(param, domain);
  msg::info << prob.info("solver") << msg::endl;
  msg::info << prob.info("domain") << msg::endl;
  msg::info << prob.info("source") << msg::endl;

  prob.pre();
  prob.assemble();
  prob.solve();
  utils::save(utils::subfields(prob, domain.at("omega"), "u"),
              domain.at("omega"), "u", param, ol);

  // Error w.r.t. closed form solution
  utils::error(utils::subfields(prob, domain.at("omega"), "u"),
               domain.at("omega"), "uAna", param, ol);

  // Done
  utils::dump(param);
}

void full_eigenmode(const Onelab &ol, Parameter &param){
  // Options
  options(param);

  // Mesh and domains
  SMap<Domain> domain;
  problem::helper::mesh(ol, param);
  problem::helper::domain(domain, param);

  // Solver Helmholtz eigenproblem
  problem::ProblemFullHelmholtz prob(param, domain);
  msg::info << prob.info("solver") << msg::endl;
  msg::info << prob.info("domain") << msg::endl;
  msg::info << prob.info("source") << msg::endl;

  std::vector<cmplx> lambda;
  std::vector<Field0> v;
  std::vector<double> residual;
  std::vector<std::list<std::string> > status;
  eigensolver::cim(param, prob, "u", "us",
                   utils::path::parse(param.getString("cim_path")),
                   lambda, v, residual, status);

  // Save
  CSVio csv("eig");
  for(size_t i = 0; i < lambda.size(); i++)
    csv << gmshfem::csv::scientific
        << lambda[i].real() << lambda[i].imag() << residual[i]
        << gmshfem::csv::endl;
  for(size_t i = 0; i < v.size(); i++)
    utils::save(v[i], domain.at("omega"), "u_"+std::to_string(i), param, ol);

  // Done
  utils::dump(param);
}

void full(int argc, char **argv, Parameter &param, const Onelab &ol){
  // Init GmshFEM
  GmshFem gmshFem(argc, argv);

  // Source or eigenvalue?
  if(param.getFlag("eigenmode"))
    full_eigenmode(ol, param);
  else
    full_source(ol, param);
}


int main(int argc, char **argv){
  // MPI & return status
  closeddm::mpi::init(argc, argv);
  int status = 0;

  try{
    // Runtime arguments
    std::unordered_map<std::string, std::list<std::string> > arg =
      utils::args(argc, argv);

    // Database, if any
    std::string db;
    if(arg.count("-db") != 0 && arg.at("-db").size() > 0)
      db = arg.at("-db").back();

    // ONELAB, if any
    Onelab ol(argc, argv);
    if(ol.isConnected() && ol.action() != "compute")
      throw 0;

    // Warning if no ONELAB server and no database
    if(!ol.isConnected() && db.empty())
      msg::warning << "No ONELAB server found and no database file provided"
                   << msg::endl;

    // First argument?
    std::string arg1 = (argc > 1)? argv[1] : "";

    // Parameters
    Parameter param(db, arg, ol);

    // Which solver to use?
    std::string solver;
    if(ol.isConnected())    solver = ol.getString("06Solver/00Type");
    else if(arg1 == "full") solver = "full";
    else                    solver = "ddm";

    // Run
    if     (solver == "ddm")   ddm(argc, argv, param, ol);
    else if(solver == "full") full(argc, argv, param, ol);
    else                throw Exception("Unknown solver: "+solver);
  }
  catch(int e){
  }
  catch(std::exception& e){
    // Errors
    status = -1;
    msg::error << e.what() << msg::endl;
  }

  // Done
  closeddm::mpi::finalize();
  return status;
}
