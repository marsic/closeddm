// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include <iomanip>
#include <numeric>

#include "gmsh.h"
#include "gmshddm/MPIInterface.h"
#include "gmshfem/Post.h"

#include "AnalyticFactory.h"
#include "Constant.h"
#include "Message.h"
#include "MPIHelper.h"
#include "Utils.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace closeddm::input;
using namespace closeddm::problem;
using namespace gmshddm::domain;
using namespace gmshddm::mpi;
using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace gmshfem::post;

template<class T>
T closeddm::utils::calc(const T& val, const std::string &op){
  // Function name
  std::string name = "closeddm::utils::calc(): ";

  // Empty case
  if(op.empty())
    throw Exception(name+"operation is an empty string");

  // Number case
  try{ return std::stod(op); }
  catch(...){ }

  // Equality case
  if(op[0] == '=')
    return val;

  // Arithmetic case
  if(op[0] != '/' && op[0] != '*')
    throw Exception(name+"unknown operation: "+op.substr(0,1));

  double arg = 0;
  try       { arg = std::stod(op.substr(1)); }
  catch(...){ throw Exception(name+"cannot parse: "+op.substr(1)); }

  if     (op[0] == '/') return val/arg;
  else if(op[0] == '*') return val*arg;

  // This should never happen...
  throw Exception(name+"this should never happen :(...");
  return 0;
}

template<>
std::string closeddm::utils::to_string(double z, unsigned int precision){
  std::stringstream str;
  str << std::scientific << std::setprecision(precision) << z;
  return str.str();
}

template<>
std::string closeddm::utils::to_string(cmplx z, unsigned int precision){
  double r = std::real(z);
  double i = std::imag(z);

  std::stringstream str;
  if(r < 0)
    str << "-";
  str << utils::to_string(std::abs(r), precision);

  if(i < 0)
    str << " - ";
  else
    str << " + ";
  str << utils::to_string(std::abs(i), precision) << imaginarySymbol;

  return str.str();
}

cmplx closeddm::utils::complexify(double a, double b){
  return a + J*b;
}

cmplx closeddm::utils::damping(cmplx k, double gamma){
  return +J * std::real(k)*gamma/100.;
}

void closeddm::utils::dump(const Parameter &param){
  if(param.getFlag("dump")){
    msg::info  << "Parameter used:" << msg::endl
               << "++++++++++++++ " << msg::endl;
    msg::print << param.dump()      << msg::endl;
  }
}

void closeddm::utils::table(std::ostream &os,
                            const std::vector<std::string> &txt,
                            size_t width, std::string sep){
  std::vector<size_t> w(txt.size(), width);
  table(os, txt, w, sep);
}

void closeddm::utils::table(std::ostream &os,
                            const std::vector<std::string> &txt,
                            const std::vector<size_t> &width, std::string sep){
  // Sanity
  if(txt.size() != width.size())
    throw Exception("closeddm::utils::table(): "
                    "txt and width must have the same size");

  // Store original status
  const auto oldFlags = os.setf(std::ios::left, std::ios::adjustfield);
  const auto oldWidth = os.width();

  // Loop and print
  for(size_t i = 0; i < txt.size(); i++){
    os.width(width[i]);
    os << sep + " " + txt[i];
  }
  os << sep << std::endl;

  // Restore
  os.setf(oldFlags);
  os.width(oldWidth);
}

std::string closeddm::utils::cat(const std::list<std::string> &lst,
                                 std::string sym){
  return std::accumulate(std::next(lst.begin()), lst.end(), *lst.begin(),
                         [&sym](const std::string &a, const std::string &b){
                           return b + sym + a;
                         });
}

std::unordered_map<std::string, std::list<std::string> >
closeddm::utils::args(int argc, char** argv){
  // Init map //
  std::unordered_map<std::string, std::list<std::string> > map;

  // Loop through argv //
  std::string myopt;
  for(int i = 0; i < argc; i++){
    std::string tmp(argv[i]);      // Convert argv[i] to std::string
    bool isOpt = !tmp.empty()      // Is tmp an option?
                 && tmp[0] == '-'; //
    if(isOpt){                     // If tmp is an option
      myopt = tmp;                 // --> it becomes my current option (CO)
      if(map.count(myopt) == 0)    // --> init if myopt is not already in map
        map[myopt] = {};           // --> ...
    }                              //
    if(!isOpt && !myopt.empty()){  // If tmp is not an option and if I have a CO
      map[myopt].push_back(tmp);   // --> add tmp to myopt in the map
    }
  }

  // Return map //
  return map;
}

template<class F, class D>
void closeddm::utils::error(const F &u, const D &omega, const std::string &name,
                            const Parameter &param, const Onelab &ol){
  if(param.getString("error") == "none")
    return;

  SFunction uAna = analytic::factory(param);
  double E = error(uAna, u, omega, 2 * param.getNumber("gauss"));

  msg::info << "Relative error (w.r.t. closed form solution): "
            << msg::scientific << msg::precision(16) << E << msg::endl;

  ol.setNumber("05Post/03Relative L2 error", E);
  save(uAna, omega, name, param, ol);
}

double closeddm::utils::error(const SEvObject &uAna,
                              const SEvObject &uFem,
                              const Domain &omega, int gauss){
  // Numerator and denominator
  double N, D;
  internal::error(uAna, uFem, omega, gauss, N, D);

  // Done
  return sqrt(N / D);
}

double closeddm::utils::error(const SEvObject &uAna,
                              const std::vector<const SEvObject*> &uFem,
                              const Subdomain &omega, int gauss){
  // Numerator and denominator of error for each subdomain
  std::list<double> Nl, Dl;
  unsigned int nDom = omega.numberOfSubdomains();
  for(unsigned int i = 0; i < nDom; i++){
    if(isItMySubdomain(i)){
      double Ni, Di;
      internal::error(uAna, *uFem[i], omega(i), gauss, Ni, Di);

      Nl.push_back(Ni);
      Dl.push_back(Di);
    }
  }

  // Sum my numerators and denominators
  double myN = std::accumulate(Nl.begin(), Nl.end(), 0.0);
  double myD = std::accumulate(Dl.begin(), Dl.end(), 0.0);

  // Gather all of them and add them together
  double N = closeddm::mpi::allreduce(myN, '+');
  double D = closeddm::mpi::allreduce(myD, '+');

  // Done
  return sqrt(N / D);
}

double closeddm::utils::error(const SEvObject &uAna,
                              const std::vector<const SEvObject*> &uFem,
                              const Domain &omega, int gauss){
  // Sanity
  if(uFem.size() != 1)
    throw Exception("closeddm::utils::error(): uFem.size() must be 1, "
                    "when used with a single domain");

  // Done
  return error(uAna, *uFem[0], omega, gauss);
}

void closeddm::utils::internal::error(const SEvObject &uAna,
                                      const SEvObject &uFem,
                                      const Domain &omega, int gauss,
                                      double &N, double &D){
  // Integration of numerator and denominator of L2-error over omega
  // NB: by contruction, the integrals are real with a 0 imaginar part
  std::string G = "Gauss" + std::to_string(gauss);

  N = std::real(integrate(pow(abs(uAna - uFem), 2), omega, G));
  D = std::real(integrate(pow(abs(uAna),        2), omega, G));
}

void closeddm::utils::save(const SEvObject &u,
                           const Domain &omega,
                           const std::string &name,
                           const Parameter &param,
                           const Onelab &ol){
  std::unordered_map<const SEvObject*, std::list<const Domain*> > uMap;
  uMap[&u].push_back(&omega);

  std::unordered_map<const Domain*, unsigned int> dMap;
  dMap[&omega] = 0;

  internal::save(uMap, dMap, false, name, param, ol);
}

void closeddm::utils::save(const SEvObject &u,
                           const Subdomain &omega,
                           const std::string &name,
                           const Parameter &param,
                           const Onelab &ol){
  std::unordered_map<const SEvObject*, std::list<const Domain*> > uMap;
  for(unsigned int i = 0; i < omega.numberOfSubdomains(); i++)
    if(isItMySubdomain(i))
      uMap[&u].push_back(&omega(i));

  std::unordered_map<const Domain*, unsigned int> dMap;
  for(unsigned int i = 0; i < omega.numberOfSubdomains(); i++)
    if(isItMySubdomain(i))
      dMap[&omega(i)] = i;

  internal::save(uMap, dMap, true, name, param, ol);
}

void closeddm::utils::save(const std::vector<const SEvObject*> &u,
                           const Subdomain &omega,
                           const std::string &name,
                           const Parameter &param,
                           const Onelab &ol){
  std::unordered_map<const SEvObject*, std::list<const Domain*> > uMap;
  for(unsigned int i = 0; i < omega.numberOfSubdomains(); i++)
    if(isItMySubdomain(i))
      uMap[u[i]].push_back(&omega(i));

  std::unordered_map<const Domain*, unsigned int> dMap;
  for(unsigned int i = 0; i < omega.numberOfSubdomains(); i++)
    if(isItMySubdomain(i))
      dMap[&omega(i)] = i;

  internal::save(uMap, dMap, true, name, param, ol);
}

void closeddm::utils::save(const std::vector<const SEvObject*> &u,
                           const Domain &omega,
                           const std::string &name,
                           const Parameter &param,
                           const Onelab &ol){
  if(u.size() != 1)
    throw Exception("closeddm::utils::save(): u.size() must be 1, "
                    "when used with a single domain");

  std::unordered_map<const SEvObject*, std::list<const Domain*> > uMap;
  uMap[u[0]].push_back(&omega);

  std::unordered_map<const Domain*, unsigned int> dMap;
  dMap[&omega] = 0;

  internal::save(uMap, dMap, true, name, param, ol);
}

void closeddm::utils::internal::
save(std::unordered_map<const SEvObject*,
                        std::list<const Domain*> > &uMap,
     std::unordered_map<const Domain*, unsigned int> &dMap,
     bool isPartitioned,
     const std::string &name,
     const Parameter &param,
     const Onelab &ol){
  // Save ? //
  if(!param.getFlag("save"))
    return;

  // Create view(s) //
  std::unordered_map<const Domain*, int> view;
  for(auto ui = uMap.begin(); ui != uMap.end(); ui++)
    for(auto di = ui->second.begin(); di != ui->second.end(); di++)
      view[*di] = gmshfem::post::save(*ui->first, **di, name, "", "",
                                      true, 0, 0, dMap[*di]);

  // Filename(s) //
  std::unordered_map<const Domain*, std::string> filename;
  if(isPartitioned)
    for(auto di = dMap.begin(); di != dMap.end(); di++)
      filename[di->first] = name + "_" + std::to_string(di->second) + ".msh";
  else
    filename[dMap.begin()->first] = name + ".msh";

  // Save //
  for(auto di = dMap.begin(); di != dMap.end(); di++)
    gmsh::view::write(view[di->first], filename[di->first]);

  // Forget //
  for(auto di = dMap.begin(); di != dMap.end(); di++)
    gmsh::view::remove(view[di->first]);

  // Merge //
  if(param.getFlag("merge"))
    for(auto fi = filename.begin(); fi != filename.end(); fi++)
      ol.merge(fi->second);
}

std::vector<const SEvObject*>
closeddm::utils::subfields(const Problem &prob,
                           const Subdomain &omega,
                           const std::string &name){
  size_t nDom = omega.numberOfSubdomains();

  std::vector<const SEvObject*> tmp(nDom);
  for(size_t i = 0 ; i < nDom; i++)
    tmp[i] = &prob.field(name, i, -1);

  return tmp;
}

std::vector<const SEvObject*>
closeddm::utils::subfields(const Problem &prob,
                           const Domain &omega,
                           const std::string &name){
  std::vector<const SEvObject*> tmp(1);
  tmp[0] = &prob.field(name, 0, -1);

  return tmp;
}

std::vector<const SEvObject*>
closeddm::utils::subfields(SField0 &f){
  size_t nDom = f.size();

  std::vector<const SEvObject*> tmp(nDom);
  for(size_t i = 0 ; i < nDom; i++)
    tmp[i] = &f(i);

  return tmp;
}

bool closeddm::utils::isInPolygon(cmplx z, const std::vector<cmplx> &q){
  // Uses Jordan curve theorem, i.e. odd or even crossing of a *ray*,         //
  // which we assume to be shot for increasing values of x.                   //
  // To this end, we need to determine the intersection of                    //
  //   - the ray "y_ray(x)  = Y", for all x >= X (see above) with             //
  //   - an edge "y_edge(x) = dy/dx * (x-x0) + y0",                           //
  // assuming a point z of coordnate (X, Y).                                  //
  // The intersecting point xi satisfying the above system of equation is     //
  //     xi = (Y-y0) * dx/dy + x0.                                            //
  // Because of our assumption on the direction of the rays, we can determine //
  // if the ray crosses the edge with the following condition:                //
  //     xi >= X  <=>  xi - X >= 0.                                           //
  // The point is on the edge if                                              //
  //     xi == X  <=>  xi - X == 0.                                           //
  //////////////////////////////////////////////////////////////////////////////

  // Is last vertex duplicated? Consider one vertex less if that's the case...
  size_t N = q.size();
  if(q[0] == q[N-1])
    N--;

  // Loop through the edges and keep track of of the "even or odd" crossings.
  // IMPORTANT: 'boundary cases' (i.e. point on vertex or node) are false,
  //            since we demand to be *strictly* inside the polygon...
  bool odd = false; // Zero crossing at the beginning: that's not an odd number!
  for(size_t i = 0, j = N-1; i < N; j = i++){ // NB: j = i++; --> j = i; i++;
    if(z == q[i])                                     // If x is on a vertex
      return false;                                   // ---> false
                                                      // ...
    if((q[i].imag() > z.imag()) !=                    // Could the ray intersect
       (q[j].imag() > z.imag())){                     // edge {q[i], q[j]}?
      double dx = (q[j].real()-q[i].real());          // --> Find intersection
      double dy = (q[j].imag()-q[i].imag());          // --> ...
      double xi =                                     // --> ...
        (z.imag()-q[i].imag()) * dx/dy + q[i].real(); // --> ...
                                                      // --> ...
      if(z.real() == xi)                              // --> If point on edge
        return false;                                 //     >>> false
      if(z.real() < xi)                               // --> If ray crosses edge
        odd = !odd;                                   //     >>> flip odd
    }
  }

  // Done (point is inside if the number of ray crossing is odd)
  return odd;
}

std::pair<std::vector<cmplx>, std::vector<cmplx> >
closeddm::utils::path::parse(std::string path){
  // A few const stuff
  const std::string self = "closeddm::utils::path::parse(): ";
  const std::string::size_type npos = std::string::npos;

  // Find paranthesis
  std::string::size_type open  = path.find_first_of("(");
  std::string::size_type close = path.find_last_of(")");
  if(open  == npos) throw Exception(self + "no '(' found");
  if(close == npos) throw Exception(self + "no ')' found");
  if(close <  open) throw Exception(self + "')' must come after '('");

  // Function name
  std::string name = path.substr(0, open);
  if(name.empty()) throw Exception(self + "no name given to path");

  // Remove space in arguments
  std::string tmp = path.substr(open+1, close-open-1);
  tmp.erase(std::remove(tmp.begin(), tmp.end(), ' '), tmp.end());

  // C string
  char *args = new char[tmp.length() + 1];
  strcpy(args, tmp.c_str());
  tmp.clear();

  // Parse arguments
  std::list<double> lst;
  for(char *p = strtok(args, ","); p != NULL; p = strtok(NULL, ",")){
    try       { lst.push_back(std::stod(p)); }
    catch(...){ throw Exception(self + std::string(p) + " is not a number"); }
  }
  std::vector<double> a(lst.begin(), lst.end());
  lst.clear();

  // Call
  auto check =
    [&self](const std::vector<double> &x, size_t n, std::string name)->void{
      if(n != x.size())
        throw Exception(self + name + " takes " +
                        std::to_string(n) + " arguments, but " +
                        std::to_string(x.size()) + " were given");
    };

  if(name == "circle"){
    check(a, 4, name);
    return circle(a[0]+J*a[1], a[2], a[3]);
  }
  else{
    throw Exception(self + "function " + name + " is unknown");
  }

  // Done
  delete[] args;
  return std::make_pair(std::vector<cmplx>(0), std::vector<cmplx>(0));
}

std::pair<std::vector<cmplx>, std::vector<cmplx> >
closeddm::utils::path::circle(cmplx q0, double r, size_t N){
  // Alloc for N points, i.e. {q[0], q[1], ..., q[N-1]}
  std::pair<std::vector<cmplx>, std::vector<cmplx> > c;
  c.first.resize(N);
  c.second.resize(N);

  // Populate N first points, i.e. from q[0] to q[N-1]
  for(size_t i = 0; i < N; i++){
    double t = 2.*Pi * (double)(i) / (double)(N);
    cmplx  e = r * std::exp(J * t);

    c.first[i]  = q0 + e;
    c.second[i] = J * e;
  }

  // Done
  return c;
}

template<>
gmshfem::field::FunctionSpaceTypeForm0
closeddm::utils::functionspace::family(std::string name){
  gmshfem::field::FunctionSpaceTypeForm0 HGrad;
  if(name == "lagrange")
    HGrad = gmshfem::field::FunctionSpaceTypeForm0::Lagrange;
  else if(name == "hierarchical")
    HGrad = gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1;
  else
    throw Exception("Unknown FunctionSpaceTypeForm0 family: " + name);
  return HGrad;
}

// Explicit instantiation //
////////////////////////////
namespace closeddm::utils{
  template int calc<int>(const int&, const std::string&);
  template cmplx calc<cmplx>(const cmplx&, const std::string&);
  template double calc<double>(const double&, const std::string&);

  template void error<Field0,
                      Domain>(const Field0&,
                              const Domain&,
                              const std::string&,
                              const Parameter&, const Onelab&);
  template void error<std::vector<const SEvObject*>,
                      Subdomain>(const std::vector<const SEvObject*>&,
                                 const Subdomain&,
                                 const std::string&,
                                 const Parameter&, const Onelab&);
  template void error<std::vector<const SEvObject*>,
                      Domain>(const std::vector<const SEvObject*>&,
                              const Domain&,
                              const std::string&,
                              const Parameter&, const Onelab&);
}
