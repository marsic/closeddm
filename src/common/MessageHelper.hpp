// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_MESSAGE_HELPER_H_
#define _CLOSEDDM_MESSAGE_HELPER_H_

#include "gmshfem/Message.h"
#include "MPIHelper.h"

namespace closeddm{
  namespace msg{
    /// Handles messages in an MPI framework

    /// @see closeddm::mpi
    template<typename T>
    class MessageHelper{
    private:
      T *out;

    public:
      /// Instanciates a new MessageHelper with the given output `out`
      MessageHelper(T &out){ this->out = &out; };
      /// Destructor
      ~MessageHelper(void){ };

      /// Inserts formated data `u` in this MessageHelper,
      /// if and only if mpi::master() is `true`
      template<typename U>
      MessageHelper &operator<<(const U &u){
        if(mpi::master())
          *out << u;
        return *this;
      };

      /// Inserts modificator `u` in this MessageHelper,
      /// if and only if mpi::master() is `true`
      template<gmshfem::common::MessageModificatorType U>
      MessageHelper &operator<<(gmshfem::common::MessageModificator<U> &u){
        if(mpi::master())
          *out << u;
        return *this;
      };
    };
  }
}

#endif
