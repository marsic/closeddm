// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_UTILS_HPP_
#define _CLOSEDDM_UTILS_HPP_

#include "Constant.h"

template<class T, class U>
void closeddm::utils::trapeze(const std::vector<U> &q,
                              const std::function<void(const U &x, T &ret)> &g,
                              const std::function<void(const size_t &p,
                                                       const U &x, U &ret)> &fp,
                              std::vector<T> &I){
  // Indices of 'current' and 'previous' evaluation point for trapeze rule
  unsigned int prv = 0, cur = 1;

  // Buffers for evaluating g and f_p
  std::vector<T> gV;
  gV.reserve(2);
  gV.emplace_back(I[0]);
  gV.emplace_back(I[0]);

  std::vector<U> fV[2];
  fV[prv].resize(I.size());
  fV[cur].resize(I.size());

  // Lambda to evaluate g and f_p at some point x
  // and to store the results in their respective buffer gV and fV
  auto eval = [](const std::function<void(const U &x, T &ret)> &g,
                 const std::function<void(const size_t &p,
                                          const U &x, U &ret)> &fp,
                 const U &x,
                 T &gV,
                 std::vector<U> &fV)->void{
    g(x, gV);
    for(size_t p = 0; p < fV.size(); p++)
      fp(p, x, fV[p]);
  };

  // Initialze trapeze loop by evaluating g(q[0]) and f_p(q[0])
  eval(g, fp, q[0], gV[prv], fV[prv]);

  // Loop over all points but first (already evaluated),
  // i.e. the sequence q[i] \forall i \in Q = \{1, ..., N-1},
  // where N = q.size() and compute for all p the trapeze sum:
  //     I_p = \sum_{k \in Q} (h_p(q[i-1]) + h_p(q[i])) * delta[i],
  // where h_p(q) = f_p(q) * g(q) and delta[i] = q[i] - q[i-1]
  // NB: leftover 1/2 is added later
  for(size_t i = 1; i < q.size(); i++){  // For each point in Q:
    eval(g, fp, q[i], gV[cur], fV[cur]); //  * evaluate functions at q[i],
    for(size_t p = 0; p < I.size(); p++) //  * for each I[p], add to sum and
      I[p] += (fV[prv][p]*gV[prv] + fV[cur][p]*gV[cur]) * (q[i] - q[i-1]);
    std::swap(cur, prv);                 //  * swap cur with prv
  }

  // Add leftover 1/2
  for(size_t p = 0; p < I.size(); p++)
    I[p] *= 1./2.;
}

template<class T, class U>
void closeddm::utils::trapezeClosed(const std::vector<U> &q,
                                    const std::vector<U> &jac,
                                    const std::function<void(const U &x,
                                                                   T &ret)> &g,
                                    const std::function<void(const size_t &p,
                                                             const U &x,
                                                                   U &ret)> &fp,
                                    std::vector<T> &I){
  // Buffers for g and fp
  T gV(I[0]);
  std::vector<U> fV(I.size());

  // Integration loop
  for(size_t i = 0; i < q.size(); i++){   // For each q[i] of the contour
    g(q[i], gV);                          //  * evaluate g at q[i]
    for(size_t p = 0; p < I.size(); p++){ //  * for each p
      fp(p, q[i], fV[p]);                 //     > evaluate fp at q[i]
      I[p] += fV[p] * gV * jac[i];        //     > add to sum
    }
  }

  // Leftover 1/N, where N is the number of sampling points
  for(size_t p = 0; p < I.size(); p++)
    I[p] *= 2. * closeddm::constant::Pi / ((double)(q.size()));
}

#endif
