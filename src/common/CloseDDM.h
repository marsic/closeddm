// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_H_
#define _CLOSEDDM_H_

#include <complex>
#include "gmshddm/InterfaceField.h"
#include "gmshddm/SubdomainField.h"
#include "gmshfem/Field.h"
#include "gmshfem/Function.h"

/// The closeddm main namespace
namespace closeddm{
  /// @brief Alias for `neighbourhood` maps
  /// @details For instance nmap[i][j] is the associated with the interface
  /// between subdomains `i` and `j`, on the side of `i`
  template<class T>
    using    NMap = std::vector<std::unordered_map<unsigned int, T> >;
  /// Alias for maps with strings as keys
  template<class T>
    using    SMap = std::unordered_map<std::string, T>;

  /// Alias for complex numbers
  using     cmplx = std::complex<double>;
  /// Alias for complex 0-form fields
  using    Field0 = gmshfem::field::Field<cmplx, gmshfem::field::Form::Form0>;
  /// Alias for complex 0-form subfields
  using   SField0 = gmshddm::field::SubdomainField<cmplx,
                                                   gmshfem::field::Form::Form0>;
  /// Alias for complex 0-form interface fields
  using   IField0 = gmshddm::field::InterfaceField<cmplx,
                                                   gmshfem::field::Form::Form0>;
  /// Alias for complex scalar function
  using SFunction = gmshfem::function::ScalarFunction<cmplx>;
  /// Alias for complex scalar evaluable objects
  using SEvObject = gmshfem::function::
                    GeneralEvaluableObject<cmplx, gmshfem::Degree::Degree0>;
}

#endif
