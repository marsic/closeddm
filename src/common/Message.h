// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_MESSAGE_H_
#define _CLOSEDDM_MESSAGE_H_

#include "gmshfem/Message.h"
#include "MessageHelper.hpp"

namespace closeddm{
  /// Output messages

  /// @note Wrapper for
  ///   - gmshfem::common::MessageModificatorType
  ///   - gmshfem::common::MessageType
  /// @see https://gitlab.onelab.info/gmsh/fem/-/blob/master/src/common/Message.h
  namespace msg{
    /// End of line
    static auto&       endl = gmshfem::msg::endl;
    /// Flush
    static auto&      flush = gmshfem::msg::flush;
    /// Fill
    static auto&       fill = gmshfem::msg::fill;
    /// Number in fixed precision format
    static auto&      fixed = gmshfem::msg::fixed;
    /// Set number precision
    static auto&  precision = gmshfem::msg::precision;
    /// Number in scientific format
    static auto& scientific = gmshfem::msg::scientific;


    /// Normal output
    static auto&   print = gmshfem::msg::print;
    /// Info output
    static auto&    info = gmshfem::msg::info;
    /// Warning output
    static auto& warning = gmshfem::msg::warning;
    /// Error output
    static auto&   error = gmshfem::msg::error;
    /// Debug output
    static auto&   debug = gmshfem::msg::debug;
  }
}

#endif
