// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/Exception.h"

#include "CloseDDM.h"
#include "CloseDDMDefines.h"
#include "MPIHelper.h"

int closeddm::mpi::init(int argc, char **argv){
#ifdef HAVE_MPI
  return MPI_Init(&argc, &argv);
#else
  return 0;
#endif
}

int closeddm::mpi::finalize(void){
#ifdef HAVE_MPI
  return MPI_Finalize();
#else
  return 0;
#endif
}

bool closeddm::mpi::master(void){
#ifdef HAVE_MPI
  return (mpi::rank(MPI_COMM_WORLD) == 0);
#else
  return true;
#endif
}

template<class T>
T closeddm::mpi::allreduce(T send, char op){
#ifdef HAVE_MPI
  T recv;
  MPI_Allreduce(&send, &recv, 1, trait<T>::type, operation(op), MPI_COMM_WORLD);
  return recv;
#else
  return send;
#endif
}

#ifdef HAVE_MPI
int closeddm::mpi::rank(MPI_Comm com){
  int rank;
  MPI_Comm_rank(com, &rank);
  return rank;
}

MPI_Op closeddm::mpi::operation(char op){
  std::string name = "MPIHelper::operation: ";

  switch(op){
  case '+': return MPI_SUM;
  default:  throw gmshfem::common::Exception(name + "unknown operator: " + op);
  }

  // You should never end up here...
  throw gmshfem::common::Exception(name + "something went wrong :(...");
  return MPI_SUM;
}
#endif

// Explicit instantiation & Traits //
/////////////////////////////////////
namespace closeddm::mpi{
  template double allreduce<double>(double send, char op);
}

#ifdef HAVE_MPI
template<> MPI_Datatype closeddm::mpi::trait<double>::type = MPI_DOUBLE;
#endif
