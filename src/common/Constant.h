// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_CONSTANT_H_
#define _CLOSEDDM_CONSTANT_H_

#include <cmath>
#include <complex>

namespace closeddm{
  /// Some mathematical constants
  namespace constant{
    /// Pi
    constexpr double Pi = std::atan(1) * 4;
    /// Imaginary unit
    constexpr std::complex<double> J(0, 1);
  }
}

#endif
