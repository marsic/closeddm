// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_UTILS_H_
#define _CLOSEDDM_UTILS_H_

#include <list>
#include <string>

#include "gmshddm/Subdomain.h"
#include "gmshfem/GeneralEvaluableObject.h"

#include "CloseDDM.h"
#include "Onelab.h"
#include "Parameter.h"
#include "Problem.h"
// NB: include "Utils.hpp" at end of file !

namespace closeddm{
  /// A library with some useful functions
  namespace utils{
    /// Symbol for imaginary unit
    const std::string imaginarySymbol = "i";

    /// Returns the result of the simple arthemtic operation `op` on `val1.
    /// For instance, `calc(42., "/2")` returns 21
    template<class T> T calc(const T &val, const std::string &op);
    /// Returns the string associated with `z` with the precision `precision`
    template<class T> std::string to_string(T z, unsigned int precision=4);
    /// Returns `a + constant::J*b`
    cmplx complexify(double a, double b);
    /// Returns `constant::J * std::real(k)*gamma/100.`
    cmplx damping(cmplx k, double gamma);

    /// Dumps the given Parameter `param` on msg::print
    /// if `param.getFlag("dump")` is `true`
    void dump(const closeddm::input::Parameter &param);
    /// Writes `txt` in `os` as a table of width `width` and separator `sep`
    void table(std::ostream &os,
               const std::vector<std::string> &txt,
               size_t width=15, std::string sep="|");
    /// Same as above, but where `width` can be different between columns
    void table(std::ostream &os,
               const std::vector<std::string> &txt,
               const std::vector<size_t> &width,
               std::string sep="|");
    /// Returns a string resulting from the concatenation of a list `lst`
    /// of strings, with each entry separated with the symbol `sym`
    std::string cat(const std::list<std::string> &lst, std::string sym="");
    /// Let `argv` be an array of char* (i.e. 'c strings') of size `argc` and
    /// organized as follows:
    ///   * some entries start with a `-`; they are called options (o) and
    ///   * some are not; they are called values (v).
    /// A value is associated to its nearest left option.
    /// For instance, in the following array [... o_1 v_1 v_2 v_3 o_2 ...]
    /// the three values (v_1, v_2 and v_3) are associated with o_1.
    /// This function returns a map with all options in `argv`
    /// and their associated values.
    std::unordered_map<std::string, std::list<std::string> > args(int argc,
                                                                  char **argv);

    /// Uses the trapezoidal rule to evaluate the integrals:
    ///   \f[I_p = \int_\Gamma g(x) f_p(x) \mathrm{d}x
    ///      \quad\forall p \in \{0, 1, \dots, P-1\}\f]
    /// where \f$g\f$ is a function from U to T,
    /// \f$f_p\f$ are functions from U to U and
    /// \f$\Gamma\f$ is the one dimentional integraion domain
    /// (a contour in the complex plane for instance).
    ///
    /// @param[in] q A vector representing the points of U sampling \f$\Gamma\f$
    /// @param[in] g A function representing \f$g(x)\f$
    /// @param[in] fp A function representing \f$f_p(x)\f$
    /// @param[out] I The vector of size \f$P\f$ storing the resulting integrals
    ///
    /// @see <https://en.wikipedia.org/wiki/Trapezoidal_rule>
    /// @warning U and T must be such that the product
    ///          \f$(g f_p)\in T\f$ is well defined
    ///          (U could be the set of complex numbers and
    ///           T the set of (n by m) matrices on U, for instance)
    template<class T, class U>
    void trapeze(const std::vector<U> &q,
                 const std::function<void(const U &x, T &ret)> &g,
                 const std::function<void(const size_t &p,
                                          const U &x, U &ret)> &fp,
                 std::vector<T> &I);
    /// This is a "cheaper" version of trapeze() that needs less storage
    /// and one evaluation less, but at the cost of:
    ///
    ///   1. having a *closed* curve \f$\Gamma\f$
    ///      parametrized with \f$t \in [0, 2\pi]\f$,
    ///   2. having \f$t\f$ sampled with *equidistant* points and
    ///   3. having the jacobian of the parametrization at the sampling points.
    ///
    /// This functions has one additional input parameter
    /// (when compared with trapeze()), namely the jacobian:
    /// @param[in] jac The jacobian of the parametrization evaluated at each `q`
    ///
    /// @note The closed curve \f$\Gamma\f$ is given by the sequence:
    ///         \f[\{\mathtt{q[0], q[1], ..., q[N-1], q[0]}\}.\f]
    ///       In other words, the "starting" point must be counted only once
    template<class T, class U>
    void trapezeClosed(const std::vector<U> &q,
                       const std::vector<U> &jac,
                       const std::function<void(const U &x, T &ret)> &g,
                       const std::function<void(const size_t &p,
                                                const U &x, U &ret)> &fp,
                       std::vector<T> &I);

    /// Computes the relative L2 error between the discrete field `u`
    /// (defined on `omega`) and its analytical (closed-form) solution, that is
    /// \f[\sqrt{\int_\Omega \frac{(u-u_\text{ana})^2}
    ///                           {u_\text{ana}^2}\mathrm{d}\Omega},\f]
    /// where \f$u\f$ is the discrete field
    /// and \f$u_\text{ana}\f$ its analytical solution.
    ///
    /// This function can be altered with the following Parameter in `param`:
    ///  - "error", do nothing if "none", otherwise compute
    /// the analytical solution according to analytic::factory()
    ///  - "gauss", the quadrature rule for computing the above integral
    ///    (the actual number used is "gauss" * 2)
    ///
    /// Upon success, the computed L2 error is set into
    /// the ONELAB instance `ol` (entry '05Post/03Relative L2 error')
    /// and the analytical solution is saved on disk (with the filename `name`)
    template<class F, class D>
    void   error(const F &u, const D &omega, const std::string &name,
                 const closeddm::input::Parameter &param,
                 const closeddm::input::Onelab &ol);
    /// Computes
    /// \f[\sqrt{\int_\Omega \frac{(u_\text{fem}-u_\text{ana})^2}
    ///                           {u_\text{ana}^2}\mathrm{d}\Omega}\f]
    /// using `gauss` integration points,
    /// where \f$\Omega\f$ is the integration domain `omega`,
    /// \f$u_\text{ana}\f$ is the scalar function `uAna` and
    /// \f$u_\text{fem}\f$ is the discrete field `uFem`
    double error(const SEvObject &uAna,
                 const SEvObject &uFem,
                 const gmshfem::domain::Domain &omega, int gauss);
    /// Same as above, but with the discrete field `*uFem[i]`
    /// defined on the subdomain `omega(i)`
    double error(const SEvObject &uAna,
                 const std::vector<const SEvObject*> &uFem,
                 const gmshddm::domain::Subdomain &omega, int gauss);
    /// Same as above, but with the discrete field `*uFem[0]`
    /// defined on the domain `omega`
    double error(const SEvObject &uAna,
                 const std::vector<const SEvObject*> &uFem,
                 const gmshfem::domain::Domain &omega, int gauss);

    /// Saves on disk, in a file named `name`,
    /// the field `u` defined on domain `omega`.
    /// This function can be altered with the following Parameter in `param`:
    ///  - "save", do not save if `false`
    ///  - "merge", merge the saved file in the ONELAB instance `ol`
    void save(const SEvObject &u,
              const gmshfem::domain::Domain &omega,
              const std::string &name,
              const closeddm::input::Parameter &param,
              const closeddm::input::Onelab &ol);
    /// Same as above, but now `u` is defined on each subdomain `omega`
    void save(const SEvObject &u,
              const gmshddm::domain::Subdomain &omega,
              const std::string &name,
              const closeddm::input::Parameter &param,
              const closeddm::input::Onelab &ol);
    /// Same as above, but now `*u[i]` is defined on subdomain `omega(i)`
    void save(const std::vector<const SEvObject*> &u,
              const gmshddm::domain::Subdomain &omega,
              const std::string &name,
              const closeddm::input::Parameter &param,
              const closeddm::input::Onelab &ol);
    /// Same as above, but now `*u[0]` is defined on domain `omega`
    void save(const std::vector<const SEvObject*> &u,
              const gmshfem::domain::Domain &omega,
              const std::string &name,
              const closeddm::input::Parameter &param,
              const closeddm::input::Onelab &ol);

    /// Returns a vector `v`, such that `v[i]` is the portion of the
    /// discrete field `name`, defined on the subdomain `omega(i)`,
    /// of the given Problem `prob`.
    std::vector<const SEvObject*>
    subfields(const closeddm::problem::Problem &prob,
              const gmshddm::domain::Subdomain &omega,
              const std::string &name);
    /// Same as above but for a single domain.
    std::vector<const SEvObject*>
    subfields(const closeddm::problem::Problem &prob,
              const gmshfem::domain::Domain &omega,
              const std::string &name);
    /// Returns a vector `v`, such that `v[i]` is a SEvObject pointer
    /// pointing toward `f(i)`, where `f` is a SField0.
    std::vector<const SEvObject*>
    subfields(SField0 &f);

    /// Returns true if `z` is *strictly* inside the polygon given by `q`,
    /// and false otherwise.
    /// The vertices of the polygon are given in the *complex* plane
    /// and each edge is given by the vertices {q[i], q[i+1]}
    /// (modulo the number of unique vertices).
    /// @note @parblock
    ///       Given a polygon with N unique vertices, `q` can be either of size:
    ///
    ///         - N, if each vertex is unique or
    ///         - N+1, if q[N] := q[0], that is the "first" vertex comes twice;
    ///
    ///       in both cases, the last edge is {q[N-1], q[0]}
    ///       @endparblock
    /// @see Inspired from
    ///        - <https://en.wikipedia.org/wiki/Point_in_polygon>,
    ///        - <https://en.wikipedia.org/wiki/Jordan_curve_theorem>,
    ///        - <https://en.wikipedia.org/wiki/Even%E2%80%93odd_rule> and
    ///        - <https://wrfranklin.org/Research/Short_Notes/pnpoly.html>
    bool isInPolygon(cmplx z, const std::vector<cmplx> &q);

    /// Some useful functions to create a path in the complex plane
    namespace path{
      /// Parses the given string, `path`, and returns the associated path
      /// (see the other functions of this namespace for relevant calls).
      /// @note A complex number is encoded with two consecutive arguments.
      /// For instance, in the case of a circular path, one has:
      ///
      ///     auto q = path::parse("circle(16, 0, 5, 88)");
      ///
      /// which is equivalent to
      ///
      ///     auto q = path::circle(cmplx(16, 0), 5, 88);
      std::pair<std::vector<cmplx>,
                std::vector<cmplx> > parse(std::string path);

      /// Let \f$\mathcal{C}\f$ be a circle of radius r and center q0
      /// in the complex plane.
      /// Let this curve be parametrized with \f$t\in[0,2\pi]\f$,
      /// where \f$t\f$ is sampled with N *equidistant* points \f$t_i\f$.
      /// Returns a pair of vectors:
      ///   * the first vector of the pair
      ///     gives the values \f$\mathcal{C}(t_i)\f$
      ///   * the second vector of the pair
      ///     gives the values \f$\mathcal{C}^\prime(t_i)\f$,
      ///     that is the jacobian of the parametrization
      /// @warning The sampled sequence is [z[0], z[1], ..., z[N-1]].
      ///          In other words, the "starting" point is counted
      ///          only once and the returned vector has therefore a size of N
      std::pair<std::vector<cmplx>,
                std::vector<cmplx> > circle(cmplx q0, double r, size_t N);
    }

    /// Some useful functions to create function spaces
    namespace functionspace{
      /// Returns the requested gmshfem::field::FunctionSpaceTypeForm{0,1,2,3}
      template<class T> T family(std::string name);
    }

    namespace internal{
      void error(const SEvObject &uAna, const SEvObject &uFem,
                 const gmshfem::domain::Domain &omega, int gauss,
                 double &N, double &D);

      template<class F, class D>
      void save(const F &u, const D &omega,
                const std::string &name,
                const closeddm::input::Parameter &param,
                const closeddm::input::Onelab &ol);
      void save(std::unordered_map<const SEvObject*,
                                   std::list<const gmshfem::
                                                       domain::Domain*> > &uMap,
                std::unordered_map<const gmshfem::domain::Domain*,
                                   unsigned int> &pMap,
                bool isPartitioned,
                const std::string &name,
                const closeddm::input::Parameter &param,
                const closeddm::input::Onelab &ol);
    }
  }
}

#include "Utils.hpp"
#endif
