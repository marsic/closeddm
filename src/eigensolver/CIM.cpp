// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include <numeric>
#include "CIM.h"
#include "CloseDDM.h"
#include "Constant.h"
#include "Field.h"
#include "Utils.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace closeddm::input;
using namespace closeddm::problem;
using namespace closeddm::eigensolver::cim_internal;
using namespace gmshfem::common;

namespace closeddm::eigensolver{
  void cim(Parameter &param,
           Problem &prob,
           const std::string &field,
           const std::string &source,
           const std::pair<std::vector<cmplx>, std::vector<cmplx> > &contour,
           std::vector<cmplx> &lambda,
           std::vector<Field0> &v,
           std::vector<double> &residual,
           std::vector<std::list<std::string> > &status){
    // Namespace
    using std::string;

    // Parameters
    size_t nSnapStart = param.getNumber("cim_start");
    size_t nSnapStep  = param.getNumber("cim_step");
    double thr        = param.getNumber("cim_thr");
    string vHatKind   = param.getString("cim_vhat");
    bool   gs         = param.getFlag("cim_gs") == 1;
    size_t maxIt      = param.getNumber("eig_maxIt");
    double tol        = param.getNumber("eig_tol");

    // Contour
    std::vector<cmplx> q, jac;
    if(contour.second.empty() || param.getFlag("cim_jac") == 0){
      q.reserve(contour.first.size()+1);
      q.insert(q.begin(), contour.first.begin(), contour.first.end());
      q.push_back(q[0]); // Last point is first point
      jac.clear();       // No Jacobian info
    }
    else{
      q   = contour.first;
      jac = contour.second;
    }

    // Number of formulations and pre-process to initialize a few things
    size_t nForm = prob.size();
    for(size_t f = 0; f < nForm; f++)
      prob.formulation(f).pre();

    // Find A
    std::vector<Matrix> A(2);
    SVD svd;
    svd.setThreshold(thr);
    findA(q, jac, prob, field, source, svd,
          nSnapStart, nSnapStep, maxIt, gs, vHatKind, A);

    // Compute B
    Matrix V0, B;
    buildB(A, svd, V0, B);

    // Solve eigenproblem B*x = lambda*x
    std::vector<gmshfem::algebra::Vector<cmplx> > x;
    eigenproblem(B, V0, q, jac, prob, field, tol,
                 lambda, x, residual, status);

    // Concatenated field from split field
    std::vector<Field0*> fieldS = prob.field(field);
    Field0 fieldC;
    Field::concatenate(fieldS, fieldC, true);
    fieldC.name("field_cat");

    // Save each eigenvector in a new field
    v.reserve(x.size());
    for(size_t i = 0; i < x.size(); i++){
      v.emplace_back(fieldC);          // Copy 'original' concatenated field
      v.back().setUnknownVector(x[i]); // Assign i-th eigenvector
    }
  }

  namespace cim_internal{
    void eigenproblem(const Matrix &A,
                      const Matrix &V0,
                      const std::vector<cmplx> &q,
                      const std::vector<cmplx> &jac,
                      problem::Problem &prob,
                      const std::string &field,
                      double tol,
                      std::vector<cmplx> &lambda,
                      std::vector<gmshfem::algebra::Vector<cmplx> > &x,
                      std::vector<double> &residual,
                      std::vector<std::list<std::string> > &status){
      // Status strinsg
      const std::string ok   = "ok";  // OK, everything went fine :)
      const std::string out  = "out"; // Outside contour
      const std::string tolr = "tol"; // Tolerance not reached

      // Solve eigenproblem A*x = lambda*x
      Eigen::ComplexEigenSolver<Matrix> eig;
      eig.compute(A, true);

      // Allocate space for eigenvalues, eigenvectors and status
      size_t nEig = eig.eigenvalues().size();
      size_t nDof = V0.rows();
      lambda.reserve(nEig);
      x.reserve(nEig);
      residual.reserve(nEig);
      status.resize(nEig);

      // Print
      const std::vector<size_t> width = {8, 27, 13, 20};
      utils::table(std::cout, {"Index", "Eigenvalue", "Residual", "Status"},
                   width);

      // Get eigenpairs and determine their status
      for(size_t i = 0; i < nEig; i++){
        // Get i-th eigenvalue, check if it is inside the contour and save it
        cmplx cl = eig.eigenvalues()[i]; // Get i-th candidate
        if(!utils::isInPolygon(cl, q))   // If it is not inside the contour,
          status[i].push_back(out);      // --> mark it
        lambda.push_back(cl);            // Save candidate eigenvalue

        // Get i-th eigenvector and save it
        std::vector<cmplx> cx(nDof);
        Eigen::Map<Matrix> cxm(cx.data(), nDof, 1);
        cxm = V0 * eig.eigenvectors().col(i);
        x.emplace_back(std::move(cx));

        // Refresh problem with current eigenvalue
        refresh(prob, cl);

        // Get split field from problem
        std::vector<Field0*> vS = prob.field(field);

        // Assign current eigenvector to concatenated field
        Field0 vC;
        Field::concatenate(vS, vC, true);
        vC.name("v_cat");
        vC.setUnknownVector(x.back());

        // Check residual (continuous level) and save it
        Field::split(vC, vS);          // Split field
        double res = prob.residual();  // Residual
        if(res > tol)                  // If residual is too high,
          status[i].push_back(tolr);   // --> mark it
        residual.push_back(res);       // Save residual

        // Save status
        if(status[i].empty())      // If no particular status so far,
          status[i].push_back(ok); // --> mark the eigenpair as ok

        // Print
        utils::table(std::cout, {std::to_string(i),
                                 utils::to_string(cl),
                                 utils::to_string(res),
                                 utils::cat(status[i], "+")}, width);
      }
    }

    void buildB(const std::vector<Matrix> &A, const SVD &svd,
                Matrix &V0, Matrix &B){
      // Sanity
      if(A.size() < 2)
        throw Exception("CIM::buildB(): we must have A.size() >= 2");

      // Proxies
      const Matrix &V = svd.matrixU();
      const Matrix &W = svd.matrixV();
      const Matrix &S = svd.singularValues();

      // Sizes
      const size_t nDofM  = A[0].rows() - 1;
      const size_t nSnapM = A[0].cols() - 1;
      const size_t rankM  = svd.rank()  - 1;

      // V0
      V0 = V(Eigen::seq(0,  nDofM), Eigen::seq(0, rankM));

      // B = V0^H * A[1] * W0 * S^{-1}
      B = V0.adjoint() * A[1] * W(Eigen::seq(0, nSnapM), Eigen::seq(0, rankM));
      for(Eigen::Index j = 0; j < B.cols(); j++)
        B.col(j) /= S(j);
    }

    void findA(const std::vector<cmplx> &q,
               const std::vector<cmplx> &jac,
               problem::Problem &prob,
               const std::string &field,
               const std::string &source,
               SVD &svd,
               size_t nSnapStart, size_t nSnapStep, size_t maxIt,
               bool gs, const std::string &vHatKind,
               std::vector<Matrix> &A){
      // Split source field
      std::vector<Field0*> sourceS = prob.field(source);

      // Concatenated source field
      Field0 sourceC;
      Field::concatenate(sourceS, sourceC, true);
      sourceC.name("source_cat");

      // Number of DoFs in the source and total sum
      size_t nDof = sourceC.numberOfUnknownDofs();

      // Init vHat with nSnapStart snapshots
      std::list<gmshfem::algebra::Vector<cmplx> > vHat;
      incrVHat(nDof, nSnapStart, gs, vHatKind, vHat);

      // Print
      int width = 14;
      utils::table(std::cout,
                   {"Iteration", "col(vHat)", "rank(A[0])", "dof(source)",
                    "avrg(inner)"},
                   width);

      // Loop to construct the A[p]s
      unsigned int svdFlags = Eigen::ComputeThinU | Eigen::ComputeThinV;
      bool rankDrop = false;
      for(size_t it = 0; !rankDrop && vHat.size() <= nDof && it < maxIt; it++){
        std::list<size_t> inner;           // Keep track of inner iterations
        buildA(q, jac, vHat,               // Cmpt. the A[p]s assoc. with vHat
               prob, field, source,        // ...
               inner, A);                  // ...
        svd.compute(A[0], svdFlags);       // Perform the rank test on A[0]
        size_t rank = svd.rank();          // ...
        size_t l    = vHat.size();         // ...
                                           // ...
        if(rank == 0)                      // If we have a zero rank
          throw Exception("CIM::findA(): " // --> we're in trouble...
                          "zero rank!");   //     ...
        if(rank < l)                       // If we have a rank drop
          rankDrop = true;                 //  --> the A[p]s are found :)!
        else                               // Otherwise
          incrVHat(nDof, nSnapStep,        // ...
                   gs, vHatKind, vHat);    // --> increase vHat

        double average =
          std::accumulate(inner.begin(), inner.end(), 0.) / inner.size();
        utils::table(std::cout, {std::to_string(it + 1),
                                 std::to_string(l),
                                 std::to_string(rank),
                                 std::to_string(nDof),
                                 std::to_string(average)}, width);
      }

      // Info
      if(rankDrop)
        std::cout << "Rank drop found :-)!" << std::endl;
      else
        std::cout << "No rank drop found :-(!" << std::endl;
    }

    void buildA(const std::vector<cmplx> &q,
                const std::vector<cmplx> &jac,
                const std::list<gmshfem::algebra::Vector<cmplx> > &vHat,
                Problem &prob,
                const std::string &field,
                const std::string &source,
                std::list<size_t> &iteration,
                std::vector<Matrix> &A){
      // Sanity
      if(vHat.empty())
        throw Exception("CIM::buildA(): vHat is empty!");

      // Allocate the A[p]s
      for(size_t p = 0; p < A.size(); p++)
        A[p] = Matrix::Zero(vHat.back().size(), vHat.size());

      // Field solver tolerance and maximum iteration
      const int  maxIt = prob.parameter().getNumber("maxIt");
      const double tol = std::pow(10, prob.parameter().getNumber("tolE"));

      // Functions g and fp defining the integrals in the A[p]s, i.e.
      // A[p] = \int_Gamma g * f_p d\Gamma
      std::function<void(const cmplx &x, Matrix &ret)>
        g = [&prob, &vHat, &field, &source, &tol, &maxIt, &iteration]
                                            (const cmplx &x, Matrix &ret)->void{
          refresh(prob, x);
          solve(prob, vHat, field, source, "default",
                tol, maxIt, iteration, ret);
        };
      std::function<void(const size_t &p, const cmplx &x, cmplx &ret)>
        fp = [](const size_t &p, const cmplx &x, cmplx &ret)->void{
          ret = std::pow(x, p);
        };

      // Compute the A[p]s with trapezoidal rule
      if(jac.empty())
        utils::trapeze<Matrix, cmplx>(q, g, fp, A);
      else
        utils::trapezeClosed<Matrix, cmplx>(q, jac, g, fp, A);
      for(size_t p = 0; p < A.size(); p++)
        A[p] *= 1./(2.*J*Pi);
    }

    void refresh(problem::Problem &prob, cmplx lambda){
      // Set new lambda
      prob.parameter().setNumber("k",  std::real(lambda));
      prob.parameter().setNumber("kd", std::imag(lambda));
      prob.refresh();

      // Pre-process formulations
      for(size_t f = 0; f < prob.size(); f++)
        prob.formulation(f).pre();
    }

    void solve(Problem &prob,
               const std::list<gmshfem::algebra::Vector<cmplx> > &rhs,
               const std::string &field,
               const std::string &source,
               const std::string &solver,
               const double &tol,
               const int &maxIt,
               std::list<size_t> &iteration,
               Matrix &X){
      // Split source field
      std::vector<Field0*> sourceS = prob.field(source);

      // Concatenated source field
      Field0 sourceC;
      Field::concatenate(sourceS, sourceC, true);
      sourceC.name("source_cat");

      // Split solution field
      std::vector<Field0*> solutionS = prob.field(field);

      // Concatenated solution field
      Field0 solutionC;
      Field::concatenate(solutionS, solutionC, true);
      solutionC.name("solution_cat");

      // For each rhs[j], set it, solve for it and assign solution to X[:, j]
      gmshfem::algebra::Vector<cmplx> buf;
      std::list<gmshfem::algebra::Vector<cmplx> >::const_iterator
        it = rhs.begin();
      for(size_t j = 0; it != rhs.end(); j++, it++){
        sourceC.setUnknownVector(*it);                   // Set source field
        Field::split(sourceC, sourceS);                  // Split it

        prob.assemble();                                 // Assemble
        prob.solve(solver, tol, maxIt);                  // Solve
        iteration.push_back(prob.iteration());           // Inner iteration log

        Field::concatenate(solutionS, solutionC, false); // Concatenate solution
        solutionC.getUnknownVector(buf);                 // Set to buffer

        X.col(j) = buf.getEigen();                       // Add buffer to X
      }
    }

    void incrVHat(size_t nDof, size_t nStep, bool gs, const std::string &kind,
                  std::list<gmshfem::algebra::Vector<cmplx> > &vHat){
      for(size_t j = 0; j < nStep; j++){
        vHat.emplace_back(nDof);

        if(kind == "alt")
          populateVAlt(vHat.size(), vHat.back());
        else if(kind == "rnd")
          populateVRnd(vHat.size(), vHat.back());
        else
          throw Exception("CIM::incrVHat(): unknown kind: " + kind);

        if(!gs)
          vHat.back() /= vHat.back().norm();
      }
      if(gs)
        gramSchmidt(vHat);
    }

    void populateVAlt(size_t nVHat, gmshfem::algebra::Vector<cmplx> &vj){
      // Populate v_j with an alternating signal
      bool zero = true;
      for(size_t i = 0; i < vj.size(); i++){
        if(i%nVHat == 0)       // Alternate when needed
          zero = !zero;        // ...
                               // ...
        vj[i] = zero? 0. : 1.; // Assign 0 or 1 depending on flag
      }
    }

    void populateVRnd(size_t nVHat, gmshfem::algebra::Vector<cmplx> &vj){
      // Populate v_j with a random signal
      std::srand((unsigned int)(std::time(NULL)) + (unsigned int)(nVHat));
      for(size_t i = 0; i < vj.size(); i++)
        vj[i] = cmplx((double)(std::rand()), (double)(std::rand()));
    }

    void gramSchmidt(std::list<gmshfem::algebra::Vector<cmplx> > &x){
      size_t N = x.size();
      std::vector<std::list<gmshfem::algebra::Vector<cmplx> >::iterator> it(N);
      std::list<gmshfem::algebra::Vector<cmplx> >::iterator tmp = x.begin();
      for(size_t k = 0; k < N; k++, tmp++)
        it[k] = tmp;

      for(size_t k = 0; k < N; k++){
        for(size_t l = 0; l < k; l++)
          (*it[k]) -= (*it[l]) * ((*it[l]) * (*it[k]));

        *it[k] /= it[k]->norm();
      }
    }
  }
}
