// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_CIM_H_
#define _CLOSEDDM_CIM_H_

#include "Eigen/Dense"

#include "CloseDDM.h"
#include "EigenSolver.h"
#include "Problem.h"

namespace closeddm{
  namespace eigensolver{
    /// An eigenvalue solver implementing the contour integral method (CIM)
    /// proposed in [Beyn2012].
    /// In particular, it implements the "integral algorithm 1", which is suited
    /// for countours containing only a few eigenvalues.
    /// Formaly, the considered (possibly non-linear) eigenvalue problem
    /// reads as follows:
    /// \f[\text{Find the eigenpairs $(\lambda, v)$
    ///          lying in some closed countour $\Gamma$, such that:}\quad
    ///    T(\lambda)v = 0\quad
    ///    \text{with}~v\in\mathbb{C}^m, v\neq0~\text{and}~\lambda\in\Omega,\f]
    /// where \f$\Gamma\subset\Omega\subset\mathbb{C}\f$ and
    /// \f$T: \Omega\rightarrow\mathbb{C}^{m,m}\f$ is holomorphic in
    /// \f$\Omega\f$.
    /// In this function, the matrix \f$T\f$ is obtained by descretizing
    /// a finite element (FE) problem and extracting the diagonal block
    /// corresponding to a field of the FE problem.
    /// This diagonal block can be the entire matrix itself.
    ///
    /// @param[in] param The input::Parameter of this function (see notes below)
    /// @param[in] prob The problem::Problem defining \f$T\f$
    /// @param[in] field The name of the field in `prob` defining \f$T\f$
    /// @param[in] source The name of the volumic source field in `prob`
    /// @param[in] contour The pair of complex vectors
    ///            describing the contour \f$\Gamma\f$:
    ///              * the first vector of the pair gives the
    ///                complex coordinates of the points sampling \f$\Gamma\f$
    ///              * the second vector of the pair gives the
    ///                jacobian of the mapping between \f$\Gamma\f$ and a
    ///                parameter \f$t \in [0, 2\pi]\f$ sampled with
    ///                *equidistant* points; unused if jac.size() == 0
    /// @param[out] lambda The vector whose i-th entry is
    ///                    the i-th found eigenvalues \f$\lambda_i\f$
    /// @param[out] v The vector whose i-th entry is
    ///               the i-th found eigenvectors \f$v_i\f$
    /// @param[out] residual The vector whose i-th entry is
    ///                      the residual associated with the i-th eigenpair
    /// @param[out] status The vector whose i-th entry is a list of string
    ///                    with the status of the i-th eigenpair
    ///                    \f$(\lambda_i, v_i)\f$ (see notes below)
    ///
    /// @note
    ///  - The vector `contour.first` contains the following complex points:
    ///    [z[0], z[1], ... z[N-1]], that is the first point is
    ///    counted only once, even if \f$\Gamma\f$ is closed
    ///  - The relevant parameters for this function are:
    ///    + "cim_start", the number of snapshots (that is the number of columns
    ///      of \f$\hat{V}\f$ in [Beyn2012]) used at the beginning of the CIM,
    ///    + "cim_step", the amount by which the snapshots are incremented
    ///      at each iteration of the CIM,
    ///    + "cim_thr", the threshold used for determining the rank of a matrix,
    ///    + "cim_jac", if `0`, don't use the jacobian (even if provided),
    ///    + "cim_vhat", Strategy for building \f$\hat{V}\f$ ("alt" or "rnd"),
    ///    + "cim_gs", if `1`, use Gram-Schmidt process on \f$\hat{V}\f$,
    ///    + "eig_maxIt", the maximum number of iterations of the CIM and
    ///    + "eig_tol", the tolerance below which an eigenpair is accepted.
    ///  - By iteration, we mean the iterative process used to determines
    ///    a suitable \f$A_{0,N}\f$ [Beyn2010, p. 3849].
    ///  - The possible status are:
    ///    + "ok", the eigenpair satisfy all the criteria to be accepted,
    ///    + "out", the eigenvalue is not *strictly* inside \f$\Gamma\f$ and
    ///    + "tol", the eigenpair did not pass the tolerance test.
    ///
    /// @warning This function will modify the given problem::Problem
    ///          and its input::Parameter.
    ///
    /// @see [Beyn2010]: W.-J. Beyn, "An integral method for solving
    ///                  nonlinear eigenvalue problems",
    ///                  Linear Algebra and its Applications,
    ///                  vol. 436, no. 10, pp.  3839-3863, 2012,
    ///                  [DOI: 10.1016/j.laa.2011.03.030](
    ///                    https://doi.org/10.1016/j.laa.2011.03.030)
    void cim(input::Parameter &param,
             problem::Problem &prob,
             const std::string &field,
             const std::string &source,
             const std::pair<std::vector<cmplx>, std::vector<cmplx> > &contour,
             std::vector<cmplx> &lambda,
             std::vector<Field0> &v,
             std::vector<double> &residual,
             std::vector<std::list<std::string> > &status);

    namespace cim_internal{ // Helpers for cim
      using Matrix = Eigen::Matrix<cmplx, Eigen::Dynamic, Eigen::Dynamic,
                                   Eigen::ColMajor>;
      using SVD    = Eigen::JacobiSVD<Matrix>;

      void eigenproblem(const Matrix &A,
                        const Matrix &V0,
                        const std::vector<cmplx> &q,
                        const std::vector<cmplx> &jac,
                        problem::Problem &prob,
                        const std::string &field,
                        double tol,
                        std::vector<cmplx> &lambda,
                        std::vector<gmshfem::algebra::Vector<cmplx> > &v,
                        std::vector<double> &residual,
                        std::vector<std::list<std::string> > &status);

      void buildB(const std::vector<Matrix> &A, const SVD &svd,
                  Matrix &V0, Matrix &B);

      void findA(const std::vector<cmplx> &q,
                 const std::vector<cmplx> &jac,
                 problem::Problem &prob,
                 const std::string &field,
                 const std::string &source,
                 SVD &svd,
                 size_t nSnapStart, size_t nSnapStep, size_t maxIt,
                 bool gs, const std::string &vHatKind,
                 std::vector<Matrix> &A);
      void buildA(const std::vector<cmplx> &q,
                  const std::vector<cmplx> &jac,
                  const std::list<gmshfem::algebra::Vector<cmplx> > &vHat,
                  problem::Problem &prob,
                  const std::string &field,
                  const std::string &source,
                  std::list<size_t> &iteration,
                  std::vector<Matrix> &A);

      void refresh(problem::Problem &prob, cmplx lambda);
      void solve(problem::Problem &prob,
                 const std::list<gmshfem::algebra::Vector<cmplx> > &rhs,
                 const std::string &field,
                 const std::string &source,
                 const std::string &solver,
                 const double &tol,
                 const int &maxIt,
                 std::list<size_t> &iteration,
                 Matrix &x);

      void incrVHat(size_t nDof, size_t nStep, bool gs, const std::string &kind,
                    std::list<gmshfem::algebra::Vector<cmplx> > &vHat);
      void populateVAlt(size_t nVHat, gmshfem::algebra::Vector<cmplx> &vj);
      void populateVRnd(size_t nVHat, gmshfem::algebra::Vector<cmplx> &vj);

      void gramSchmidt(std::list<gmshfem::algebra::Vector<cmplx> > &x);
    }
  }
}

#endif
