// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_EIGENSOLVER_H_
#define _CLOSEDDM_EIGENSOLVER_H_

namespace closeddm{
  /// Solvers for eigenvalue problems
  namespace eigensolver{
  }
}

#endif
