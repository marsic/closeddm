// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEMHELPER_H_
#define _CLOSEDDM_PROBLEMHELPER_H_

#include "CloseDDM.h"
#include "Parameter.h"

namespace closeddm{
  namespace problem{
    /// Helper functions to create a Problem
    namespace helper{
      /// Reads the mesh from the given Onelab server `ol` (if available)
      /// or the given Parameter `param` (fallback)
      void mesh(const closeddm::input::Onelab &ol,
                const closeddm::input::Parameter &param);
      /// Generates the Domain map `domain` from the given Parameter `param`
      void domain(SMap<gmshfem::domain::Domain>& domain,
                  const closeddm::input::Parameter &param);
      /// Generates the Subdomain map `domain`, Interface map `interface` and
      /// neighbourhood map `neighbour` from the given Parameter `param`
      void domain(SMap<gmshddm::domain::Subdomain>& domain,
                  SMap<gmshddm::domain::Interface>& interface,
                  std::vector<std::vector<unsigned int> > &neighbour,
                  const closeddm::input::Parameter &param);
    }
  }
}

#endif
