// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "Source.h"
#include "SourceModal.h"
#include "SourceRectangle.h"
#include "SourceZero.h"

using namespace gmshfem::common;
using namespace gmshfem::domain;
using namespace closeddm;
using namespace closeddm::input;
using namespace closeddm::problem;

Source* Source::factory(const Parameter &param, const Domain &omega,
                        std::string force){
  std::string kind = force.empty()? param.getString("source") : force;

  if     (kind == "modal")     return new SourceModal(param, omega);
  else if(kind == "rectangle") return new SourceRectangle(param);
  else if(kind == "zero")      return new SourceZero();
  else
    throw Exception("closeddm::problem::Source::factory: "
                    "unknown kind '"+kind+"'");
}

Source::~Source(void){
};
