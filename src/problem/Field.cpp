// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/Formulation.h"
#include "Field.h"

using namespace closeddm::problem;
using namespace gmshddm::field;
using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::dofs;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::field;
using namespace gmshfem::problem;
using namespace std;

size_t closeddm::problem::Field::
hash::operator()(const KeyIdxPair &kip) const noexcept{
  SearchDof dof(kip.first.first, kip.first.second);
  return dof.hash();
}

bool closeddm::problem::Field::
equal_to::operator()(const KeyIdxPair &kipA,
                     const KeyIdxPair &kipB) const noexcept{
  return kipA.first.first  == kipB.first.first &&
         kipA.first.second == kipB.first.second;
}

closeddm::problem::Field::Key
closeddm::problem::Field::rootKey(const FieldInterface<cmplx> &f,
                                  const RawDof &dof){
  return make_pair(dof.numType - GMSHFEM_DOF_FIELD_OFFSET * f.tag(),
                   dof.entity);
}

closeddm::problem::Field::Key
closeddm::problem::Field::convertKey(const FieldInterface<cmplx> &fSrc,
                                     const RawDof &dSrc,
                                     const FieldInterface<cmplx> &fDest){
  Key key = rootKey(fSrc, dSrc);
  key.first = key.first + GMSHFEM_DOF_FIELD_OFFSET * fDest.tag();
  return key;
}

void closeddm::problem::Field::
associate(const FieldInterface<cmplx> &fullF,
          const vector<RawDof> &fullV,
          const vector<gmshfem::field::Field<cmplx, Form::Form0>*> &subF,
          const vector<vector<RawDof> > &subV,
          vector<pair<size_t, size_t> > &forward,
          vector<vector<size_t> > &backward){
  // Create sets form vectors //
  KeyIdxPairSet fullS = set(fullV);

  vector<KeyIdxPairSet> subS(subV.size());
  for(size_t j = 0; j < subV.size(); j++)
    subS[j] = set(subV[j]);

  // Init forward and backward //
  forward.resize(fullV.size());
  backward.resize(subV.size());
  for(size_t j = 0; j < subV.size(); j++)
    backward[j].resize(subV[j].size());

  // Loop across 'full' entries and link then with the 'sub' ones //
  // --> 'forward' match                                          //
  for(size_t i = 0; i < fullV.size(); i++){
    bool match = false;
    for(size_t j = 0; j < subV.size() && !match; j++){
      // Convert 'full' key into 'sub[j]' key and look for it in sub[j]
      Key key = convertKey(fullF, fullV[i], *subF[j]);
      KeyIdxPairSet::iterator found = subS[j].find(make_pair(key, 0));

      // If found, populate forward and mark sub[j] as matched
      if(found != subS[j].end()){
        forward[i] = make_pair(j, found->second);
        match = true;
      }
    }

    // If no match were found, throw exception
    if(!match)
      throw Exception("No match found for DoF (" +
                      to_string(fullV[i].numType) + ", " +
                      to_string(fullV[i].entity)  + ") " +
                      "of field " + to_string(fullF.tag()));
  }

  // Loop across 'sub' entries and link then with the 'full' ones //
  // --> 'backward' match                                         //
  for(size_t i = 0; i < subV.size(); i++){
    for(size_t j = 0; j < subV[i].size(); j++){
      // Convert 'sub' key into 'full' key and look for it in full
      Key key = convertKey(*subF[i], subV[i][j], fullF);
      KeyIdxPairSet::iterator found = fullS.find(make_pair(key, 0));

      // If found, populate backward; throw exception otherwise
      if(found != fullS.end())
        backward[i][j] = found->second;
      else
        throw Exception("No match found for DoF (" +
                        to_string(fullV[i].numType) + ", " +
                        to_string(fullV[i].entity)  + ") " +
                        "of field " + to_string(fullF.tag()));
    }
  }
}

void closeddm::problem::Field::
concatenate(const gmshddm::field::SubdomainField<cmplx, Form::Form0> &sub,
            gmshfem::field::Field<cmplx, Form::Form0> &full,
            bool init){
  // Temporary vector
  vector<gmshfem::field::Field<cmplx, Form::Form0>*> tmp(sub.size());
  for(size_t i = 0; i < sub.size(); i++)
    tmp[i] = const_cast<gmshfem::field::Field<cmplx, Form::Form0>*>(&sub(i));

  // Concatenate
  concatenate(tmp, full, init);
}

void closeddm::problem::Field::
concatenate(const vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub,
            gmshfem::field::Field<cmplx, Form::Form0> &full,
            bool init){
  // Sanity //
  if(sub.size() == 0)
    return;

  // Initialization of full //
  if(init){
    // Domain of definition
    Domain all;
    for(size_t i = 0; i < sub.size(); i++)
      all |= sub[i]->domain();

    // Recover function space and constraints
    FunctionSpace<double, Form::Form0> *H = sub[0]->getFunctionSpace();
    list<vector<Constraint<cmplx, Degree::Degree0> > > cnstrnt;
    for(size_t i = 0; i < sub.size(); i++)
      cnstrnt.push_back(sub[i]->getConstraints());

    // Create field and constraint it
    full = gmshfem::field::Field<cmplx, Form::Form0>("full", all,
                                                     H->type(), H->order());
    for(list<vector<Constraint<cmplx, Degree::Degree0> > >::iterator
          itL = cnstrnt.begin(); itL != cnstrnt.end(); itL++)
      for(vector<Constraint<cmplx, Degree::Degree0> >::iterator
            itV = itL->begin(); itV != itL->end(); itV++)
        full.addConstraint(itV->_domain, itV->_function);

    // Finalize by preprocessing a dummy problem
    Formulation<cmplx> dummy("concatenate_dummy");
    dummy.integral(dof(full), tf(full), full.domain(), "gauss1");
    dummy.pre();
  }

  // Concatenate unknown DoFs //
  concatenate(sub, full,
              &FieldInterface<cmplx>::getUnknownDofVector,
              &FieldInterface<cmplx>::getUnknownVector,
              &FieldInterface<cmplx>::setUnknownVector);

  // TODO: LinkedDofs? //
  for(size_t i = 0; i < sub.size(); i++)
    if(sub[i]->numberOfLinkedDofs() != 0)
      throw Exception("Cannot handle linked DoFs (yet)");
}

void closeddm::problem::Field::
split(const gmshfem::field::Field<cmplx, Form::Form0> &full,
      gmshddm::field::SubdomainField<cmplx, Form::Form0> &sub){
  // Temporary vector
  vector<gmshfem::field::Field<cmplx, Form::Form0>*> tmp(sub.size());
  for(size_t i = 0; i < sub.size(); i++)
    tmp[i] = const_cast<gmshfem::field::Field<cmplx, Form::Form0>*>(&sub(i));

  // Split
  split(full, tmp);
}

void closeddm::problem::Field::
split(const gmshfem::field::Field<cmplx, Form::Form0> &full,
      vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub){
  split(full, sub,
        &FieldInterface<cmplx>::getUnknownDofVector,
        &FieldInterface<cmplx>::getUnknownVector,
        &FieldInterface<cmplx>::setUnknownVector);
}

closeddm::problem::Field::KeyIdxPairSet
closeddm::problem::Field::set(const vector<RawDof> &dof){
  KeyIdxPairSet kips;
  for(size_t i = 0; i < dof.size(); i++)
    kips.insert(make_pair(make_pair(dof[i].numType, dof[i].entity), i));
  return kips;
}

void closeddm::problem::Field::
associate(const vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub,
          const gmshfem::field::Field<cmplx, Form::Form0> &full,
          void(FieldInterface<cmplx>::*dof)(vector<RawDof>&) const,
          vector<pair<size_t, size_t> > &forward,
          vector<vector<size_t> > &backward){
  // Get DoFs of subfield
  vector<vector<RawDof> > subDof(sub.size());
  for(size_t i = 0; i < sub.size(); i++)
    (sub[i]->*dof)(subDof[i]);

  // Get DoFs of concatenated field
  vector<RawDof> fullDof;
  (full.*dof)(fullDof);

  // Associate DoFs in full with those of sub
  associate(full, fullDof, sub, subDof, forward, backward);
}

void closeddm::problem::Field::
value(const vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub,
      const gmshfem::field::Field<cmplx, Form::Form0> &full,
      void(FieldInterface<cmplx>::*val)(algebra::Vector<cmplx>&,
                                        const RawOrder) const,
      vector<algebra::Vector<cmplx> > &subV,
      algebra::Vector<cmplx> &fullV){
  // Get values of subfield
  for(size_t i = 0; i < sub.size(); i++)
    (sub[i]->*val)(subV[i], RawOrder::Hash);

  // Get values of concatenated field
  (full.*val)(fullV, RawOrder::Hash);
}

void closeddm::problem::Field::
concatenate(const vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub,
            gmshfem::field::Field<cmplx, Form::Form0> &full,
            void(FieldInterface<cmplx>::*dof)(vector<RawDof>&) const,
            void(FieldInterface<cmplx>::*val)(algebra::Vector<cmplx>&,
                                              const RawOrder) const,
            void(FieldInterface<cmplx>::*set)(const algebra::Vector<cmplx>&,
                                              const RawOrder)){
  // Associate DoFs in full with those of sub
  vector<pair<size_t, size_t> > forward;
  vector<vector<size_t> > backward;
  associate(sub, full, dof, forward, backward);

  // Get values of subfield and concatenated field
  vector<algebra::Vector<cmplx> > subVal(sub.size());
  algebra::Vector<cmplx> fullVal;
  value(sub, full, val, subVal, fullVal);

  // Copy values from sub to full
  for(size_t i = 0; i < fullVal.size(); i++)
    fullVal[i] = subVal[forward[i].first][forward[i].second];

  // Update full
  (full.*set)(fullVal, RawOrder::Hash);
}

void closeddm::problem::Field::
split(const gmshfem::field::Field<cmplx, Form::Form0> &full,
      vector<gmshfem::field::Field<cmplx, Form::Form0>*> &sub,
      void(FieldInterface<cmplx>::*dof)(vector<RawDof>&) const,
      void(FieldInterface<cmplx>::*val)(algebra::Vector<cmplx>&,
                                        const RawOrder) const,
      void(FieldInterface<cmplx>::*set)(const algebra::Vector<cmplx>&,
                                        const RawOrder)){
  // Associate DoFs in full with those of sub
  vector<pair<size_t, size_t> > forward;
  vector<vector<size_t> > backward;
  associate(sub, full, dof, forward, backward);

  // Get values of subfield and concatenated field
  vector<algebra::Vector<cmplx> > subVal(sub.size());
  algebra::Vector<cmplx> fullVal;
  value(sub, full, val, subVal, fullVal);

  // Copy values from full to sub
  for(size_t i = 0; i < subVal.size(); i++)
    for(size_t j = 0; j < subVal[i].size(); j++)
      subVal[i][j] = fullVal[backward[i][j]];

  // Update sub
  for(size_t i = 0; i < subVal.size(); i++)
    (sub[i]->*set)(subVal[i], RawOrder::Hash);
}
