// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmsh.h"
#include "gmshfem/Exception.h"
#include "ProblemHelper.h"

using namespace closeddm;
using namespace closeddm::input;
using namespace gmshddm::domain;
using namespace gmshfem::common;
using namespace gmshfem::domain;

void closeddm::problem::helper::mesh(const Onelab &ol,
                                     const Parameter &param){
  std::string mesh = ol.isConnected()?
                     ol.model()+".msh" : param.getString("msh");
  if(!mesh.empty())
    gmsh::open(mesh);
  else
    throw Exception("No mesh given");
}

void closeddm::problem::helper::domain(SMap<Domain>& domain,
                                       const Parameter &param){
  // Parameters
  unsigned int nDom = param.getNumber("nDom");
  bool       cavity = param.getFlag("cavity");
  bool    eigenmode = param.getFlag("eigenmode");

  // Dimensionality
  int dim = gmsh::model::getDimension();

  // Init with empty domains
  domain["omega"]  = Domain();
  domain["wall"]   = Domain();
  domain["gammaD"] = Domain();
  domain["gammaI"] = Domain();

  // Domains
  for(unsigned int i = 0; i < nDom; i++)
    domain["omega"] |= Domain(dim, i+1);

  if(eigenmode)
    domain["wall"]   |= Domain(dim-1, nDom+1);
  else
    domain["gammaD"] |= Domain(dim-1, nDom+1);

  for(unsigned int i = 0; i < nDom; i++)
    domain["wall"] |= Domain(dim-1, nDom + (nDom+1) + (i+1));

  if(cavity)
    domain["wall"]   |= Domain(dim-1, nDom+nDom+1);
  else
    domain["gammaI"] |= Domain(dim-1, nDom+nDom+1);
}

void closeddm::problem::helper::domain(SMap<Subdomain>& domain,
                                       SMap<Interface>& interface,
                                       std::vector<
                                         std::vector<unsigned int> > &neighbour,
                                       const Parameter &param){
  // Parameters
  unsigned int nDom = param.getNumber("nDom");
  bool       cavity = param.getFlag("cavity");
  bool    eigenmode = param.getFlag("eigenmode");

  // Dimensionality
  int dim = gmsh::model::getDimension();

  // Subdomains (init)
  domain.emplace("omega",  nDom);
  domain.emplace("gammaD", nDom);
  domain.emplace("wall",   nDom);
  domain.emplace("gammaI", nDom);

  // Subdomains (populate)
  for(unsigned int i = 0; i < nDom; i++)
    domain.at("omega")(i) = Domain(dim, i+1);

  if(eigenmode)
    domain.at("wall")(0)   = Domain(dim-1, nDom+1);
  else
    domain.at("gammaD")(0) = Domain(dim-1, nDom+1);

  for(unsigned int i = 0; i < nDom; i++)
    domain.at("wall")(i) |= Domain(dim-1, nDom + (nDom+1) + (i+1));

  if(cavity)
    domain.at("wall")(nDom-1)  |= Domain(dim-1, nDom+nDom+1);
  else
    domain.at("gammaI")(nDom-1) = Domain(dim-1, nDom+nDom+1);

  // Interfaces (init)
  interface.emplace("sigma",  nDom);
  interface.emplace("dSigma", nDom);

  // Interfaces (populate)
  for(unsigned int i = 0; i < nDom; i++){
    if(i != 0)
      interface.at("sigma")(i, i-1) = Domain(dim-1, nDom + (i+1));
    if(i != nDom-1)
      interface.at("sigma")(i, i+1) = Domain(dim-1, nDom + (i+1) + 1);
  }

  for(unsigned int i = 0; i < nDom; i++){
    if(i != 0)
      interface.at("dSigma")(i, i-1) = Domain(dim-2, 2*nDom+(nDom+1)+(i+1));
    if(i != nDom-1)
      interface.at("dSigma")(i, i+1) = Domain(dim-2, 2*nDom+(nDom+1)+(i+1) + 1);
  }

  // Neighbourhood
  neighbour.resize(nDom);
  for(unsigned int i = 0; i < nDom; i++){
    if(i != 0)
      neighbour[i].push_back(i - 1);
    if(i != nDom-1)
      neighbour[i].push_back(i + 1);
  }
}
