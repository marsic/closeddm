// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmsh.h"
#include "Constant.h"
#include "SourceRectangle.h"

using namespace gmshfem::function;
using namespace closeddm;
using namespace closeddm::constant;
using namespace closeddm::input;

closeddm::problem::SourceRectangle::SourceRectangle(const Parameter &param){
  allmodes = param.getFlag("allmodes");
  ny = param.getNumber("ny");
  nz = param.getNumber("nz");
  ly = param.getNumber("lsy");
  lz = param.getNumber("lsz");
  dim = gmsh::model::getDimension();
};

closeddm::problem::SourceRectangle::~SourceRectangle(void){
};

SFunction closeddm::problem::SourceRectangle::get(void) const{
  // Generic function pointers //
  SFunction(*fY)(int,double,SFunction) = (dim > 1)? fSin : fOne;
  SFunction(*fZ)(int,double,SFunction) = (dim > 2)? fSin : fOne;

  // Number of terms per dimension //
  int N = (allmodes && dim > 1)? ny : 1;
  int M = (allmodes && dim > 2)? nz : 1;

  // Source //
  SFunction fSrc = 0;
  for(int n = 0; n < N; n++)
    for(int m = 0; m < M; m++)
      fSrc = fSrc + fY(ny-n, ly, y<cmplx>()) * fZ(nz-m, lz, z<cmplx>());

  // Done //
  return fSrc;
};

std::string closeddm::problem::SourceRectangle::info(void) const{
  // Mode string
  std::string mode;
  switch(dim){
  case 1:  mode = "1"; break;
  case 2:  mode = std::to_string(ny); break;
  case 3:  mode = "("+std::to_string(ny)+", "+std::to_string(nz)+")"; break;
  default: mode = "?error?"; break;
  }

  // Done
  return std::string((allmodes)? "the " : "mode ")
    + mode
    + std::string((allmodes)? " first modes" : "");
}

SFunction closeddm::problem::SourceRectangle::fSin(int n, double l,
                                                   SFunction xyz){
  return sin<cmplx>((n*Pi/l) * xyz);
}

SFunction closeddm::problem::SourceRectangle::fOne(int n, double l,
                                                   SFunction xyz){
  return 1.;
}
