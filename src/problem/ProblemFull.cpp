// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "ProblemFull.h"

using namespace closeddm;
using namespace closeddm::problem;

ProblemFull::~ProblemFull(void){
}

size_t ProblemFull::size(void) const{
  return 1;
}

Field0& ProblemFull::field(const std::string &name,
                           size_t i, size_t j){
  SMap<Field0>::iterator it = ffield.find(name);
  if(it != ffield.end())
    return it->second;
  else
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
}

const Field0& ProblemFull::field(const std::string &name,
                                 size_t i, size_t j) const{
  SMap<Field0>::const_iterator it = ffield.find(name);
  if(it != ffield.end())
    return it->second;
  else
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
}

std::vector<Field0*> ProblemFull::field(const std::string &name){
  SMap<Field0>::iterator it = ffield.find(name);
  if(it != ffield.end())
    return {&(it->second)};
  else
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
}

gmshfem::problem::Formulation<cmplx>& ProblemFull::formulation(size_t i){
  return *form;
}

gmshfem::algebra::MatrixCRS<cmplx> ProblemFull::matrix(void) const{
  gmshfem::algebra::MatrixCRS<cmplx> A;
  form->getLHS(A);
  return A;
}

void ProblemFull::pre(void){
  form->pre();
}

void ProblemFull::assemble(void){
  form->assemble();
}

void ProblemFull::solve(std::string solver, double tol, int maxIt){
  form->solve();
}

size_t ProblemFull::iteration(void) const{
  return 0;
}
