// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_SOURCE_RECTANGLE_H_
#define _CLOSEDDM_SOURCE_RECTANGLE_H_

#include "Source.h"

namespace closeddm{
  namespace problem{
    /// Source obtained from the port modes of a rectangular waveguide
    class SourceRectangle final: public Source{
    private:
      bool allmodes;
      int    ny;
      int    nz;
      double ly;
      double lz;
      int   dim;

    public:
      /// Constructs a new SourceRectangle defined with the Parameter `param`,
      /// whose relevant values are:
      ///  - "lsy", the 'y' dimension of the port
      ///  - "lsz", the 'z' dimension of the port
      ///  - "ny", the mode number along 'y'
      ///  - "nz", the mode number along 'z'
      ///  - "allmodes", if this flag is `true`, a multi-modal Source is imposed
      SourceRectangle(const closeddm::input::Parameter &param);
      ~SourceRectangle(void);

      virtual SFunction    get(void) const override final;
      virtual std::string info(void) const override final;

    private:
      static SFunction fSin(int n, double l, SFunction xyz);
      static SFunction fOne(int n, double l, SFunction xyz);
    };
  }
}

#endif
