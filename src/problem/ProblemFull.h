// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEM_FULL_H_
#define _CLOSEDDM_PROBLEM_FULL_H_

#include "gmshfem/Formulation.h"
#include "Problem.h"

namespace closeddm{
  namespace problem{
    /// Base class for Problem solved without the optimized Schwarz method (OSM)
    class ProblemFull: public Problem{
    protected:
      using Formulation = gmshfem::problem::Formulation<cmplx>;

    protected:
      gmshfem::problem::Formulation<cmplx> *form;
      SMap<Field0> ffield;

    public:
      /// Destructor
      virtual ~ProblemFull(void) = 0;

      virtual size_t size(void) const override final;

      virtual
        Field0& field(const std::string &name,
                      size_t i, size_t j=-1)                  override final;
      virtual
        const Field0& field(const std::string &name,
                            size_t i, size_t j=-1) const      override final;
      virtual
        std::vector<Field0*> field(const std::string &name)   override final;
      virtual
        gmshfem::problem::Formulation<cmplx>& formulation(size_t i=-1)
                                                              override final;
      virtual
        gmshfem::algebra::MatrixCRS<cmplx> matrix(void) const override final;

      virtual void pre(void)                              override final;
      virtual void assemble(void)                         override final;
      virtual void solve(std::string solver="default",
                         double tol=1e-6, int maxIt=1000) override final;
      virtual size_t iteration(void) const                override final;
    };
  }
}

#endif
