// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEM_OSM_H_
#define _CLOSEDDM_PROBLEM_OSM_H_

#include "gmshddm/Formulation.h"
#include "Problem.h"
#include "TC.h"

namespace closeddm{
  namespace problem{
    /// Base class for Problem solved with the optimized Schwarz method (OSM)
    class ProblemOSM: public Problem{
    protected:
      gmshddm::problem::Formulation<cmplx> *form;
      SMap<SField0> sffield;
      SMap<IField0> iffield;
      std::vector<tc::TC*> ttc;

    public:
      virtual ~ProblemOSM(void) = 0;

      virtual size_t size(void) const override final;

      /// Returns the gmshddm::problem::Formulation,
      /// defining this optimized Schwarz Problem
      gmshddm::problem::Formulation<cmplx> &osFormulation(void);

      virtual
        Field0& field(const std::string &name,
                      size_t i, size_t j=-1)                  override final;
      virtual
        const Field0& field(const std::string &name,
                            size_t i, size_t j=-1) const      override final;
      virtual
        std::vector<Field0*> field(const std::string &name)   override final;
      virtual
        gmshfem::problem::Formulation<cmplx>& formulation(size_t i)
                                                              override final;
      virtual
        gmshfem::algebra::MatrixCRS<cmplx> matrix(void) const override final;

      virtual void pre(void)                              override final;
      virtual void assemble(void)                         override final;
      virtual void solve(std::string solver="default",
                         double tol=1e-6, int maxIt=1000) override final;
      virtual size_t iteration(void) const                override final;
    };
  }
}

#endif
