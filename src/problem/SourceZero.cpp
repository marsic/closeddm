// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "SourceZero.h"

using namespace closeddm;

closeddm::problem::SourceZero::SourceZero(void){
};

closeddm::problem::SourceZero::~SourceZero(void){
};

SFunction closeddm::problem::SourceZero::get(void) const{
  return 0;
}

std::string closeddm::problem::SourceZero::info(void) const{
  return "no source";
}
