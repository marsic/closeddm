// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_FIELD_H_
#define _CLOSEDDM_FIELD_H_

#include <unordered_set>
#include "gmshddm/SubdomainField.h"
#include "gmshfem/Dof.h"
#include "gmshfem/FieldInterface.h"
#include "CloseDDM.h"

namespace closeddm{
  namespace problem{
    /// Helper class to handle field and subfields
    class Field{
    private:
      typedef std::pair<unsigned long long, unsigned long long> Key;
      typedef std::pair<Key, size_t> KeyIdxPair;
      struct hash{
        size_t operator()(const KeyIdxPair &kip)  const noexcept;
      };
      struct equal_to{
        bool   operator()(const KeyIdxPair &kipA,
                          const KeyIdxPair &kipB) const noexcept;
      };
      typedef std::unordered_set<std::pair<std::pair<unsigned long long,
                                                     unsigned long long>,
                                           size_t>,
                                 hash, equal_to> KeyIdxPairSet;

    public:
      /// Given a field `f` and a DoF `dof` of that field,
      /// returns the root key of `dof`, that is the key without its field tag
      static Key rootKey(const gmshfem::field::FieldInterface<cmplx> &f,
                         const gmshfem::dofs::RawDof &dof);

      /// Given a field `fSrc` and a DoF `dSrc` of that field,
      /// and given another field `fDest`,
      /// converts the key of `dSrc` into the same key within `fDest`,
      /// that is with the field tag of `fDest` instead of `fSrc`
      static Key convertKey(const gmshfem::field::FieldInterface<cmplx> &fSrc,
                            const gmshfem::dofs::RawDof &dSrc,
                            const gmshfem::field::FieldInterface<cmplx> &fDest);

      /// Given
      /// i) a field `fullF` and its DoFs `fullD` and
      /// ii) a subfield `subF` (as a vector of fields) and its DoFs `subD`,
      /// populate the vectors `forward` and `backward`
      /// such that the following is true:
      /// * `fullD[i] == subD[forward[i].first][forward[i].second]`
      /// * `subD[j][i] == fullD[backward[j][i]]`
      static void associate(const gmshfem::field::FieldInterface<cmplx> &fullF,
                            const std::vector<gmshfem::dofs::RawDof> &fullD,
                            const std::vector<gmshfem::field::Field<
                              cmplx, gmshfem::field::Form::Form0>*> &subF,
                            const std::vector<
                              std::vector<gmshfem::dofs::RawDof> > &subD,
                            std::vector<std::pair<size_t, size_t> > &forward,
                            std::vector<std::vector<size_t> > &backward);

      /// Given a subfield `sub`, populates a field `full`
      /// such that `full` is the concatenation of `sub`.
      /// If `init` is `true`, `full` is initialized (i.e. setting up the name,
      /// domain of definition, type and order) by this function.
      /// Otherwise, `full` is assumed to have been previously initialized.
      static void concatenate(const gmshddm::field::SubdomainField<
                                cmplx, gmshfem::field::Form::Form0> &sub,
                              gmshfem::field::Field<
                                cmplx, gmshfem::field::Form::Form0> &full,
                              bool init=true);
      /// Same as above, but the subfield is represented with a vector of fields
      static void concatenate(const std::vector<gmshfem::field::Field<
                                cmplx, gmshfem::field::Form::Form0>*> &sub,
                              gmshfem::field::Field<
                                cmplx, gmshfem::field::Form::Form0> &full,
                              bool init=true);

      /// Given a field `full`, populates a subfield `sub`
      /// such that `full` can be recovered by concatenating `sub`.
      /// The splitting pattern is deduced from `sub`,
      /// which thus needs to be initialized before calling this function.
      static void split(const gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0> &full,
                        gmshddm::field::SubdomainField<
                          cmplx, gmshfem::field::Form::Form0> &sub);
      /// Same as above, but the subfield is represented with a vector of fields
      static void split(const gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0> &full,
                        std::vector<gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0>*> &sub);

    private:
      static KeyIdxPairSet set(const std::vector<gmshfem::dofs::RawDof> &dof);

      static void associate(const std::vector<gmshfem::field::Field<
                              cmplx, gmshfem::field::Form::Form0>*> &sub,
                            const gmshfem::field::Field<
                              cmplx, gmshfem::field::Form::Form0> &full,
                            void(gmshfem::field::FieldInterface<cmplx>::*dof)
                                (std::vector<gmshfem::dofs::RawDof>&) const,
                            std::vector<std::pair<size_t, size_t> > &forward,
                            std::vector<std::vector<size_t> > &backward);

      static void value(const std::vector<gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0>*> &sub,
                        const gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0> &full,
                        void(gmshfem::field::FieldInterface<cmplx>::*val)
                            (gmshfem::algebra::Vector<cmplx>&,
                             const gmshfem::dofs::RawOrder) const,
                        std::vector<gmshfem::algebra::Vector<cmplx> > &subV,
                        gmshfem::algebra::Vector<cmplx> &fullV);

      static void concatenate(const std::vector<gmshfem::field::Field<
                                cmplx, gmshfem::field::Form::Form0>*> &sub,
                              gmshfem::field::Field<
                                cmplx, gmshfem::field::Form::Form0> &full,
                              void(gmshfem::field::FieldInterface<cmplx>::*dof)
                                  (std::vector<gmshfem::dofs::RawDof>&) const,
                              void(gmshfem::field::FieldInterface<cmplx>::*val)
                                  (gmshfem::algebra::Vector<cmplx>&,
                                   const gmshfem::dofs::RawOrder) const,
                              void(gmshfem::field::FieldInterface<cmplx>::*set)
                                  (const gmshfem::algebra::Vector<cmplx>&,
                                   const gmshfem::dofs::RawOrder));

      static void split(const gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0> &full,
                        std::vector<gmshfem::field::Field<
                          cmplx, gmshfem::field::Form::Form0>*> &sub,
                        void(gmshfem::field::FieldInterface<cmplx>::*dof)
                            (std::vector<gmshfem::dofs::RawDof>&) const,
                        void(gmshfem::field::FieldInterface<cmplx>::*val)
                            (gmshfem::algebra::Vector<cmplx>&,
                             const gmshfem::dofs::RawOrder) const,
                        void(gmshfem::field::FieldInterface<cmplx>::*set)
                            (const gmshfem::algebra::Vector<cmplx>&,
                             const gmshfem::dofs::RawOrder));
    };
  }
}

#endif
