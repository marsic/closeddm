// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_SOURCE_MODAL_H_
#define _CLOSEDDM_SOURCE_MODAL_H_

#include "Source.h"

namespace closeddm{
  namespace problem{
    /// Source obtained from a modal (eigenvalue) analysis
    class SourceModal final: public Source{
    private:
      const gmshfem::domain::Domain *omega;
      Field0 *fSrc;
      double target;
      int gauss;

    public:
      /// Constructs a new SourceModal defined on the given Domain `omega`
      /// with the Parameter `param`; the relevant ones are:
      ///  - "order", the FE order of the modal problem
      ///  - "ny", the target eigenvalue of the mode to select as Source
      SourceModal(const closeddm::input::Parameter &param,
                  const gmshfem::domain::Domain &omega);
      ~SourceModal(void);

      virtual SFunction    get(void) const override final;
      virtual std::string info(void) const override final;
    };
  }
}

#endif
