// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "ABC.h"
#include "Constant.h"
#include "Field.h"
#include "ProblemOSMHelmholtz.h"
#include "TCFactory.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::problem;
using namespace closeddm::utils;
using namespace gmshddm::domain;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::field;
using namespace std;

ProblemOSMHelmholtz::ProblemOSMHelmholtz(Parameter &param,
                                         const SMap<Subdomain> &domain,
                                         const SMap<Interface> &interface,
                                         const vector<
                                                 vector<
                                                   unsigned int> > &neighbour){
  // Parameters
  this->param = &param;

  // Domains
  this->domain    = &domain;
  this->interface = &interface;
  this->neighbour = &neighbour;

  // Weak formulation
  this->form = NULL;
  this->src  = NULL;
  refresh();
}

ProblemOSMHelmholtz::~ProblemOSMHelmholtz(void){
  clear();
}

void ProblemOSMHelmholtz::refresh(void){
  // Clear //
  ///////////
  clear();

  // Domains //
  /////////////
  // Subdomains
  const Subdomain omega  = domain->at("omega");
  const Subdomain wall   = domain->at("wall");
  const Subdomain gammaD = domain->at("gammaD");
  const Subdomain gammaI = domain->at("gammaI");

  // Interfaces
  const Interface sigma  = interface->at("sigma");
  const Interface dSigma = interface->at("dSigma");

  // Neighbour (proxy)
  const std::vector<std::vector<unsigned int> > &neighbour = *this->neighbour;

  // Source field and 0th-order ABC //
  ////////////////////////////////////
  src = Source::factory(*param, gammaD(0),
                        param->getFlag("eigenmode")? "zero" : "");
  SFunction fSrc = src->get();
  cmplx     kInf = abc::zero(*param);

  // Fields //
  ////////////
  int O = param->getNumber("order");
  std::string family = param->getString("family");
  FunctionSpaceTypeForm0 HGrad =
    utils::functionspace::family<FunctionSpaceTypeForm0>(family);

  sffield["u"]      = SField0("u",  omega|gammaD|gammaI|wall|sigma, HGrad, O);
  sffield["lambda"] = SField0("lambda",   gammaD,                   HGrad, O);
  iffield["g"]      = IField0("g",                    dSigma|sigma, HGrad, O);
  sffield["us"]     = SField0("us", omega|gammaD|gammaI|wall|sigma, HGrad, O);

  // Proxies (references)
  SField0 &u      = sffield["u"];
  SField0 &lambda = sffield["lambda"];
  IField0 &g      = iffield["g"];
  SField0 &us     = sffield["us"];

  // Constraints
  for(size_t i = 0; i < neighbour.size(); i++)
    u(i).addConstraint(wall(i), 0);

  for(size_t i = 0; i < neighbour.size(); i++)
    for(size_t jj = 0; jj < neighbour[i].size(); jj++)
      g(i, neighbour[i][jj]).addConstraint(dSigma(i, neighbour[i][jj]), 0);

  // Constraints on us
  for(size_t i = 0; i < neighbour.size(); i++)
    us(i).addConstraint(wall(i), 0);

  // Init 'us' with dummy formulation
  std::vector<gmshfem::problem::Formulation<cmplx> > dummy;
  dummy.reserve(neighbour.size());
  for(size_t i = 0; i < neighbour.size(); i++)
    dummy.emplace_back(
      gmshfem::problem::Formulation<cmplx>("dummy_" + std::to_string(i)));
  for(size_t i = 0; i < neighbour.size(); i++){
    dummy[i].integral(dof(us(i)), tf(us(i)), omega(i), "gauss1");
    dummy[i].pre();
  }

  // Formulation //
  /////////////////
  // Wavenumber
  cmplx k = complexify(param->getNumber("k"), param->getNumber("kd"));

  // Info string
  std::string info = "Helmholtz (OSM)";
  info += " - FE"   +   std::to_string(O);
  info += " - k = " + utils::to_string(k);

  // Init
  this->form = new gmshddm::problem::Formulation<cmplx>(info, neighbour);
  string quad = quadrature();
  gmshddm::problem::Formulation<cmplx> &form = *this->form; // Proxy (reference)

  // OSM sources
  SFunction fSrcDD = form.physicalSource(fSrc);
  std::vector<SFunction> usDD(neighbour.size());
  for(size_t i = 0; i < neighbour.size(); i++)
    usDD[i] = form.physicalSource(us(i));
  form.addInterfaceField(g);

  // Transmition condition(s)
  std::list<tc::TC*> tmp = tc::factory(*param, neighbour,sigma, dSigma);
  ttc.insert(ttc.begin(), tmp.begin(), tmp.end());
  tmp.clear();

  // Weak formulation
  for(size_t i = 0; i < neighbour.size(); i++){
    // Helmholtz
    form(i).integral(     d(dof(u(i))), d(tf(u(i))), omega(i), quad);
    form(i).integral(-k*k * dof(u(i)),    tf(u(i)),  omega(i), quad);
    form(i).integral(       -usDD[i],     tf(u(i)),  omega(i), quad);

    // Absorbing boundary condition (Sommerfeld)
    form(i).integral(-J*kInf * dof(u(i)), tf(u(i)), gammaI(i), quad);

    // Physical source (via a Lagrange multiplier)
    form(i).integral(dof(lambda(i)), tf(u(i)), gammaD(i), quad);
    form(i).integral(dof(u(i)), tf(lambda(i)), gammaD(i), quad);
    form(i).integral(-fSrcDD,   tf(lambda(i)), gammaD(i), quad);

    // Transmission condition
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      SFunction gInJI = form.artificialSource(g(j, i));

      form(i).integral(-gInJI, tf(u(i)), sigma(i, j), quad);
      for(auto &tc: ttc)
        tc->interface(form(i), u(i), sigma(i, j), i, j, quad);
    }

    // Update law
    for(size_t jj = 0; jj < neighbour[i].size(); jj++){
      const unsigned int j = neighbour[i][jj];
      SFunction gInJI = form.artificialSource(g(j,i));

      form(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), quad);
      form(i, j).integral(gInJI,        tf(g(i, j)), sigma(i, j), quad);
      for(auto &tc: ttc)
        tc->update(form(i, j), u(i), g(i, j), sigma(i, j), i, j, quad);
    }
  }
}

void ProblemOSMHelmholtz::clear(void){
  if(form) delete form;
  sffield.clear();
  iffield.clear();
  if(src) delete src;

  for(auto &tc: ttc)
    delete tc;
  ttc.clear();
}

double ProblemOSMHelmholtz::residual(void) const{
  // Subdomains, quadrature rule and wavenumber
  const Subdomain &omega = domain->at("omega");
  const string quad = quadrature();
  const cmplx k = complexify(param->getNumber("k"), param->getNumber("kd"));

  // Fields: residual and wave (and its concatenated variant)
  const SField0 &u = sffield.at("u"); // Wave
  Field0 uCat;                        // Concatenated wave
  Field::concatenate(u, uCat);        // ...
  Field0 r(uCat);                     // Residual

  // Concatenated computational domain
  Domain omegaCat = omega.getUnion();

  // Formulation for residual
  gmshfem::problem::Formulation<cmplx> res("residual");
  res.integral(   dof(r),      tf(r),  omegaCat, quad);
  res.integral(    -d(uCat), d(tf(r)), omegaCat, quad);
  res.integral(+k*k * uCat,    tf(r),  omegaCat, quad);

  // Assemble problem (we're just interested in the RHS)
  res.pre();
  res.assemble();

  // Solve and save residual field (optional)
  // res.solve();
  // gmshfem::post::save(r, omegaCat, "residual");

  // Return norm of RHS (estimation of 'real' residual)
  gmshfem::algebra::Vector<cmplx> b;
  res.getRHS(b);
  return b.norm();
}

string ProblemOSMHelmholtz::info(const std::string &kind, int i) const{
  if     (kind == "solver")       return "Optimized Schwarz Problem";
  else if(kind == "domain")       return " - " + infoDomain();
  else if(kind == "source")       return " - " + infoSource();
  else if(kind == "tc" && i == 0) return " - TC(s): * " + ttc[i]->info();
  else if(kind == "tc" && i != 0) return "          * " + ttc[i]->info();
  else                            return infoUnknwn();
}

int ProblemOSMHelmholtz::nInfo(const std::string &kind) const{
  if(kind == "tc") return ttc.size();
  else             return 0;
}
