// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEM_H_
#define _CLOSEDDM_PROBLEM_H_

#include "gmshddm/Formulation.h"

#include "CloseDDM.h"
#include "Parameter.h"
#include "Source.h"
#include "TC.h"

namespace closeddm{
  /// Finite element problem
  namespace problem{
    /// Base class for finite element (FE) problem

    /// A problem can be either a ProblemFull (no optimized Schwarz (OS) solver)
    /// or a ProblemOSM (with OS solver).
    /// In the latter case, the underlying gmshfem::problem::Formulation
    /// and associated discrete fields are tagged
    /// with their respective subdomain (one tag) or interface (two tags).
    class Problem{
    protected:
      using Parameter = closeddm::input::Parameter;

    protected:
      Parameter *param;
      const Source *src;

    public:
      /// Destructor
      virtual ~Problem(void) = 0;

      /// Returns the number of gmshfem::problem::Formulation
      /// defining this Problem
      virtual size_t size(void) const = 0;
      /// Returns the Parameter that defined this Problem
      Parameter& parameter(void);
      /// Refresh this Problem (account for a possibly modified Parameter)
      virtual void refresh(void) = 0;

      /// Returns the field `name`, tagged with `i` and `j`,
      /// defining this Problem
      virtual Field0& field(const std::string &name,
                            size_t i, size_t j) = 0;
      /// Same as above, but const
      virtual const Field0& field(const std::string &name,
                                  size_t i, size_t j) const = 0;
      /// Returns the all fields `name` defining this Problem
      virtual std::vector<Field0*> field(const std::string &name) = 0;
      /// Returns the gmshfem::problem::Formulation, tagged with `i`,
      /// defining this Problem
      virtual gmshfem::problem::Formulation<cmplx>& formulation(size_t i) = 0;
      /// Returns a copy of the matrix of the system that we solve for
      virtual gmshfem::algebra::MatrixCRS<cmplx> matrix(void) const = 0;

      /// Pre-assembles the discrete problem
      virtual void pre(void) = 0;
      /// Assembles the discrete problem
      virtual void assemble(void) = 0;
      /// Solves the discrete problem with the given solver `solver`, tolerance
      /// `tol` and maximum number of iteration `maxIt` (when applicable)
      /// @see GmshFEM and GmshDDM for more details about the solver
      virtual void solve(std::string solver="default",
                         double tol=1e-6, int maxIt=1000) = 0;
      /// Computes the residual associated with this Problem
      virtual double residual(void) const = 0;
      /// Returns the number of iterations that were needed for solving
      /// @note Returns `0` if this method does not make sense
      virtual size_t iteration(void) const = 0;

      /// Returns a string with some information (of nature `kind`)
      /// regarding this Problem;
      /// depending on `kind`, some extra index `i` may be required
      virtual std::string info(const std::string &kind, int i=0) const = 0;
      /// Returns the number of indices (in the range [0, `nInfo(...)`[)
      /// associated with an information of kind `kind`
      virtual int nInfo(const std::string &kind) const = 0;

    protected:
      virtual void clear(void) = 0;

      std::string quadrature(void) const;
      std::string infoDomain(void) const;
      std::string infoSource(void) const;
      std::string infoUnknwn(void) const;
    };
  }
}

#endif
