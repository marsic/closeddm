// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "Problem.h"

using namespace closeddm::problem;

Problem::~Problem(void){
}

closeddm::input::Parameter& Problem::parameter(void){
  return *param;
}

std::string Problem::quadrature(void) const{
  return "Gauss" + std::to_string((int)(param->getNumber("gauss")));
}

std::string Problem::infoDomain(void) const{
  return std::string(param->getFlag("cavity")? "Closed " : "Open ") + "problem";
}

std::string Problem::infoSource(void) const{
  return "Source: " + src->info();
}

std::string Problem::infoUnknwn(void) const{
  return "unknown kind :(";
}
