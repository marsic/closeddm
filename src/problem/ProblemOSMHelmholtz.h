// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEM_OSM_HELMHOLTZ_H_
#define _CLOSEDDM_PROBLEM_OSM_HELMHOLTZ_H_

#include "ProblemOSM.h"

namespace closeddm{
  namespace problem{
    /// A ProblemOSM for the Helmholtz equation

    /// This Problem defines the following subfields:
    ///  - u, the wave field defining the Helmholtz problem
    ///  - lambda, a Lagrange multiplier for (weak) Dirichlet conditions
    ///
    /// This Problem defines the following interface fields as well:
    ///  - g, the artificial source of the OSM
    class ProblemOSMHelmholtz final: public ProblemOSM{
    private:
      const SMap<gmshddm::domain::Subdomain>        *domain;
      const SMap<gmshddm::domain::Interface>        *interface;
      const std::vector<std::vector<unsigned int> > *neighbour;

    public:
      /// Constructs a new ProblemOSMHelmholtz defined on the Subomain `domain`
      /// with the Parameter `param`. The subdomains give rise to
      /// the interfaces `interface` with a neighbourhood map `neighbour`.
      ///
      /// The Subomain are composed of (per subdomain):
      ///  - "omega", the computational domain
      ///  - "wall", the walls (i.e. u = 0)
      ///  - "gammaD", the boundary (excluding the artificial ones)
      ///     along which the Source is imposed
      ///  - "gammaI", the boundary (excluding the artificial ones)
      ///    along which the ABC is imposed
      ///
      /// The Interface are composed of (per subdomain):
      ///  - "sigma", the artificial boundary
      ///  - "dSigma", the boundary of "sigma"
      ///
      /// The relevant parameters are:
      ///  - "k", the real part of the wavenumber of the Helmholtz problem
      ///  - "kd", the imaginary part of the wavenumber of the Helmholtz problem
      ///  - "order", the FE order
      ///  - "eigenmode", flag for eigenvalue problem
      ProblemOSMHelmholtz(Parameter &param,
                          const SMap<gmshddm::domain::Subdomain> &domain,
                          const SMap<gmshddm::domain::Interface> &interface,
                          const std::vector<
                                  std::vector<unsigned int> > &neighbour);
      ~ProblemOSMHelmholtz(void);

      virtual void refresh(void) override final;
      virtual double residual(void) const override final;
      virtual std::string info(const std::string &kind, int i=0) const
                                                                 override final;
      virtual int nInfo(const std::string &kind) const override final;

    protected:
      virtual void clear(void) override final;
    };
  }
}

#endif
