// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmshfem/Formulation.h"
#include "Message.h"
#include "Utils.h"
#include "SourceModal.h"

using namespace gmshfem::algebra;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::field;
using namespace gmshfem::problem;
using namespace closeddm;
using namespace closeddm::input;

closeddm::problem::SourceModal::SourceModal(const Parameter &param,
                                            const Domain &omega){
  // Function space
  std::string family = param.getString("family");
  FunctionSpaceTypeForm0 HGrad =
    utils::functionspace::family<FunctionSpaceTypeForm0>(family);

  // Init
  this->omega  = &omega;
  this->target = param.getNumber("ny");
  this->gauss  = (int)(param.getNumber("gauss"));
  this->fSrc   = new Field0("fSrc", omega, HGrad, param.getNumber("order"));
  fSrc->addConstraint(omega.getBoundary(), 0.);

  // Warning
  msg::warning << "Modal source is not normalized" << msg::endl;
};

closeddm::problem::SourceModal::~SourceModal(void){
  delete fSrc;
};

SFunction closeddm::problem::SourceModal::get(void) const{
  // Helmholtz //
  std::string gauss = "Gauss" + std::to_string(this->gauss);
  Formulation<cmplx> formulation("fSourceEigenMode");

  formulation.integral(  d(dof(*fSrc)), d(tf(*fSrc)), *omega, gauss);
  formulation.integral(dt2_dof(*fSrc),    tf(*fSrc),  *omega, gauss);

  // Eigensolve //
  Vector<cmplx> lambda;
  formulation.pre();
  formulation.assemble(true);
  formulation.eigensolve(lambda, true, target);

  // Extract requested mode //
  return real(eigenfunction(*fSrc, 0));

  // // Save //
  // for(size_t i = 0; i < lambda.size(); i++){
  //   std::cout << "Mode " << i << ": " << lambda[i] << std::endl;
  //   save(eigenfunction(**fSrc, i), omega, "*fSrc_"+std::to_string(i));
  // }
  //
  // // Projection //
  // Field0 p("p", omega, FunctionSpaceTypeForm0::HierarchicalH1, order);
  // Formulation<cmplx> proj("projection");
  // proj.integral(dof(p), tf(p), omega, gauss);
  // proj.integral(-fSrc,  tf(p), omega, gauss);
  // proj.pre();
  // proj.assemble();
  // proj.solve();
  // gmshfem::post::save(p, omega, "p");
}

std::string closeddm::problem::SourceModal::info(void) const{
  return "modal source (target: "+std::to_string(target)+")";
}
