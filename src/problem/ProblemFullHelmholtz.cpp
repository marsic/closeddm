// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "ABC.h"
#include "Constant.h"
#include "ProblemFullHelmholtz.h"
#include "Utils.h"

using namespace closeddm::constant;
using namespace closeddm::problem;
using namespace closeddm::utils;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::field;
using namespace std;

ProblemFullHelmholtz::ProblemFullHelmholtz(Parameter &param,
                                           const SMap<Domain> &domain){
  // Parameters
  this->param = &param;

  // Domain
  this->domain = &domain;

  // Weak formulation
  this->form = NULL;
  this->src  = NULL;
  refresh();
}

ProblemFullHelmholtz::~ProblemFullHelmholtz(void){
  clear();
}

void ProblemFullHelmholtz::refresh(void){
  // Clear //
  ///////////
  clear();

  // Domains //
  /////////////
  const Domain &omega  = domain->at("omega");
  const Domain &wall   = domain->at("wall");
  const Domain &gammaD = domain->at("gammaD");
  const Domain &gammaI = domain->at("gammaI");

  // Source field and 0th-order ABC //
  ////////////////////////////////////
  src = Source::factory(*param, gammaD,
                        param->getFlag("eigenmode")? "zero" : "");
  SFunction fSrc = src->get();
  cmplx     kInf = abc::zero(*param);

  // Fields //
  ////////////
  int O = param->getNumber("order");
  std::string family = param->getString("family");
  FunctionSpaceTypeForm0 HGrad =
    utils::functionspace::family<FunctionSpaceTypeForm0>(family);

  ffield["u"]      = Field0("u",  omega|gammaD|gammaI|wall, HGrad, O);
  ffield["lambda"] = Field0("lambda",   gammaD,             HGrad, O);
  ffield["us"]     = Field0("us", omega|gammaD|gammaI|wall, HGrad, O);

  // Proxies (references)
  Field0 &u      = ffield["u"];
  Field0 &lambda = ffield["lambda"];
  Field0 &us     = ffield["us"];

  // Constraints
  u.addConstraint(wall, 0);
  us.addConstraint(wall, 0);

  // Init 'us' with dummy formulation
  gmshfem::problem::Formulation<cmplx> dummy("dummy");
  dummy.integral(dof(us), tf(us), omega, "gauss1");
  dummy.pre();

  // Formulation //
  /////////////////
  // Wavenumber
  cmplx k = complexify(param->getNumber("k"), param->getNumber("kd"));

  // Info string
  std::string info = "Helmholtz";
  info += " - FE"   +   std::to_string(O);
  info += " - k = " + utils::to_string(k);

  // Init
  this->form  = new gmshfem::problem::Formulation<cmplx>(info);
  string quad = quadrature();
  gmshfem::problem::Formulation<cmplx> &form = *this->form; // Proxy (reference)

  // Weak formulation
  form.integral(     d(dof(u)), d(tf(u)), omega, quad);
  form.integral(-k*k * dof(u),    tf(u),  omega, quad);
  form.integral(          -us,    tf(u),  omega, quad);

  form.integral(-J*kInf * dof(u), tf(u), gammaI, quad);

  form.integral(dof(lambda), tf(u), gammaD, quad);
  form.integral(dof(u), tf(lambda), gammaD, quad);
  form.integral(-fSrc,  tf(lambda), gammaD, quad);
}

void ProblemFullHelmholtz::clear(void){
  if(form) delete form;
  ffield.clear();
  if(src)  delete src;
}

double ProblemFullHelmholtz::residual(void) const{
  // Domain, quadrature rule and wavenumber
  const Domain &omega = domain->at("omega");
  const string quad = quadrature();
  const cmplx k = complexify(param->getNumber("k"), param->getNumber("kd"));

  // Fields: residual and wave
  const Field0 &u = ffield.at("u"); // Wave
  Field0 r(u);                      // Residual

  // Formulation for residual
  gmshfem::problem::Formulation<cmplx> res("residual");
  res.integral(   dof(r),   tf(r),  omega, quad);
  res.integral(    -d(u), d(tf(r)), omega, quad);
  res.integral(+k*k * u,    tf(r),  omega, quad);

  // Assemble problem (we're just interested in the RHS)
  res.pre();
  res.assemble();

  // Solve and save residual field (optional)
  // res.solve();
  // gmshfem::post::save(r, omega, "residual");

  // Return norm of RHS (estimation of 'real' residual)
  gmshfem::algebra::Vector<cmplx> b;
  res.getRHS(b);
  return b.norm();
}

string ProblemFullHelmholtz::info(const std::string &kind, int i) const{
  if     (kind == "solver") return "Direct Problem";
  else if(kind == "domain") return " - " + infoDomain();
  else if(kind == "source") return " - " + infoSource();
  else                      return infoUnknwn();
}

int ProblemFullHelmholtz::nInfo(const std::string &kind) const{
  return 0;
}
