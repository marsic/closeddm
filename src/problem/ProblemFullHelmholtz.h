// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_PROBLEM_FULL_HELMHOLTZ_H_
#define _CLOSEDDM_PROBLEM_FULL_HELMHOLTZ_H_

#include "ProblemFull.h"

namespace closeddm{
  namespace problem{
    /// A ProblemFull for the Helmholtz equation

    /// This Problem defines the following fields:
    ///  - u, the wave field defining the Helmholtz problem
    ///  - lambda, a Lagrange multiplier for (weak) Dirichlet conditions
    class ProblemFullHelmholtz final: public ProblemFull{
    private:
      const SMap<gmshfem::domain::Domain> *domain;

    public:
      /// Constructs a new ProblemFullHelmholtz defined on the Domain `domain`
      /// with the Parameter `param`.
      ///
      /// The Domain is composed of:
      ///  - "omega", the computational domain
      ///  - "wall", the walls (i.e. u = 0)
      ///  - "gammaD", the boundary along which the Source is imposed
      ///  - "gammaI", the boundary along which the ABC is imposed
      ///
      /// The relevant parameters are:
      ///  - "k", the real part of the wavenumber of the Helmholtz problem
      ///  - "kd", the imaginary part of the wavenumber of the Helmholtz problem
      ///  - "order", the FE order
      ///  - "eigenmode", flag for eigenvalue problem
      ProblemFullHelmholtz(Parameter &param,
                           const SMap<gmshfem::domain::Domain> &domain);
      ~ProblemFullHelmholtz(void);

      virtual void refresh(void) override final;
      virtual double residual(void) const override final;
      virtual std::string info(const std::string &kind, int i=0) const
                                                                 override final;
      virtual int nInfo(const std::string &kind) const override final;

    protected:
      virtual void clear(void) override final;
    };
  }
}

#endif
