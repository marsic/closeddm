// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "gmsh.h"

#include "ABC.h"
#include "Constant.h"
#include "Message.h"
#include "Utils.h"

using namespace closeddm;
using namespace closeddm::constant;
using namespace closeddm::input;
using namespace closeddm::utils;

#define SQU(a) (a*a)

cmplx closeddm::problem::abc::zero(const Parameter &param){
  // Parameters
  cmplx   k = complexify(param.getNumber("k"), param.getNumber("kd"));
  double ly = param.getNumber("lsy");
  double lz = param.getNumber("lsz");
  int    ny = param.getNumber("ny");
  int    nz = param.getNumber("nz");
  int   dim = gmsh::model::getDimension();

  // Warning if multimode
  if(param.getFlag("allmodes") && !param.getFlag("cavity"))
    msg::warning << "Multi-mode excitation in open waveguide: "
                 << "0th-order ABC is not exact!" << msg::endl;

  // ABC
  cmplx tmp = SQU(k);
  if(dim > 1) tmp -= SQU(ny*Pi/ly);
  if(dim > 2) tmp -= SQU(nz*Pi/lz);
  if(dim > 3) throw gmshfem::common::Exception("closeddm::problem::abc: "
                                               "cannot do more than 3D ;)");

  return std::sqrt(tmp);
}
