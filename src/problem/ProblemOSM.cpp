// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#include "ProblemOSM.h"

using namespace closeddm;
using namespace closeddm::problem;

ProblemOSM::~ProblemOSM(void){
}

size_t ProblemOSM::size(void) const{
  return form->size();
}

gmshddm::problem::Formulation<cmplx>& ProblemOSM::osFormulation(void){
  return *form;
}

Field0& ProblemOSM::field(const std::string &name,
                          size_t i, size_t j){
  SMap<SField0>::iterator itS = sffield.find(name);
  SMap<IField0>::iterator itI = iffield.find(name);

  if(itS != sffield.end())
    return itS->second(i);
  else if(itI != iffield.end())
    return itI->second(i, j);
  else
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
}

const Field0& ProblemOSM::field(const std::string &name,
                                size_t i, size_t j) const{
  SMap<SField0>::const_iterator itS = sffield.find(name);
  SMap<IField0>::const_iterator itI = iffield.find(name);

  if(itS != sffield.end())
    return itS->second(i);
  else if(itI != iffield.end())
    return itI->second(i, j);
  else
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
}

std::vector<Field0*> ProblemOSM::field(const std::string &name){
  SMap<SField0>::iterator it = sffield.find(name);
  if(it != sffield.end()){
    std::vector<Field0*> sub(it->second.size());
    for(size_t i = 0; i < sub.size(); i++)
      sub[i] = &(it->second(i));
    return sub;
  }
  else{
    throw gmshfem::common::Exception("Problem::field(): unknown field "+name);
  }
}

gmshfem::problem::Formulation<cmplx>& ProblemOSM::formulation(size_t i){
  return (*form)(i);
}

gmshfem::algebra::MatrixCRS<cmplx> ProblemOSM::matrix(void) const{
  gmshfem::algebra::MatrixCRS<cmplx> D;
  D = form->computeMatrix(); // Conversion MatrixCCS -> CRS
  return D;
}

void ProblemOSM::pre(void){
  form->pre();
}

void ProblemOSM::assemble(void){
  form->pre();
}

void ProblemOSM::solve(std::string solver, double tol, int maxIt){
  form->solve((solver == "default")? "gmres+mgs" : solver, tol, maxIt);
}

size_t ProblemOSM::iteration(void) const{
  return form->numberOfIterations();
}
