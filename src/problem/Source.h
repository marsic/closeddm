// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_SOURCE_H_
#define _CLOSEDDM_SOURCE_H_

#include "CloseDDM.h"
#include "Parameter.h"

namespace closeddm{
  namespace problem{
    /// Base class and factory for source signals
    class Source{
    public:
      /// Returns a source defined on the given Domain `omega` with the given
      /// Parameter `param` (see child classes for the relevant ones).
      /// The kind of the Source can be defined either via
      /// the "source" Parameter or overwritten with `force` (if not empty).
      /// The possible kinds are:
      ///  - "modal", a port mode obtained from a modal (eigenvalue) analysis
      ///  - "rectangle", a port mode of a rectangular waveguide
      ///  - "zero", a zero signal
      static Source* factory(const closeddm::input::Parameter &param,
                             const gmshfem::domain::Domain &omega,
                             std::string force="");

    public:
      /// Destructor
      virtual ~Source(void) = 0;

      /// Returns the function representing this Source
      virtual SFunction    get(void) const = 0;
      /// Returns a string describing this Source
      virtual std::string info(void) const = 0;
    };
  }
}

#endif
