// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_ABC_H_
#define _CLOSEDDM_ABC_H_

#include "CloseDDM.h"
#include "Parameter.h"

namespace closeddm{
  namespace problem{
    /// Absorbing boundary condition (ABC)
    namespace abc{
      /// Returns the value of a zeroth-order ABC of a rectangular waveguide
      /// defined with the given Parameter `param`; the relevant parameters are:
      ///  - "k", the real part of the wavenumber
      ///  - "kd", the imaginary part of the wavenumber
      ///  - "lsy", the 'y' dimension of the ABC boundary
      ///  - "lsz", the 'z' dimension of the ABC boundary
      ///  - "ny", the mode number along 'y' of the signal to absorb
      ///  - "nz", the mode number along 'z' of the signal to absorb
      ///  - "allmodes", flag is true when the source signal is multi-modal;
      ///    in the case the zeroth-order ABC is inexact...
      cmplx zero(const closeddm::input::Parameter &param);
    };
  }
}

#endif
