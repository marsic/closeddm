// closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
// Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
// See the LICENSE.txt file for license information

#ifndef _CLOSEDDM_SOURCE_ZERO_H_
#define _CLOSEDDM_SOURCE_ZERO_H_

#include "Source.h"

namespace closeddm{
  namespace problem{
    /// A zero Source
    class SourceZero final: public Source{
    public:
      /// Constructs a new SourceZero
      SourceZero(void);
      ~SourceZero(void);

      virtual SFunction    get(void) const override final;
      virtual std::string info(void) const override final;
    };
  }
}

#endif
