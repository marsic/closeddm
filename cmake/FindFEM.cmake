# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find GmshFEM                                         #
#                                                             #
# This will define:                                           #
#  FEM_FOUND - Was GmshFEM found?                             #
#  FEM_INC   - GmshFEM include directory                      #
#  FEM_LIB   - GmshFEM library                                #
#                                                             #
# Usage:                                                      #
#  find_package(FEM)                                          #
#                                                             #
# Remark:                                                     #
#  Does not touch FEM_INC or FEM_LIB if already defined       #
###############################################################

## Try to set FEM_LIB and FEM_INC from environment variables ##
###############################################################
if(NOT DEFINED FEM_LIB)
  find_library(FEM_LIB "gmshfem" HINTS $ENV{FEM_LIB} NO_CACHE)
endif()
if(NOT DEFINED FEM_INC)
  find_path(FEM_INC  "GmshFem.h" HINTS $ENV{FEM_INC} NO_CACHE
            PATH_SUFFIXES "gmshfem")
  if(FEM_INC)
    get_filename_component(FEM_INC ${FEM_INC}/.. ABSOLUTE)
  endif()
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FEM
  "GmshFEM cannot be found"
  FEM_LIB FEM_INC)
