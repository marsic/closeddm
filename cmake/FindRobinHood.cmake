# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find robin_hood unordered map & set                  #
# https://github.com/martinus/robin-hood-hashing              #
#                                                             #
# This will define:                                           #
#  RBH_FOUND - Was robin_hood found?                          #
#  RBH_INC   - robin_hood include directory                   #
#                                                             #
# Usage:                                                      #
#  find_package(RobinHood)                                    #
#                                                             #
# Remark:                                                     #
#  Does not touch RBH_INC if already defined                  #
###############################################################

## Try to set RBH_LIB from environment variables ##
###################################################
if(NOT DEFINED RBH_INC)
  find_path(RBH_INC "robin_hood.h" HINTS $ENV{RBH_INC} NO_CACHE
    PATH_SUFFIXES "gmshfem")
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RobinHood
  "robin_hood cannot be found"
  RBH_INC)
