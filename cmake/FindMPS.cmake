# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find MPSolve                                         #
#                                                             #
# This will define:                                           #
#  MPS_FOUND - Was MPSolve found?                             #
#  MPS_INC   - MPSolve include directory                      #
#  MPS_LIB   - MPSolve library                                #
#                                                             #
# Usage:                                                      #
#  find_package(MPS)                                          #
#                                                             #
# Remark:                                                     #
#  Does not touch MPS_INC or MPS_LIB if already defined       #
###############################################################

## Try to set MPS_LIB and MPS_INC from environment variables ##
###############################################################
if(NOT DEFINED MPS_LIB)
  find_library(MPS_LIB "mps" HINTS $ENV{MPS_LIB} NO_CACHE)
endif()
if(NOT DEFINED MPS_INC)
  find_path(MPS_INC  "mps.h" HINTS $ENV{MPS_INC} NO_CACHE
            PATH_SUFFIXES "mps")
  if(MPS_INC)
    get_filename_component(MPS_INC ${MPS_INC}/.. ABSOLUTE)
  endif()
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MPS
  "MPSolve cannot be found"
  MPS_LIB MPS_INC)
