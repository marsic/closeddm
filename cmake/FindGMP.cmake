# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find the GNU Multiple Precision Arithmetic Library   #
#                                                             #
# This will define:                                           #
#  GMP_FOUND - Was GMP found?                                 #
#  GMP_INC   - GMP include directory                          #
#  GMP_LIB   - GMP library                                    #
#                                                             #
# Usage:                                                      #
#  find_package(GMP)                                          #
###############################################################

if(NOT DEFINED GMPC_LIBC)
  find_library(GMP_LIBC   "gmp" $ENV{GMP_LIBC} NO_CACHE)
endif()
if(NOT DEFINED GMPX_LIBX)
  find_library(GMP_LIBX "gmpxx" $ENV{GMP_LIBX} NO_CACHE)
endif()
if(NOT DEFINED GMP_INC)
  find_path(GMP_INC   "gmpxx.h" $ENV{GMP_INC}  NO_CACHE)
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GMP
  "GMP cannot be found"
  GMP_LIBC GMP_LIBX GMP_INC)
