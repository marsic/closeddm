# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find GmshDDM                                         #
#                                                             #
# This will define:                                           #
#  DDM_FOUND - Was GmshDDM found?                             #
#  DDM_INC   - GmshDDM include directory                      #
#  DDM_LIB   - GmshDDM library                                #
#                                                             #
# Usage:                                                      #
#  find_package(DDM)                                          #
#                                                             #
# Remark:                                                     #
#  Does not touch DDM_INC or DDM_LIB if already defined       #
###############################################################

## Try to set DDM_LIB and DDM_INC from environment variables ##
###############################################################
if(NOT DEFINED DDM_LIB)
  find_library(DDM_LIB "gmshddm" HINTS $ENV{DDM_LIB} NO_CACHE)
endif()
if(NOT DEFINED DDM_INC)
  find_path(DDM_INC  "GmshDdm.h" HINTS $ENV{DDM_INC} NO_CACHE
            PATH_SUFFIXES "gmshddm")
  if(DDM_INC)
    get_filename_component(DDM_INC ${DDM_INC}/.. ABSOLUTE)
  endif()
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(DDM
  "GmshDDM cannot be found"
  DDM_LIB DDM_INC)
