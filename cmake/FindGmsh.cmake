# closeddm - a ONELAB solver for testing optimized Schwarz methods in cavities
# Copyright (C) 2021-2023 N. Marsic, Technische Universitaet Darmstadt
# See the LICENSE.txt file for license information

###############################################################
# Try to find Gmsh                                            #
#                                                             #
# This will define:                                           #
#  GMSH_FOUND - Was Gmsh found?                               #
#  GMSH_INC   - Gmsh include directory                        #
#  GMSH_LIB   - Gmsh library                                  #
#                                                             #
# Usage:                                                      #
#  find_package(Gmsh)                                         #
#                                                             #
# Remark:                                                     #
#  Does not touch GMSH_INC or GMSH_LIB if already defined     #
###############################################################

## Try to set GMSH_LIB and GMSH_INC from environment variables ##
#################################################################
if(NOT DEFINED GMSH_LIB)
  find_library(GMSH_LIB "gmsh" HINTS $ENV{GMSH_LIB} NO_CACHE)
endif()
if(NOT DEFINED GMSH_INC)
  find_path(GMSH_INC "gmsh.h" HINTS $ENV{GMSH_INC} NO_CACHE)
endif()

## CMake check and done ##
##########################
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Gmsh
  "Gmsh cannot be found"
  GMSH_LIB GMSH_INC)
