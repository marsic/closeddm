# closeddm
closeddm is a [ONELAB](https://www.onelab.info) solver
for testing optimized
[Schwarz methods](https://en.wikipedia.org/wiki/Schwarz_alternating_method)
for time-harmonic wave problems in cavities.


## Dependencies
closeddm requires the following libraries:

* [GmshDDM](https://gitlab.onelab.info/gmsh/ddm)
* [GMP](https://gmplib.org) and
* [MPSolve](https://github.com/robol/MPSolve).

The main dependencies of GmshDDM are:

* [GmshFEM](https://gitlab.onelab.info/gmsh/fem)
* [Gmsh](https://gitlab.onelab.info/gmsh/gmsh)
* [PETSc](https://petsc.org) compiled with [MUMPS](http://mumps.enseeiht.fr)
* [SLEPc](https://slepc.upv.es) and
* [Eigen](https://eigen.tuxfamily.org).

Regarding compilation details (flags and so on),
see our [CI script](.gitlab-ci.yml), [container](ci/Dockerfile) (Linux builds)
and provisioning [script](ci/msys2/provision.sh)
(Microsoft Windows builds with [MSYS2](https://www.msys2.org)).

closeddm requires a mesh of the computational domain in the Gmsh
[`.msh`](https://gmsh.info/doc/texinfo/gmsh.html#File-formats) format
and supports [MPI](https://en.wikipedia.org/wiki/Message_Passing_Interface)
as well.


## Usage
Without ONELAB, closeddm can be used as follows:

```bash
./closeddm -msh domain.msh
```

where `domain.msh` is a mesh of the computational domain.
The problem at hand can be modified with additional command line options
(see [Parameter.cpp](src/interface/Parameter.cpp) for more information).

If a ONELAB database file is however available, it can be exploited by calling

```bash
./closeddm -msh domain.msh -db database.db
```

where `database.db` is the database file.

If MPI is supported, one can run

```bash
mpirun -np N ./closeddm -msh domain.msh
```

as well, where `N` is the number of process to run.

If ONELAB is availlable, closeddm can be added as a solver.
To do so, you must edit your `.gmshrc` file and add

```
Solver.Executable1 = "PATH_TO_CLOSEDDM_BINARY";
Solver.Name1 = "closeddm";
Solver.Extension1 = ".cdd";
```

assuming this is your first ONELAB solver
(otherwise you must use an index larger than `1`, see the
[solver options](https://gmsh.info/doc/texinfo/gmsh.html#Solver-options-list)
of Gmsh for more information).
One can then simply run

```bash
gmsh problem.cdd
```

for accessing the ONELAB GUI of Gmsh.
So far, `.cdd` files are just empty files that share the same name
as their corresponding `.geo` file.
See [test/rectangle.geo](test/rectangle.geo) for more information
concerning the physical tag convention.


## Screenshot
Below is a screenshot showing the solution
of a simulation involving a rectangular cavity filled with 7 circular obstacles
(each radius is half of a wavelength).
The cavity is excited with the first port mode applied on the left boundary
at a frequency close to a resonant one.
The ONELAB GUI is visble on this screenshot as well.

![screenshot](https://git.rwth-aachen.de/marsic/closeddm-static/raw/main/obstacles_D17_O07_R50_closed_ml64.png)


## References
closeddm is based on the following references:

* [D. A. Bini and L. Robol,
   "Solving secular and polynomial equations: A multiprecision algorithm,"
   _Journal of Computational and Applied Mathematics_, vol. 272,
   pp. 276–292, 2014.](https://doi.org/10.1016/j.cam.2013.04.037)
* [Y. Boubendir,
   "An analysis of the BEM-FEM non-overlapping domain decomposition method
    for a scattering problem,”
   _Journal of Computational and Applied Mathematics_, vol. 204, no. 2,
   pp. 282–291, 2007.](https://doi.org/10.1016/j.cam.2006.02.044)
* [Y. Boubendir, X. Antoine and C. Geuzaine,
   "A quasi-optimal non-overlapping domain decomposition algorithm
    for the Helmholtz equation,”
   _Journal of Computational Physics_, vol. 231, no. 2,
   pp. 262–280, 2012.](https://doi.org/10.1016/j.jcp.2011.08.007)
* [B. Després,
   "Décomposition de domaine et problème de Helmholtz,"
   _Comptes Rendus de l'Acadéemie des Sciences_, vol. 311,
   pp. 313-316, 1990.](https://gallica.bnf.fr/ark:/12148/bpt6k57815213)
* [M. J. Gander, F. Magoulès and F. Nataf,
   "Optimized Schwarz methods without overlap for the Helmholtz equation,”
   _SIAM Journal on Scientific Computing_, vol. 24, no. 1,
   pp. 38–60, 2002.](https://doi.org/10.1137/S1064827501387012)
* [C. Geuzaine and J.-F. Remacle,
   "Gmsh: A 3-D finite element mesh generator
    with built-in pre- and post-processing facilities,"
   _International Journal for Numerical Methods in Engineering_, vol. 79, no. 11,
   pp. 1309–1331, 2009](https://doi.org/10.1002/nme.2579)
* [N. Marsic, C. Geuzaine and H. De Gersem,
   "Transmission operators for the non-overlapping Schwarz method
    for solving Helmholtz problems in rectangular cavities,"
   _arXiv preprint_, 2022](https://arxiv.org/abs/2205.06518)
* [A. Royer, E. Béchet and C. Geuzaine,
   "Gmsh-Fem: An efficient finite element library based on Gmsh,"
   in _14th WCCMECCOMAS Congress_, 2021.](https://doi.org/10.23967/wccm-eccomas.2020.161)


## Funding
This research project has been funded by the
[Deutsche Forschungsgemeinschaft](https://dfg.de)
(DFG, German Research Foundation) -
Project number [445906998](https://gepris.dfg.de/gepris/projekt/445906998).

<div align="right">
    <a href="https://www.dfg.de">
        <img src="https://git.rwth-aachen.de/marsic/closeddm-static/raw/main/dfg_logo_schriftzug_blau_foerderung_en.gif"
             alt="dfg"
             width="200"/>
    </a>
</div>
