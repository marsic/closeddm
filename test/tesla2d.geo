// OpenCASCADE //
/////////////////
SetFactory("OpenCASCADE");

// User data & macros //
////////////////////////
Include "tesla2d.dat";
Include "macro.geo";

// Geometry //
//////////////
// Create N unit cells and translate them
L = 2*(D+R);
s = {};
For i In {0:NDOM-1}
  a  = news; Rectangle(a) = {0,  0, 0,  L,LY};
  b  = news; Disk(b)      = {D+R,LY,0,     R};
  c  = news; Disk(c)      = {D+R,0, 0,     R};
  s += BooleanUnion{Surface{a}; Delete;}{Surface{b, c}; Delete;};

  Translate{i*L, 0, 0}{Surface{s[i]};}
EndFor

// Fragment
CELL[] = BooleanFragments{Surface{s[]}; Delete;}{};

// Boundaries
eps = L/1000; Lxt = L; Lyt = LY; Lzt = 0; Call Slice;

// Boundary of boundaries
dGAMMA  = {};
dGAMMA += Abs(Boundary{Line{SRC[]};});
For i In {0:#INTR[]-1}
  dGAMMA += Abs(Boundary{Line{INTR[i]};});
EndFor
dGAMMA += Abs(Boundary{Line{END[]};});

// Total length of cavity chain (ONELAB) //
///////////////////////////////////////////
DefineConstant[
  LX = {NDOM*L, Name "00Geometry/00X dimension", Units "m", ReadOnly 1}
];

// Mesh //
//////////
Characteristic Length{PointsOf{Surface{CELL[]};}} = LC;
If(QUAD == 1)
  Recombine Surface "*";
EndIf

// Physical tags //
///////////////////
For i In {0:NDOM-1}
  Physical Surface(i+1) = {CELL[i]}; // Cells
EndFor

Physical Line(NDOM+1) = {SRC[]}; // Source

For i In {0:NDOM-2}
  Physical Line((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
EndFor

Physical Line((NDOM+1) + NDOM) = {END[]}; // Termination

For i In {0:NDOM-1}
  Physical Line((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
EndFor

For i In {0:NDOM}
  Physical Point((NDOM+1) + 2*NDOM + (i+1)) = {dGAMMA[{i*2, i*2+1}]}; // dBndry
EndFor
