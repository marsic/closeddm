#!/bin/sh

CDD="rectangle"
TC="pade"
VA="closed"
D="2 4 8 16 32 64"
N="1 2 4 8 16 32 64"

for i in $N
do
    for d in $D
    do
        gmsh ${CDD}.cdd \
             -setnumber "04DDM/07Number of auxiliary fields" ${i} \
             -setstring "04DDM/00Transmission condition" "${TC}" \
             -setstring "04DDM/01Variant" "${VA}" \
             -setnumber "04DDM/02Number of subdomains" ${d} \
             -setnumber "01Problem/06Cavity?" 1 \
             -setnumber "01Problem/05All modes?" 1 \
             -setnumber "01Problem/03Mode(s) along Y" 50 \
             - > log_cavity_${CDD}_D${d}_${VA}_${TC}_${i}
    done
done
