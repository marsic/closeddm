#!/bin/bash

## Usage
usage(){
    echo "Usage: sweep.sh simulation.cdd [options]"
    echo "  -setstring   Same as ONELAB -setstring"
    echo "  -setnumber   Same as ONELAB -setnumber"
    echo "  -param       Parameter (number) used for sweeping"
    echo "  -value       Value(s) (number) used for sweeping"
    echo "  -noview      Don't save views"
}

## ONELAB script to run
if [[ $# -gt 0 ]]
then
    CDD=$1
    shift
else
    echo "Error: no ONELAB script given"
    usage
    exit -1
fi

## Parse -setstring, -setnumber, -param and -value
OPT=""
PAR=""
VAL=""
while [[ $# -gt 0 ]]
do
    case $1 in
        -setstring|-setnumber) if [[ $# -gt 2 ]]
                               then
                                   OPT=$OPT" "$1" "\"$2\"" "\"$3\"
                                   shift
                                   shift
                               else
                                   echo "Error: "$1" takes two arguments"
                                   usage
                                   exit -1
                               fi
                               ;;
                       -value) while [[ $# -gt 1 && $2 != -* ]]
                               do
                                   VAL=$VAL" "$2
                                   shift
                               done
                               ;;
                       -param) if [[ $# -gt 1 && $2 != -* ]]
                               then
                                   PAR=\"$2\"
                                   shift
                               fi
                               ;;
                      -noview) OPT=$OPT" -setnumber \"05Post/00Save views?\" 0"
                               ;;
                            *) echo "Error: unknwon option "$1
                               usage
                               exit -1
                               ;;
    esac
    shift
done

## Check parameter
if [[ -n "$PAR" ]]
then
    echo "Parameter: "$PAR
else
    echo "Error: no parameter given"
    usage
    exit -1
fi

## Check value(s)
if [[ -n "$VAL" ]]
then
    echo "Value(s): "$VAL
else
    echo "Error: no value(s) given"
    usage
    exit -1
fi

## Sweep
echo "Option(s): "$OPT
echo "Starting sweeping"
for v in $VAL
do
    echo "> "$v
    eval "gmsh ${CDD} $OPT -setnumber $PAR $v - > log_sweep_$v"
done
