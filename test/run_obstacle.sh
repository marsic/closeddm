#!/bin/sh

## Echo
#######
echo $0 $@

## Positional arguments
#######################
if [[ $# -lt 4 ]]
then
    echo "run_obstacle: N_DOM N_OBSTACLE DIM R (in % of lambda) <ONELAB options>"
    exit -1
fi

if [[ $1 -lt 2 ]]
then
    echo "N_DOM must be larger than 2"
    exit -1
else
    N_DOM=$1
    shift
fi

if [[ $1 -le 0 ]]
then
    echo "N_OBSTACLE must be strictly positive"
    exit -1
else
    N_OBSTACLE=$1
    shift
fi

if [[ $1 -ne 2 && $1 -ne 3 ]]
then
    echo "DIM must be either 2 or 3"
    exit -1
else
    DIM=$1
    shift
fi

if [[ $1 -le 0 ]]
then
    echo "R must be strictly positive"
    exit -1
else
    R=$1
    shift
fi

if [[ $N_OBSTACLE -gt $N_DOM ]]
then
    echo "N_OBSTACLE must be smaller or equal to N_DOM"
    exit -1
fi

## Other arguments
##################
while [[ $# -gt 0 ]]
do
    case $1 in
        -setstring|-setnumber) if [[ $# -gt 2 ]]
                               then
                                   OPT=$OPT" "$1" "\"$2\"" "\"$3\"
                                   shift
                                   shift
                               else
                                   echo "Error: "$1" takes two arguments"
                                   exit -1
                               fi
                               ;;
                            -) OPT=$OPT" "$1
                               ;;
                            *) echo "Error: unknwon option "$1
                               exit -1
                               ;;
    esac
    shift
done

## Positions of obstacles
#########################
OL_D=""
MID=$((($N_DOM + 2 - 1)/2)) ## Ceiling division
i=0
for o in $(seq $N_OBSTACLE)
do
    OL_D=$OL_D" -setnumber 00Geometry/99Obstacle/Obstacle"$o"/00Domain "
    OL_D=$OL_D$(($MID+$i))
    if [[ $i -eq 0 ]]
    then
        i=1
    elif [[ $i -gt 0 ]]
    then
        i=$((-1*$i))
    else
        i=$((-1*$i + 1))
    fi
done

## Radius of obstacles
######################
OL_R=""
for o in $(seq $N_OBSTACLE)
do
    OL_R=$OL_R" -setnumber 00Geometry/99Obstacle/Obstacle"$o"/03Radius "$R
done

## Call
#######
CMD="gmsh obstacle.cdd"
CMD=$CMD" -setnumber \"00Geometry/99Dimension\" "$DIM
CMD=$CMD" -setnumber \"04DDM/02Number of subdomains\" "$N_DOM
CMD=$CMD" -setnumber \"00Geometry/99Obstacle/00Number\" "$N_OBSTACLE
CMD=$CMD" "$OL_D" "$OL_R" "$OPT

eval $CMD
