#!/bin/sh

CDD=rectangle
TC=pade
VA=closed
N="1 2 4 8 16 32 64 128"
T=50

for i in $N
do
    BASE=eig_cavity_${CDD}_t${T}_D02_${VA}_${TC}_${i}
    gmsh ${CDD}.cdd \
         -setnumber "00Geometry/99Interface" ${T} \
         -setnumber "04DDM/07Number of auxiliary fields" ${i} \
         -setstring "04DDM/00Transmission condition" ${TC} \
         -setstring "04DDM/01Variant" ${VA} \
         -setnumber "04DDM/05Spectrum?" 1 \
         -setnumber "01Problem/06Cavity?" 1 \
         -setnumber "05Post/00Save views?" 0 - \
        &> ${BASE}.log

    mv eig.csv ${BASE}.csv
    mv mat.csv ${BASE}.mat
done
