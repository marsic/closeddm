// OpenCASCADE //
SetFactory("OpenCASCADE");

// User data & macros //
Include "obstacle.dat";
Include "macro.geo";

// Geometry //
// Cells
c = {};
For n In {0:NDOM-1}
  If(DIM == 2)
    c += news; Rectangle(c[n]) = {0,0,0, LXC,LY};
    Translate{n*LXC, 0, 0}{Surface{c[n]};}
  Else
    c += news; Box(c[n]) = {0,0,0, LXC,LY,LZ};
    Translate{n*LXC, 0, 0}{Volume{c[n]};}
  EndIf
EndFor

// Obstacle
For o In {0:ON-1}
  tmpX = (OD~{o} - 1 + OX~{o}/100) * LXC;
  tmpY = OY~{o}/100*LY;
  If(DIM == 2)
    t = news; Disk(t) = {tmpX,tmpY,0, OR~{o}/100*LW};
    BooleanDifference{Surface{c[OD~{o}-1]}; Delete;}{Surface{t}; Delete;}
  Else
    t = news; Cylinder(t) = {tmpX,tmpY,0, 0,0,LZ, OR~{o}/100*LW};
    BooleanDifference{Volume{c[OD~{o}-1]};  Delete;}{Volume{t};  Delete;}
  EndIf
EndFor

// Fragment
If(DIM == 2)
  CELL[] = BooleanFragments{Surface{c[]}; Delete;}{};
Else
  CELL[] = BooleanFragments{Volume{c[]};  Delete;}{};
EndIf

// Boundaries
eps = LX/1000; Lxt = LXC; Lyt = LY; Lzt = LZ; Call Slice;

// Boundary of boundaries
dGAMMA  = {};
If(DIM == 2)
  dGAMMA += Abs(Boundary{Line{SRC[]};});
  For i In {0:#INTR[]-1}
    dGAMMA += Abs(Boundary{Line{INTR[i]};});
  EndFor
  dGAMMA += Abs(Boundary{Line{END[]};});
Else
  dGAMMA += Abs(Boundary{Surface{SRC[]};});
  For i In {0:#INTR[]-1}
    dGAMMA += Abs(Boundary{Surface{INTR[i]};});
  EndFor
  dGAMMA += Abs(Boundary{Surface{END[]};});
EndIf

// Mesh //
If(DIM == 2)
  Characteristic Length{PointsOf{Surface{CELL[]};}} = LC;
Else
  Characteristic Length{PointsOf{Volume{CELL[]};}}  = LC;
EndIf

Mesh.ElementOrder = 2;

// Physical tags //
For i In {0:NDOM-1}
  If(DIM == 2)
    Physical Surface(i+1) = {CELL[i]}; // Cells
  Else
    Physical  Volume(i+1) = {CELL[i]}; // Cells
  EndIf
EndFor

If(DIM == 2)
  Physical    Line(NDOM+1) = {SRC[]}; // Source
Else
  Physical Surface(NDOM+1) = {SRC[]}; // Source
EndIf

For i In {0:NDOM-2}
  If(DIM == 2)
    Physical    Line((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
  Else
    Physical Surface((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
  EndIf
EndFor

If(DIM == 2)
  Physical    Line((NDOM+1) + NDOM) = {END[]}; // Termination
Else
  Physical Surface((NDOM+1) + NDOM) = {END[]}; // Termination
EndIf

For i In {0:NDOM-1}
  If(DIM == 2)
    Physical    Line((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
  Else
    Physical Surface((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
  EndIf
EndFor

For i In {0:NDOM}
  If(DIM == 2)
    Physical Point((NDOM+1)+2*NDOM+(i+1)) = {dGAMMA[{i*2,   i*2+1}]}; // dBndry
  Else
    Physical  Line((NDOM+1)+2*NDOM+(i+1)) = {dGAMMA[{i*4,   i*4+1,
                                                     i*4+2, i*4+3}]}; // dBndry
  EndIf
EndFor
