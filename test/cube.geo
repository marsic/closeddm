// OpenCASCADE //
SetFactory("OpenCASCADE");

// User data //
Include "cube.dat";

// Geometry //
// Cells
v = {};
If(NDOM == 2)
  v += news; Box(v[0]) = {0,           0,0, LX*t2Dom/100,    LY,LZ};
  v += news; Box(v[1]) = {LX*t2Dom/100,0,0, LX*(1-t2Dom/100),LY,LZ};
Else
  For n In {0:NDOM-1}
    v += news; Box(v[n]) = {0,0,0, LX/NDOM,LY,LZ};
    Translate{n*LX/NDOM, 0, 0}{Volume{v[n]};}
  EndFor
EndIf

// Fragment
CELL[] = BooleanFragments{Volume{v[]}; Delete;}{};

// Boundaries
SRC  = {};
END  = {};
INTR = {};
For i In {0:NDOM-1}
  WALL~{i} = {};
EndFor

For i In {0:NDOM-1}
  tmp[] = Boundary{Volume{CELL[i]};};
  If(i == 0)
    SRC  += Abs(tmp[0]);
  Else
    INTR += Abs(tmp[0]);
  EndIf

  If(i == NDOM-1)
    END  += Abs(tmp[1]);
  Else
    INTR += Abs(tmp[1]);
  EndIf

  WALL~{i} += Abs(tmp[{2:5}]);
EndFor
INTR[] = Unique(INTR[]);

// Boundary of boundaries
dGAMMA~{0} = Abs(Boundary{Surface{SRC[]};});
For i In {1:NDOM-1}
  dGAMMA~{i} = Abs(Boundary{Surface{INTR[i-1]};});
EndFor
dGAMMA~{NDOM} = Abs(Boundary{Surface{END[]};});

// Mesh //
Characteristic Length{PointsOf{Volume{CELL[]};}} = LC;

// Physical tags //
For i In {0:NDOM-1}
  Physical Volume(i+1) = {CELL[i]}; // Cells
EndFor

Physical Surface(NDOM+1) = {SRC[]}; // Source

For i In {0:NDOM-2}
  Physical Surface((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
EndFor

Physical Surface((NDOM+1) + NDOM) = {END[]}; // Termination

For i In {0:NDOM-1}
  Physical Surface((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
EndFor

For i In {0:NDOM}
  Physical Line((NDOM+1) + 2*NDOM + (i+1)) = {dGAMMA~{i}[]}; // dBndry
EndFor
