// User data //
Include "trapeze.dat";

// Geometry //
// Points
eps = OP/100 * LY / NDOM / 2;
For n In {1:NDOM+1}
  Point(n)            = {+LX / NDOM * (n - 1),  0 - (n - 1) * eps, 0.5, LC};
  Point(n + NDOM + 1) = {+LX / NDOM * (n - 1), LY + (n - 1) * eps, 0.5, LC};
EndFor

// Lines
For n In {1:NDOM+1}
  Line(n) = {n, n + NDOM + 1};
EndFor

For n In {1:NDOM}
  Line(n + (NDOM + 1)       ) = {n           , n        + 1};
  Line(n + (NDOM + 1) + NDOM) = {n + NDOM + 1, n + NDOM + 2};
EndFor

// Faces
For n In {1:NDOM}
  Line Loop(n) = {n, n + (NDOM + 1) + NDOM, -(n + 1), -(n + (NDOM + 1))};
  Plane Surface(n) = {n};
EndFor

// Physicals //
For n In {1:NDOM}
  Physical Surface(n) = {n};
EndFor

For n In {1:NDOM+1}
  Physical Line(n + NDOM) = {n};
EndFor

For n In {1:NDOM}
  Physical Line(1*NDOM + NDOM+1 + n) = {n + NDOM+1, n + NDOM+1 + NDOM};
EndFor

For n In {1:NDOM+1}
  Physical Point(2*NDOM + NDOM+1 + n) = {n, n + NDOM + 1};
EndFor
