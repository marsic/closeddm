// Geometry //
// Cells
c = {};
For n In {0:NDOM-1}
  c += news; Box(c[n]) = {0,0,0, LX/NDOM,LY,LZ};
  Translate{n*LX/NDOM, 0, 0}{Volume{c[n]};}
EndFor

// I/O
IYY = LY*IY/100; IZZ = LZ*IZ/100;
OYY = LY*OY/100; OZZ = LZ*OZ/100;
c += news; Cylinder(c[NDOM+0]) = {0,IYY,IZZ, -IL,0,0, IW/2};
c += news; Cylinder(c[NDOM+1]) = {0,OYY,OZZ, -OL,0,0, OW/2};

// Fragment
CELL[] = BooleanFragments{Volume{c[]}; Delete;}{};

// Boundaries (auto with Slice)
eps = LX/1000; Lxt = LX/NDOM; Lyt = LY; Lzt = LZ; Call Slice;

// Boundaries (rework CELL[0])
//v = newv; Box(v) = {           -IL-eps,                 IYY-IW/2-eps,                  IZZ-IW/2-eps,
//                    (-IL+eps)-(-IL-eps),(IYY+IW/2+eps)-(IYY-IW/2-eps), (IZZ+IW/2+eps)-(IZZ-IW/2-eps)};
io_1 = Abs(Surface In BoundingBox{-IL-eps, IYY-IW/2-eps, IZZ-IW/2-eps,
                                  -IL+eps, IYY+IW/2+eps, IZZ+IW/2+eps});
io_2 = Abs(Surface In BoundingBox{-OL-eps, OYY-OW/2-eps, OZZ-IW/2-eps,
                                  -OL+eps, OYY+OW/2+eps, OZZ+OW/2+eps});
SRC[] = {io_1};
WALL~{0}[] = {};
tmp[] = Abs(CombinedBoundary{Volume{CELL[{0,NDOM,NDOM+1}]};});
For i In {0:#tmp[]-1}
  If(tmp[i] != io_1 && tmp[i] != io_2 && tmp[i] != INTR[0])
    WALL~{0} += tmp[i];
  EndIf
EndFor

// Boundary of boundaries
dGAMMA~{0} = Abs(Boundary{Surface{SRC[]};});
For i In {1:NDOM-1}
  dGAMMA~{i} = Abs(Boundary{Surface{INTR[i-1]};});
EndFor
dGAMMA~{NDOM} = Abs(Boundary{Surface{END[]};});

// Mesh //
Mesh.Algorithm3D = 10; // HXT
Characteristic Length{PointsOf{Volume{CELL[]};}} = LC;

// Physical tags //
For i In {0:NDOM-1}
  If(i == 0)
    Physical Volume(i+1) = {CELL[{0, NDOM, NDOM+1}]}; // Cells
  Else
    Physical Volume(i+1) = {CELL[i]};                 // Cells
  EndIf
EndFor

Physical Surface(NDOM+1) = {SRC[]}; // Source

For i In {0:NDOM-2}
  Physical Surface((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
EndFor

Physical Surface((NDOM+1) + NDOM) = {END[]}; // Termination

For i In {0:NDOM-1}
  Physical Surface((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
EndFor

For i In {0:NDOM}
  Physical Curve((NDOM+1) + 2*NDOM + (i+1)) = {dGAMMA~{i}[]}; // dBndry
EndFor
