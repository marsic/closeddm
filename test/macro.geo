Macro Slice
  // Determines                                                       //
  //  * the source (SRC)                                              //
  //  * the computational domain truncation (END)                     //
  //  * the interfaces (INTR)                                         //
  //  * the walls (WALL)                                              //
  // by slicing the domain along the X-axis in identical parts        //
  //                                                                  //
  // Assumptions:                                                     //
  //  * the computational domain is [0, LX] x [0, LY] in 2D           //
  //  * the computational domain is [0, LX] x [0, LY] x [0, LZ] in 3D //
  //                                                                  //
  // Input:                                                           //
  //  * NDOM   - number of subdomains                                 //
  //  * DIM    - problem dimensionality (2 or 3)                      //
  //  * eps    - tolerance for {Curve,Surface} In BoundingBox{}       //
  //  * Lxt    - X extension of a subdomain                           //
  //  * Lyt    - Y extension of a subdomain interface                 //
  //  * Lzt    - Z extension of a subdomain interface                 //
  //  * CELL[] - A list with the subdomains                           //
  //////////////////////////////////////////////////////////////////////
  // Init
  SRC  = {};
  END  = {};
  INTR = {};
  For i In {0:NDOM-1}
    WALL~{i} = {};
  EndFor

  // Populate
  For i In {0:NDOM-1}
    If(DIM == 2)
      l = Abs(Curve In BoundingBox{(i+0)*Lxt-eps,  0-eps,  0-eps,
                                   (i+0)*Lxt+eps,Lyt+eps,Lzt+eps});
      r = Abs(Curve In BoundingBox{(i+1)*Lxt-eps,  0-eps,  0-eps,
                                   (i+1)*Lxt+eps,Lyt+eps,Lzt+eps});
      bnd[] = Abs(Boundary{Surface{CELL[i]};});
    Else
      l = Abs(Surface In BoundingBox{(i+0)*Lxt-eps,  0-eps,  0-eps,
                                     (i+0)*Lxt+eps,Lyt+eps,Lzt+eps});
      r = Abs(Surface In BoundingBox{(i+1)*Lxt-eps,  0-eps,  0-eps,
                                     (i+1)*Lxt+eps,Lyt+eps,Lzt+eps});
      bnd[] = Abs(Boundary{Volume{CELL[i]};});
    EndIf

    If(i == 0)
      SRC  += l;
      INTR += r;
    ElseIf(i == NDOM-1)
      INTR += l;
      END  += r;
    Else
      INTR += l;
      INTR += r;
    EndIf

    For j In {0:#bnd[]-1}
      If(bnd[j] != l && bnd[j] != r)
        WALL~{i} += bnd[j];
      EndIf
    EndFor
  EndFor

  INTR[] = Unique(INTR[]);
Return
