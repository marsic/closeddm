// User data //
Include "rectangle.dat";

// Geometry //
// Points
For n In {1:NDOM+1}
  If(NDOM == 2 && n == 2)
    t = 2 * t2Dom/100.;
  Else
    t = 1;
  EndIf

  eps = 0;
  If(TEND && (n == 1 || n == NDOM+1))
    eps = LX * TILT / 100;
  EndIf
  If(TINT && (n != 1 && n != NDOM+1))
    eps = LX * TILT / 100;
  EndIf

  Point(n)            = {+LX / NDOM * (n - 1) * t - eps/2,  0, 0.5, LC};
  Point(n + NDOM + 1) = {+LX / NDOM * (n - 1) * t + eps/2, LY, 0.5, LC};
EndFor

// Lines
For n In {1:NDOM+1}
  Line(n) = {n, n + NDOM + 1};
EndFor

For n In {1:NDOM}
  Line(n + (NDOM + 1)       ) = {n           , n        + 1};
  Line(n + (NDOM + 1) + NDOM) = {n + NDOM + 1, n + NDOM + 2};
EndFor

// Faces
For n In {1:NDOM}
  Line Loop(n) = {n, n + (NDOM + 1) + NDOM, -(n + 1), -(n + (NDOM + 1))};
  Plane Surface(n) = {n};
EndFor

// Mesh //
If(STRUCT == 1)
  Transfinite Surface "*";
EndIf
If(QUAD == 1)
  Recombine Surface "*";
EndIf

// Physicals //
For n In {1:NDOM}
  Physical Surface(n) = {n};
EndFor

For n In {1:NDOM+1}
  Physical Line(n + NDOM) = {n};
EndFor

For n In {1:NDOM}
  Physical Line(1*NDOM + NDOM+1 + n) = {n + NDOM+1, n + NDOM+1 + NDOM};
EndFor

For n In {1:NDOM+1}
  Physical Point(2*NDOM + NDOM+1 + n) = {n, n + NDOM + 1};
EndFor
