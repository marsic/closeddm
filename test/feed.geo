// OpenCASCADE //
SetFactory("OpenCASCADE");

// User data & geometry //
Include "feed.dat";
Include "macro.geo";
If(DIM == 2)
  Include "feed2D.geo";
Else
  Include "feed3D.geo";
EndIf
