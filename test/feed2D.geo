// Geometry //
// Cells
c = {};
For n In {0:NDOM-1}
  c += news; Rectangle(c[n]) = {0,0,0, LX/NDOM,LY};
  Translate{n*LX/NDOM, 0, 0}{Surface{c[n]};}
EndFor

// I/O
IYY = LY*IY/100;
OYY = LY*OY/100;
c += news; Rectangle(c[NDOM+0]) = {0,IYY-IW/2,0, -IL,IW};
c += news; Rectangle(c[NDOM+1]) = {0,OYY-OW/2,0, -OL,OW};

// Fragment
CELL[] = BooleanFragments{Surface{c[]}; Delete;}{};

// Boundaries (auto with Slice)
eps = LX/1000; Lxt = LX/NDOM; Lyt = LY; Lzt = 0; Call Slice;

// Boundaries (rework CELL[0])
io_1 = Abs(Curve In BoundingBox{-IL-eps, IYY-IW/2-eps, 0-eps,
                                -IL+eps, IYY+IW/2+eps, 0+eps});
io_2 = Abs(Curve In BoundingBox{-OL-eps, OYY-OW/2-eps, 0-eps,
                                -OL+eps, OYY+OW/2+eps, 0+eps});
SRC[] = {io_1};
WALL~{0}[] = {};
tmp[] = Abs(CombinedBoundary{Surface{CELL[{0,NDOM,NDOM+1}]};});
For i In {0:#tmp[]-1}
  If(tmp[i] != io_1 && tmp[i] != io_2 && tmp[i] != INTR[0])
    WALL~{0} += tmp[i];
  EndIf
EndFor

// Boundary of boundaries
dGAMMA~{0} = Abs(Boundary{Curve{SRC[]};});
For i In {1:NDOM-1}
  dGAMMA~{i} = Abs(Boundary{Curve{INTR[i-1]};});
EndFor
dGAMMA~{NDOM} = Abs(Boundary{Curve{END[]};});

// Mesh //
Characteristic Length{PointsOf{Surface{CELL[]};}} = LC;

// Physical tags //
For i In {0:NDOM-1}
  If(i == 0)
    Physical Surface(i+1) = {CELL[{0, NDOM, NDOM+1}]}; // Cells
  Else
    Physical Surface(i+1) = {CELL[i]};                 // Cells
  EndIf
EndFor

Physical Line(NDOM+1) = {SRC[]}; // Source

For i In {0:NDOM-2}
  Physical Curve((NDOM+1) + (i+1)) = {INTR[i]}; // Interfaces
EndFor

Physical Curve((NDOM+1) + NDOM) = {END[]}; // Termination

For i In {0:NDOM-1}
  Physical Curve((NDOM+1) + 1*NDOM + (i+1)) = {WALL~{i}[]}; // Walls
EndFor

For i In {0:NDOM}
  Physical Point((NDOM+1) + 2*NDOM + (i+1)) = {dGAMMA~{i}[]}; // dBndry
EndFor
