#!/bin/bash

if [[ $# -ne 3 ]]
then
    echo "Usage: parse.sh [string match] [substring match] file"
    echo "Return: for each line in 'file' that matches [string match],"
    echo "        find a substring of the type"
    echo "                  [subtring match] = float"
    echo "        and return the above float"

    exit -1
fi

FLOAT='[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
grep "$1" $3 | grep -oE "$2 = $FLOAT" | awk '{print $(NF)}'
