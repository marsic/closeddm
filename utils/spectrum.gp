#!/usr/bin/gnuplot

## Parameters
#set key outside
set key right top
set key noenhanced
set datafile separator ";"

## Spectrum
plot for [i=1:words(ARG1)] word(ARG1, i) using 1:2 \
     title word(ARG1, i) with points

## Unit circle
set parametric
set trange [0:2*pi]
fx(t) = cos(t) + 1
fy(t) = sin(t)

replot fx(t),fy(t) title "Unit circle" with lines linecolor black

## Resize plot
#set size ratio -1
#replot

## Mouse interaction
pause mouse close
