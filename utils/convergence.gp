#!/usr/bin/gnuplot

## Parameters
#set key outside
set key right top
set key noenhanced
set datafile separator comma
set logscale x 2
set logscale y

## Plot
plot for [i=2:*] ARG1 using 1:(column(i)) with linespoints title columnheader(i)

pause mouse close
