#!/usr/bin/gnuplot

## Parameters
#set key outside
set key right top
set key noenhanced
set datafile separator comma
set logscale y

## Plot
plot for [i=0:*] ARG1 index i using 1 \
     with linespoints title columnheader(1)

pause mouse close
