#!/bin/bash
set -e

if [[ $# -ne 4 ]]
then
    echo "Usage: clean_ci_pipeline.sh [PER_PAGE] [URL] [PROJECT] [TOKEN]"
    echo "Deletes PER_PAGE entries from a GitLab repository's CI pipeline."
    echo "The repository is identified with its base URL and its PROJECT id."
    echo "In addition, a valid access TOKEN must be provided."

    exit -1
fi

PER_PAGE=$1 # How many to delete from the oldest
URL=$2      # Repository base URL
PROJECT=$3  # Numeric id of project
TOKEN=$4    # Personal access token

for PIPELINE in $(curl --header "PRIVATE-TOKEN: $TOKEN" "$URL/api/v4/projects/$PROJECT/pipelines?per_page=$PER_PAGE&sort=asc" | jq '.[].id')
do
    echo "Deleting pipeline $PIPELINE"
    curl --header "PRIVATE-TOKEN: $TOKEN" \
         --request "DELETE" "$URL/api/v4/projects/$PROJECT/pipelines/$PIPELINE"
done

## See https://stackoverflow.com/questions/53355578/how-to-delete-gitlab-ci-jobs-pipelines-logs-builds-and-history
