function p = closeOSVol(s, k, l, t, q, lambda, g)
  %% Source is p_0(-l/2) = q

  alpha = -1j*sqrt(k.*k - s.*s);

  beta0 = zeros(size(s));
  for i = (1:length(s))
    beta0(i) = 2*alpha(i)     .* cosh(alpha(i)*t*l) + ...
               2*lambda(i, 1) .* sinh(alpha(i)*t*l);
  end

  beta1 = zeros(size(s));
  for i = (1:length(s))
    beta1(i) = 2*alpha(i)     .* cosh(alpha(i)*(1-t)*l) + ...
               2*lambda(i, 2) .* sinh(alpha(i)*(1-t)*l);
  end

  p = zeros(1, 2*length(s));
  for i = (1:length(s))
    p(2*i-1) = 2*sinh(alpha(i)   *t *l)/beta0(i) * g(2*i-1);
    p(2*i-0) = 2*sinh(alpha(i)*(1-t)*l)/beta1(i) * g(2*i-0);
    p(2*i-1) = p(2*i-1) + 2*q(i)*alpha(i)/beta0(i);
  end
end
