function n = nPropModes(k, h)
  n = floor(k*h/pi);
end
