%% Convergence radius for 2 subdomains

function rho = Rho2Dom(tc, dtn)
  tc0 = tc(:, 1); %%  Left TC
  tc1 = tc(:, 2); %% Right TC

  dtn0 = dtn(:, 1); %%  Left DtN
  dtn1 = dtn(:, 2); %% Right DtN

  rho = (tc0 - dtn0) ./ (tc0 + dtn1) .* ...
        (tc1 - dtn1) ./ (tc1 + dtn0);
end
