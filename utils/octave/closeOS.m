%% Data
l = 0.50;
t = 0.33;
h = 0.25;
k = 50.002*pi/l;

np = nPropModes(k, h);
m  = (0:3*np);
q  = ones(size(m)); %% source terms (per mode)
solver = 'gmres';   %% gmres/fixed

%% Aux fields, if any
nAuxRcot = [32 32]; %2 * TCRcotMinAux(k, [(1-t) t]*l);
nAuxPade = 32;

%% Initial guess for g
gInit = rand(2*length(m), 1);

%% DtN
dtn = @(s)TCOpt(s, k, l, t, 'closed');

%% TC
tc = @(s)TCPadeC(s, k, l, t, nAuxRcot, [0 0]/100); nAuxRcot
%tc = @(s)TCRcot(s, k, l, t, nAuxRcot, [0 0]/100); nAuxRcot
%tc = @(s)TCPade(s, k, nAuxPade, [0 0], 1*pi/4);   nAuxPade
%tc = @(s)TCOO0(s, k, l, t, [0 0]/100, 'closed');
%tc = dtn;

%% x for plotting solution
nLambda = 2*floor((k*l)/(2*pi)/2); % Wavelength per l (rounded to an even int.)
x = (-l/2:l/(nLambda*20):+l/2);    % Sample with 20 pts. per wavelength

%% Modes
s = m*pi/h;
fprintf('# Modes: %d | Total propagating modes: %d\n', length(m), np);

%% Lambda (left/right)for the chosen modes
lambda = tc(s);

%% Solve
switch(solver)
  case 'gmres' %% GMRES
    N       = length(gInit); %% System size
    tol     = 1e-9;          %% GMRES tolerance
    zero    = zeros(size(gInit));
    AMinusI = @(g)closeOSAMinusI(s, k, l, t, lambda, g);
    b       = closeOSUp(lambda, zero, closeOSVol(s, k, l, t, q, lambda, zero));
    [g, flag, relRes, iter, resVec] = gmres(AMinusI, b, [],tol,N,[],[], gInit);
    if(flag == 0)
      fprintf('Converged in %d iterations (rel. residual: %g)\n', ...
              length(resVec)-1, relRes);
    else
      fprintf('Something went wrong: %d (rel. residual: %g)\n', flag, relRes);
    end

  case 'fixed' %% Fixed point
    N       = 20; %% Max iteration
    tol     = 1;  %% No meaning
    gg      = zeros(N, length(gInit));
    gg(1,:) = gInit;

    for i = (1:N)
      p          = closeOSVol(s, k, l, t, q, lambda, gg(i, :));
      gg(i+1, :) = closeOSUp(lambda, gg(i, :), p);
    end

    g      = gg(end, :);
    resVec = abs(gg);

  otherwise
    error('Wrong solver');
end

%% Solution
solution = closeOSSol(s, k, l, t, q, lambda, g, x);

%% Plot
figure(1);
hold on;
semilogy((0:length(resVec)-1), resVec/resVec(1), '-o');
%plot(resVec/resVec(1), '-o');
%plot([0, length(resVec)-1], [tol tol], '--k');

set(gca,'yscale','log')
title('Solver convergence');
xlabel('Iteration');
ylabel('Relative residual/fixed-point');
hold off;

figure(2);
hold on;
plot(x, real(solution), '-');
plot(x, imag(solution), '--');
plot([l*(t-0.5) l*(t-0.5)], [min([real(solution) imag(solution)]) ...
                             max([real(solution) imag(solution)])], '-k');

title('Solution');
xlabel('x');
ylabel('pHat(x)');
legend({'Real', 'Imag'}, 'location', 'northeast');
hold off;

figure(3);
hold on;
sa = (0*k:pi/h/100:2*k);
sb = (0*k:pi/h:2*k);
rhoa = Rho2Dom(tc(sa), dtn(sa));
rhob = Rho2Dom(tc(sb), dtn(sb));
plot(sa/k, abs(rhoa), '-');
plot(sb/k, abs(rhob), 'x');
plot([sa(1) sa(end)]/k, [1 1], '-k');
axis([0 2 0 max([abs(rhob)' 1.5])]);

title('Convergence radius');
xlabel('s/k');
ylabel('Rho');
legend({'Continuous', 'Discreet'}, 'location', 'northeast');
hold off;
