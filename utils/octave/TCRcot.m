function lambda = TCRcot(s, k, l, t, N, gamma)
  lambda0 = rcot(s, k, (1-t)*l, N(1), gamma(1));
  lambda1 = rcot(s, k,   (t)*l, N(2), gamma(2));

  lambda  = [lambda0(:), lambda1(:)];
end
