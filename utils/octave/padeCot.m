%% PadeCot TC form subdomain i to subdomain j

function lambda = padeCot(s, k, lj, N, gamma)
  %% Pade
  syms z;
  pz = pade(z*cot(z), z, 0, 'Order', [2*N 2*N]);

  %% Numerator and denominator
  [num, den] = numden(pz);
  NN = double(coeffs(num));
  DD = double(coeffs(den));

  %% Partial fraction decomposition
  [rr, pp, kk] = residue(flip(NN), flip(DD));

  %% TC
  alpha  = sqrt(k^2-s.^2);
  lambda = kk * ones(size(alpha));
  for i = [1:N]
    lambda = lambda + rr(i) ./ ((alpha*lj).^2 - pp(i));
  end
  lambda = lambda / lj;
end
