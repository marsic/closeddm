%% Data
l = 0.50;
t = 0.33;
h = 0.25;
k = 4.3 * 2*pi/l;

%% Side of interface [1 2]
side = 1;

%% TC & DtN
%N = TCRcotMinAux(k, [(1-t) t]*l)
N = 6 * [1 1];

%tc  = @(s)TCPadeC(s, k, l, t, N,    [0 0]/100); %% Needs matlab sym. toolbox...
tc  = @(s)TCRcot( s, k, l, t, N,    [0 0]/100);
%tc  = @(s)TCOO0(  s, k, l, t,       [0 0], 'closed');
%tc  = @(s)TCPade( s, k,       N(1), [0 0], 2*pi/4);
%tc  = @(s)TCOO0(  s, k, l, t,       [0 0], 'open');
dtn = @(s)TCOpt(  s, k, l, t, 'closed');

%% Plots
sa = (0*k:pi/h/1000:2*k);
sd = (0*k:pi/h:2*k);

sc  = (0:0.001:+1) * 2*k;
fcc = tc(sc);
fdc = dtn(sc);

plot(sc/k, [real(fdc(:, side)) imag(fdc(:, side))], '-');
hold on;
plot(sc/k, [real(fcc(:, side)) imag(fcc(:, side))], '--');
hold off;
axis([sc(1)/k sc(end)/k -100/l 100/l]);
