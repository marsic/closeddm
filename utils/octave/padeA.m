function y = padeA(j, N, theta)
  z   = cos(-theta / 2) + 1j*sin(-theta / 2);
  res = z * padeAj(j, N);

  z   = cos(-theta) - 1 + 1j*sin(-theta);
  y   = res / ((1 + z*padeBj(j, N)) * (1 + z*padeBj(j, N)));
end
