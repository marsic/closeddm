l = 0.50;
t = 0.33;
h = 0.25;
k = 50.002*pi/l; %50.002*pi/l
s = (0*k:pi/h:1*k);

type = 'minmax'

dtn = @(s)TCOpt(s, k, l, t, 'closed');
tc  = @(s, x)TCRcot(s, k, l, t, 32, x);
%tc  = @(s, x)TCPade(s, k, 32, x, 1*pi/4);
%tc  = @(s, x)TCOO0(s, k, l, t, x, 'closed');

rho  = @(s, x)Rho2Dom(tc(s, x), dtn(s));
opts = optimset('display', 'iter', 'tolx', 1e-18, ...
               'MaxFunEvals', 6400*8, 'MaxIter', 6400*8);
x0   = [0 0];

switch(type)
  case 'minmax'
    fToMin = @(x)(abs(rho(s, x)));
    x      = fminimax(fToMin, x0, [],[], [],[], zeros(size(x0)),[], [], opts);
  case 'mean'
    fToMin = @(x)(mean(abs(rho(s, x))));
    x      =fminsearch(fToMin, x0, opts);
end

fprintf('Initial   max. objective function: %e\n', max(fToMin(x0)));
fprintf('Initial   mean objective function: %e\n', mean(fToMin(x0)));
fprintf('Optimized max. objective function: %e\n', max(fToMin(x)));
fprintf('Optimized mean objective function: %e\n', mean(fToMin(x)));
fprintf('Initial   guess:                  [');
for i = (1:length(x0))
  fprintf('%e ', x0(i));
end
  fprintf(']\n');
fprintf('Optimized parameter:              [');
for i = (1:length(x0))
  fprintf('%e ', x(i));
end
  fprintf(']\n');

%dlmwrite('xOpt.csv', x', 'precision', '%.16e');
%x = dlmread('xOpt20.1.csv', ';', 11, 0);

figure(1);
hold on;

title('Modulus of rho');
sa = (0*k:pi/h/001:2*k);
stem(sa/k, abs(rho(sa, x0)), '--');
stem(sa/k, abs(rho(sa, x)),  '-');

%sb = (0*k:pi/h/100:2*k);
%plot(sb/k, abs(rho(sb, x0)), '--b');
%plot(sb/k, abs(rho(sb, x)),  '--r');

xlabel('s/k')
ylabel('abs(rho)')
legend({'First guess', 'Optimized', ...
        'Oversampled (first guess)', 'Oversampled (optimized)'}, ...
       'location', 'northeast');

axis([0 2 0 max([abs(rho(s, x))'+0.5 1.5])]);
hold off;

%figure(2);
%hold on;
%
%title('mean(abs(rho))');
%xb = [0:1/100:1];
%mrho = zeros(size(xb));
%for i = [1:length(xb)]
%  mrho(i) = mean(abs(rho(s, xb(i))));
%end
%plot(xb*100, mrho);
%plot([x*100 x*100], [0 max(mrho)], '-k');
%plot([xb(1) xb(end)]*100,   [1 1], '-k');
%
%xlabel('gamma [%]')
%ylabel('mean(abs(rho))')
%hold off;
