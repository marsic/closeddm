%% RCot TC form subdomain i to subdomain j

function lambda = rcot(s, k, lj, N, gamma)
  lambda = ones(size(s)) / lj;
  kc = k + 1j*k*gamma;
  for n = [1:N]
    lambda = lambda + ...
             2/lj * (1 - s.^2/k^2) ./ (1 - ((n*pi)/(k*lj))^2 - s.^2/kc^2);
  end

  %lambda = lambda + 1j*k*gamma;
end
