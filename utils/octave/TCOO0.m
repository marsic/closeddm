function lambda = TCOO0(s, k, l, t, gamma, type)
  kc = k + 1j*k*gamma;

  switch(type)
    case 'open'
      lambda = [-1j*kc(1)*ones(length(s), 1), ...
                -1j*kc(2)*ones(length(s), 1)];
    case 'closed'
      lambda = [+kc(1)*cot(k*(1-t)*l) * ones(length(s), 1), ...
                +kc(2)*cot(k*  (t)*l) * ones(length(s), 1)];
    case 'mix'
      lambda = [-(1j*kc(1) + kc(1)*cot(k*(1-t)*l)) * ones(length(s), 1), ...
                -(1j*kc(2) + kc(2)*cot(k*  (t)*l)) * ones(length(s), 1)];
    otherwise
      error(strcat('TCOO0: unknown type - ', type));
  end
end
