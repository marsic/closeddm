%% Optimal TC form subdomain i to subdomain j

function lambda = TCOpt(s, k, l, t, setting)
  alpha = -1j*sqrt(k.*k - s.*s);

  switch(setting)
    case 'open'
      lambda0 = alpha;
      lambda1 = alpha;
    case 'closed'
      lambda0 = alpha .* coth(alpha*(1-t)*l);
      lambda1 = alpha .* coth(alpha*  (t)*l);

      %% Fix NaNs, if any
      lambda0(find(isnan(lambda0), 1)) = 1/((1-t)*l);
      lambda1(find(isnan(lambda1), 1)) = 1/(  (t)*l);
    otherwise
      error('Wrong setting for TCOpt');
  end

  %% Left and right lambdas
  lambda = [lambda0(:), lambda1(:)];
end
