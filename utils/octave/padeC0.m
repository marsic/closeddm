function y = padeC0(N, theta)
  sum = 1;
  z   = cos(-theta) - 1 + 1j*sin(-theta);

  for j = (1:N)
    sum = sum + (z*padeAj(j, N)) / (1 + z*padeBj(j, N));
  end

  y = sum * (cos(theta / 2) + 1j*sin(theta / 2));
end
