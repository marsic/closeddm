function  y = padeB(j, N, theta)
  z   = cos(-theta) + 1j*sin(-theta);
  res = z * padeBj(j, N);

  z   = cos(-theta) - 1 + 1j*sin(-theta);
  y   = res / (1 + z * padeBj(j, N));
end
