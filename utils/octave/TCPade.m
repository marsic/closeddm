function lambda = TCPade(s, k, N, epsilon, theta)
  kE     = (1+1j*epsilon)*k;
  lambda = padeC0(N, theta) * ones(size(s));
  lambda = [lambda(:), lambda(:)];

  for n = (1:2)
    for j = (1:N)
      lambda(:, n) = lambda(:, n) - ...
                        padeA(j,N,theta)*(s(:).^2/kE(n)^2) ./ ...
                     (1-padeB(j,N,theta)*(s(:).^2/kE(n)^2));
    end
  end

  lambda = -1j*k*lambda;
end
