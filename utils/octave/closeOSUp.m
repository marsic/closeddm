function g = closeOSUp(lambda, gOld, p)
  %% Init
  g = zeros(size(gOld));

  for i = (1:size(lambda, 1))
    g(2*i-1) = -gOld(2*i-0) + (lambda(i, 1)+lambda(i, 2))*p(2*i-0);
    g(2*i-0) = -gOld(2*i-1) + (lambda(i, 2)+lambda(i, 1))*p(2*i-1);
  end
end
