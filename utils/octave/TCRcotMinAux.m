function nMin = TCRcotMinAux(k, lj)
  lambda_w = 2*pi/k;
  nMin = ceil(2*lj/lambda_w);
end
