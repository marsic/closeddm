function y = padeBj(j, N)
  tmp = cos(j*pi / (2*N + 1));
  y   = tmp^2;
end
