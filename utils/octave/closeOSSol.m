function y = closeOSSol(s, k, l, t, q, lambda, g, x)
  %% Source is p_0(-l/2) = q

  %% Check
  if(mod(length(x), 2) == 0)
    error('x must be have an odd length');
  end
  if(x(floor(end/2)+1) ~= 0)
    error('x must be equal to zero in its middle');
  end

  alpha = -1j*sqrt(k.*k - s.*s);
  beta0 = zeros(size(s));
  for i = (1:length(s))
    beta0(i) = 2*alpha(i)     .* cosh(alpha(i)*t*l) + ...
               2*lambda(i, 1) .* sinh(alpha(i)*t*l);
  end

  beta1 = zeros(size(s));
  for i = (1:length(s))
    beta1(i) = 2*alpha(i)     .* cosh(alpha(i)*(1-t)*l) + ...
               2*lambda(i, 2) .* sinh(alpha(i)*(1-t)*l);
  end

  gamma = l*(t-0.5);

  [v, idx] = min(abs(x-gamma));
  xM = x(1:idx);
  xP = x(idx+1:end);

  yM = zeros(size(xM));
  yP = zeros(size(xP));

  for i =(1:length(s))
    yM = yM + ((q(i)*(alpha(i)-lambda(i, 1))*exp(-alpha(i)*gamma) + ...
                g(2*i-1)*exp(+alpha(i)*l/2))*exp(+alpha(i)*xM) + ...
               (q(i)*(alpha(i)+lambda(i, 1))*exp(+alpha(i)*gamma) - ...
                g(2*i-1)*exp(-alpha(i)*l/2))*exp(-alpha(i)*xM)) / beta0(i);

    yP = yP + ((0                      - ...
                g(2*i-0)*exp(-alpha(i)*l/2))*exp(+alpha(i)*xP) + ...
               (0                      + ...
                g(2*i-0)*exp(+alpha(i)*l/2))*exp(-alpha(i)*xP)) / beta1(i);
  end
  y = [yM yP];
end
