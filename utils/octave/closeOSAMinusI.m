function y = closeOSAMinusI(s, k, l, t, lambda, x)
  p = closeOSVol(s, k, l, t, zeros(size(s)), lambda, x); % ! Source free
  g = closeOSUp(lambda, x, p);
  y = x - g; % ! (I-A)x = x - g
end
