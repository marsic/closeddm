function y = padeAj(j, N)
  tmp = sin(j*pi / (2*N + 1));
  y   = 2 / (2*N + 1) * tmp^2;
end
