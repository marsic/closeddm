#!/bin/bash

## Note
#######
# Call 'closeddm ... -memoryDDM ...' to write memory usage and timings

## Statistics
#############
function statistics(){
    # Create a temporary directory and 'trap' it for removal after EXIT
    unset TMPDIR
    trap '[[ -d "${TMPDIR}" ]] && rm -rf "${TMPDIR}"' EXIT
    TMPDIR=$(mktemp -d)

    # Parse file and create TMPDIR/post.m script
    DATA=${TMPDIR}/post.m
    MATCH='Resources @ DDM Iteration'

    echo 't = ['                                               > ${DATA}
    $(dirname $0)/parse.sh "$MATCH" "Wall" $1 | tr '\n' ','   >> ${DATA}
    echo '];'                                                 >> ${DATA}
    echo 'd = diff(t);'                                       >> ${DATA}
    echo "disp([t(end)-t(1) d(1) mean(d(2:end)) length(d)]')" >> ${DATA}
}

## Do it
########
statistics $1
octave ${DATA}
