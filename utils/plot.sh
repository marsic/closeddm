#!/bin/bash

### Generate DATA for residual
##############################
function residual(){
    ## Check
    if [[ "$1" != "absolute" && "$1" != "relative" ]]
    then
        echo "Error: residual is either [absolute] or [relative]"
        exit -1
    fi

    ## Create a temporary directory and 'trap' it for removal after EXIT
    unset TMPDIR
    trap '[[ -d "${TMPDIR}" ]] && rm -rf "${TMPDIR}"' EXIT
    TMPDIR=$(mktemp -d)

    ## Parse files and store the data in TMPDIR/data.csv
    DATA=${TMPDIR}/data.csv
    for i in ${@:2}
    do
        echo '"'$i'"' >> ${DATA}
        $(dirname $0)/parse.sh "Iteration" $1' residual' $i >> ${DATA}
        echo >> ${DATA}
        echo >> ${DATA}
    done
}

### Generate DATA for spectrum
##############################
function spectrum(){
    ## Concatenate file names
    DATA=""
    for i in $@
    do
        DATA=${DATA}" "$i
    done
}

### Plotting script
###################
## Script directory
DIR=$(dirname $0)

## Check first argument, select appropriate gnuplot script and generate DATA
if   [[ "$1" == "residual" ]]
then
    GP=${DIR}/residual.gp
    residual ${@:2}
elif [[ "$1" == "spectrum" ]]
then
    GP=${DIR}/spectrum.gp
    spectrum ${@:2}
elif [[ "$1" == "convergence" ]]
then
    GP=${DIR}/convergence.gp
    DATA=${2}
else
    echo "Usage: plot " \
         "[residual [absolute | relative] | spectrum | convergence]" \
         "file1 file2 ..."
    exit -1
fi

## Plot DATA
gnuplot -persist -c ${GP} "${DATA}"
